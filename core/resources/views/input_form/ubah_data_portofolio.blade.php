@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Portofolio</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('portofolio') }}">Portofolio</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit data portofolio
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Portofolio</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ url('update-portofolio/'.$portofolio->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Nama Klien</label>

                                            <select class="select2 form-select" id="select2-basic" name="id_users">
                                                @foreach($pilihan_user as $pilih_user)
                                                @if ($portofolio->id_users == $pilih_user->id)
                                                    <option value="{{ $pilih_user->id }}" selected>
                                                        {{ $pilih_user->name }}
                                                    </option>
                                                @else
                                                    <option value="{{ $pilih_user->id }}">
                                                        {{ $pilih_user->name }}
                                                    </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Kategori</label>

                                            <select class="select2 form-select" id="select2-basic" name="id_kategori">
                                                @foreach($pilihan_kategori as $pilih_sub)
                                                @if ($portofolio->id_kategori == $pilih_sub->id)
                                                    <option value="{{ $pilih_sub->id }}" selected>
                                                        {{ $pilih_sub->nama_kategori }}
                                                    </option>
                                                @else
                                                    <option value="{{ $pilih_sub->id }}">
                                                        {{ $pilih_sub->nama_kategori }}
                                                    </option>
                                                @endif
                                                
                                                
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Link Finish Project</label>

                                            <select class="select2 form-select" id="select2-basic" name="id_preview">
                                                @foreach($pilihan_link as $pilih_link)
                                                @if ($portofolio->id_preview == $pilih_link->id)
                                                    <option value="{{ $pilih_link->id }}" selected>
                                                        {{ $pilih_link->link_finish_project }}
                                                    </option>
                                                @else
                                                    <option value="{{ $pilih_link->id }}">
                                                        {{ $pilih_link->link_finish_project }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div> --}}
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Projek Klien </label>
                                            <br>
                                            <img src="{{ asset('app-assets/images/docs/'.$portofolio->projek) }}" id ="" alt="{{ $portofolio->name }}" style="width: 5cm;">
                                        </div>
                                        <div class="mb-1">
                                            <input class="form-control" type="file" id="" name="projek" value="{{ $portofolio->projek }}">
                                        </div> 
                                       
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Link Finish Projek</label>
                                            <input type="text" name="hasil_projek" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $portofolio->hasil_projek }}" />
                                        </div> --}}
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection