@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Klien Kami</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('klien-kami') }}">Klien</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Tambah Data</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ route('data-klien-kami') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama</label>

                                            <input type="text" name="nama" id="basic-addon-name" class="form-control @error('nama') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan nama</div>
                                        </div>  
                                        <div class="mb-1">
                                            <label for="Foto" class="form-label">Logo</label>
                                            <input class="form-control @error('logo') is-invalid @enderror" type="file" id="logo" name="logo">
                                            <div class="invalid-feedback">Silahkan masukkan gambar kategori</div>
                                        </div>           
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection