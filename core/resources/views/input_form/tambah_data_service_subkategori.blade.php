@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Layanan Kategori</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('servicesubkategori') }}">Layanan Kategori</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data Layanan Kategori
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Tambah Data Layanan Kategori</h4>
                                    
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{ url('postservicesubbkategori') }}">
                                        @csrf
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">ID Services</label>
                                            <input type="text" name="id_services" id="basic-addon-name" class="form-control" placeholder="ID Services" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter id services.</div>
                                        </div> --}}
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Nama Kategori</label>

                                            <select class="select2 form-select @error('id_kategori') is-invalid @enderror" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($pilihan_kate as $pilih_kate)
                                                
                                                <option value="{{ $pilih_kate->id }}">
                                                    {{ $pilih_kate->nama_kategori }}
                                                </option>
                                                
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Silahkan pilih kategori</div>
                                        </div>

                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Nama Subkategori</label>
                                            <select class="select2 form-select @error('id_services') is-invalid @enderror" id="select2-basic" name="id_services">
                                                <option value="">-- Pilih Subkategori --</option>
                                                @foreach($pilihan_serSub as $pilih_serSub)
                                                
                                                <option value="{{ $pilih_serSub->id }}">
                                                    {{ $pilih_serSub->jenis_paket }}
                                                </option>
                                                
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Silahkan pilih nama paket</div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
@endsection