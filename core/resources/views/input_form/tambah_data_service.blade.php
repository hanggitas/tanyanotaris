@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Layanan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('servis') }}">Layanan</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data Layanan
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Tambah Data Layanan</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ route('data_service') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('POST')
                                        <!-- <div class="mb-1">
                                            <label for="Foto" class="form-label">Gambar</label>
                                            <input class="form-control  @error('foto') is-invalid @enderror" type="file" id="Foto" name="foto">
                                            <div class="invalid-feedback">Silahkan masukkan gambar kategori</div>
                                        </div> -->
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Kategori</label>
                                            <select class="select2 form-select @error('id_kategori') is-invalid @enderror" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($kategori as $item)
                                                    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                @endforeach
                                            </select>    
                                            <div class="invalid-feedback">Silahkan Pilih Kategori</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Layanan</label>
                                            <input type="text" name="jenis_paket" id="basic-addon-name" class="form-control @error('jenis_paket') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="invalid-feedback">Silahkan masukkan nama layanan</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Deskripsi Layanan</label>
                                            <input type="text" name="detail_paket" id="basic-addon-name" class="form-control @error('detail_paket') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="invalid-feedback">Silahkan masukkan deskripsi layanan</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Persyaratan</label>
                                            <input type="text" name="syarat_layanan" id="basic-addon-name" class="form-control @error('syarat_layanan') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="invalid-feedback">Silahkan masukkan persyaratan</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Tahapan Pengerjaan</label>
                                            <input type="text" name="tahapan_pengerjaan" id="basic-addon-name" class="form-control @error('tahapan_pengerjaan') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="invalid-feedback">Silahkan masukkan tahapan pengerjaan</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Estimasi Waktu</label>
                                            <input type="text" name="estimasi_waktu" id="basic-addon-name" class="form-control @error('estimasi_waktu') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="invalid-feedback">Silahkan masukkan estimasi waktu</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Harga</label>
                                            <input type="text" name="harga" id="basic-addon-name" class="form-control @error('harga') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="invalid-feedback">Silahkan masukkan harga layanan</div>
                                        </div>
                                                      
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection