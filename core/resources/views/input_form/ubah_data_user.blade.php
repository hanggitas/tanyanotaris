@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Ubah Data User</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#}">User</a>
                                    </li>
                                    <li class="breadcrumb-item active">Ubah Data User
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Ubah Data User</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ url('update-user/'.$user->id) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                        {{-- <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Email</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="email" value="{{$user->email}}" rows="3">
                                        </div> --}}
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Nama</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="name" value="{{$user->name}}" rows="3">
                                        </div>
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Password</label>
                                            <input type="password" class="form-control" id="validationBioBootstrap" name="password" rows="3">
                                        </div>
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Nomor HP</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="no_hp_user" value="{{$user->no_hp_user}}" rows="3">
                                        </div>
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Alamat</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="alamat_user" value="{{$user->alamat_user}}" rows="3">
                                        </div>
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Status User</label>
                                            <div class="demo-inline-spacing">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="verifikasi_email"  value="1" @if($user->status_user != 0) {{"checked"}} @endif/>
                                                    <label class="form-check-label" for="menunggu">Aktif</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="verifikasi_email" value="0" @if($user->status_user == 0) {{"checked"}} @endif/>
                                                    <label class="form-check-label" for="inlineRadio2">Tidak Aktif</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection