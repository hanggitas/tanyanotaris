@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Portofolio</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('portofolio') }}">Portofolio</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data Portofolio
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Tambah Data Portofolio</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ route('data_portofolio') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('POST')
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Nama Klien</label>
                                            <select class="select2 form-select @error('id_users') is-invalid @enderror" id="select2-basic" name="id_users">
                                                <option value="">-- Pilih Nama Klien --</option>
                                                @foreach($pilihan_user as $pilih_user)
                                                
                                                <option value="{{ $pilih_user->id }}">
                                                    {{ $pilih_user->name }}
                                                </option>
                                                
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Silahkan pilih nama klien</div>
                                        </div>

                                        <div class="mb-1">
                                            <label for="Projek" class="form-label">Projek Klien</label>
                                            <input class="form-control @error('projek') is-invalid @enderror" type="file" id="Projek" name="projek">
                                            <div class="invalid-feedback">Silahkan masukkan projek klien</div>
                                        </div>   
                                        
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection