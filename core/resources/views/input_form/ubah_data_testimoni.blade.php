@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Testimoni</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('testimoni') }}">Testimoni</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Testimoni
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Testimoni</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{ url('update-testimoni/'.$testi->id) }} " enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Nama Klien</label>

                                            <select class="select2 form-select" id="select2-basic" name="id_users">
                                                @foreach($pilihan_klien as $pilih)
                                                @if ($testi->id_users == $pilih->id)
                                                    <option value="{{ $pilih->id }}" selected>
                                                        {{ $pilih->name }}
                                                    </option>
                                                @else
                                                    <option value="{{ $pilih->id }}">
                                                        {{ $pilih->name }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Review</label>

                                            <textarea type="text" name="review" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" rows="5">{{ $testi->review}}</textarea>
                                        </div>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection