@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Subkategori</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('subkategori') }}">Subkategori</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data Subkategori
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Tambah Data Subkategori</h4>
                                    
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{ route ('data_subkategori') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Kategori</label>

                                            <select class="select2 form-select @error('id_kategori') is-invalid @enderror" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($pilihan_kategori as $pilih_kategori)
                                                
                                                <option value="{{ $pilih_kategori->id }}">
                                                    {{ $pilih_kategori->nama_kategori }}
                                                </option>
                                                
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Silahkan masukkan pilih kategori</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Subkategori</label>

                                            <input type="text" name="nama_subkategori" id="basic-addon-name" class="form-control @error('nama_subkategori') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan nama subkategori</div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="Foto" class="form-label">Gambar</label>
                                            <input class="form-control @error('foto') is-invalid @enderror" type="file" id="Foto" name="foto">
                                            <div class="invalid-feedback">Silahkan masukkan gambar subkategori</div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
@endsection