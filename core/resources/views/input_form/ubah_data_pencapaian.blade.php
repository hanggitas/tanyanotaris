@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Pencapaian</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('pencapaian') }}">Pencapaian</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Pencapaian
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Pencapaian</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{ url('update-pencapaian/'.$capai->id) }} " enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Klien Senang</label>

                                            <input type="text" name="klien_senang" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $capai->klien_senang}}" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Kasus</label>

                                            <input type="text" name="kasus" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $capai->kasus}}" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Kasus Menang</label>

                                            <input type="text" name="kasus_menang" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $capai->kasus_menang}}" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Penghargaan</label>

                                            <input type="text" name="penghargaan" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $capai->penghargaan}}" />
                                        </div>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection