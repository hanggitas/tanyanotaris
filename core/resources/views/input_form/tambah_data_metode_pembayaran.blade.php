@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Form Metode Pembayaran</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('metodepembayaran') }}">Metode Pembayaran</a>
                                </li>
                                <li class="breadcrumb-item active">Tambah Data Metode Pembayaran
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Validation -->
            <section class="bs-validation">
                <div class="row">
                    <!-- Bootstrap Validation -->
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Data Metode Pembayaran</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate method="post" action="{{ route('data_metode_pembayaran') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-1">
                                        <label class="form-label @error('nama_pemilik_rekening') is-invalid @enderror" for="basic-addon-name">Nama Pemilik Rekening</label>
                                        <input type="text" name="nama_pemilik_rekening" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="invalid-feedback">Silahkan masukkan nama pemilik rekening</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label @error('metode_pembayaran') is-invalid @enderror" for="basic-addon-name">Metode Pembayaran</label>
                                        <input type="text" name="metode_pembayaran" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="invalid-feedback">Silahkan masukkan metode pembayaran</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label @error('nama_provider') is-invalid @enderror" for="basic-addon-name">Nama Provider</label>
                                        <input type="text" name="nama_provider" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="invalid-feedback">Silahkan masukkan nama provider</div>
                                    </div>
                                    <div class="mb-1">
                                        <label for="foto_provider " class="form-label @error('foto_provider') is-invalid @enderror">Foto Provider</label>
                                        <input class="form-control" type="file" id="foto_provider" name="foto_provider">
                                        <div class="invalid-feedback">Silahkan masukkan gambar provider</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label @error('no_rekening') is-invalid @enderror" for="basic-addon-name">No Rekening</label>
                                        <input type="text" name="no_rekening" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="invalid-feedback">Silahkan masukkan nomor rekening</div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Bootstrap Validation -->
                </div>
            </section>
            <!-- /Validation -->

        </div>
    </div>
</div>
@endsection