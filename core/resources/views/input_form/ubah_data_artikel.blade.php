@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Artikel</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('artikel') }}">Artikel</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Artikel
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Artikel</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ url('updateartikel/'.$artikels->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('POST')
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Kategori</label>
                                            <select class="select2 form-select @error('id_kategori') is-invalid @enderror" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($kategori_all as $item)
                                                    @if($item->id == $kategori->id)
                                                        <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                    @endif
                                                @endforeach
                                            </select>    
                                            <div class="invalid-feedback">Silahkan Pilih Kategori</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Artikel</label>

                                            <input type="text" name="judul" id="basic-addon-name" class="form-control @error('nama_kategori') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required value="{{$artikels->judul}}" />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan nama kategori</div>
                                        </div>  
                                        <div class="mb-1">
                                            <label for="Foto" class="form-label">Gambar Artikel</label>
                                            <input class="form-control @error('gambar') is-invalid @enderror" type="file" id="Foto" name="gambar" accept="image/*">
                                            <div class="invalid-feedback">Silahkan masukkan gambar kategori</div>
                                        </div>
                                        <div class="mb-1" style="display: none;">
                                            <label class="d-block form-label" for="validationBioBootstrap">Deskripsi Artikel</label>
                                            <textarea id="deskripsi" class="form-control  @error('deskripsi') is-invalid @enderror" id="validationBioBootstrap" name="deskripsi" rows="10" required></textarea>
                                            <div class="invalid-feedback">Silahkan masukkan deskripsi</div>
                                        </div>
                                        <label class="d-block form-label" for="validationBioBootstrap">Deskripsi Artikel</label>
                                        <div id="editor" class="mb-2" style="height: 300px;">{!! $artikels->deskripsi !!}</div>
                                        <button type="submit" id="submit" style="display: none;"></button>
                                        <button type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>
                                        <a class="btn btn-outline-primary" href="{{route('artikel')}}">Kembali</a>
                                    </form>
                                </div>
                            </div>
                        </div>
@endsection
@section('script')
<script>
    function simpan()
    {
        var myEditor = document.querySelector('#editor')
        var html = myEditor.children[0].innerHTML
        $("#deskripsi").text(html);
        $("#submit").click();
    }   
</script>
@endsection