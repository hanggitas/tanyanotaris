@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Layanan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('servis') }}">Layanan</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Layanan
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Layanan</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ url('update-service/'.$service->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Kategori</label>
                                            <select class="select2 form-select @error('id_kategori') is-invalid @enderror" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($kategori_all as $item)
                                                    @if($item->id == $kategori->id_kategori)
                                                        <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                    @endif
                                                @endforeach
                                            </select>    
                                            <div class="invalid-feedback">Silahkan Pilih Kategori</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Layanan</label>
                                            <input type="text" name="jenis_paket" id="basic-addon-name" class="form-control @error('jenis_paket') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $service->jenis_paket }}" />
                                            <div class="invalid-feedback">Nama layanan wajib diisi</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Deskripsi Layanan</label>
                                            <input type="text" name="detail_paket" id="basic-addon-name" class="form-control @error('detail_paket') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $service->detail_paket }}" />
                                            <div class="invalid-feedback">Deskripsi wajib diisi</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Persyaratan</label>
                                            <input type="text" name="syarat_layanan" id="basic-addon-name" class="form-control @error('syarat_layanan') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $service->syarat_layanan }}" />
                                            <div class="invalid-feedback">Persyaratan wajib diisi</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Tahapan Pengerjaan</label>
                                            <input type="text" name="tahapan_pengerjaan" id="basic-addon-name" class="form-control @error('tahapan_pengerjaan') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $service->tahapan_pengerjaan }}" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Estimasi Waktu</label>
                                            <input type="text" name="estimasi_waktu" id="basic-addon-name" class="form-control @error('estimasi_waktu') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $service->estimasi_waktu }}" />
                                            <div class="invalid-feedback">Estimasi waktu wajib diisi</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Harga</label>
                                            <input type="text" name="harga" id="basic-addon-name" class="form-control @error('harga') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" value="{{ $service->harga }}" />
                                            <div class="invalid-feedback">Harga wajib diisi</div>
                                        </div>                  
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection