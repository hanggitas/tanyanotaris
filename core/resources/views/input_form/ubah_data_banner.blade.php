@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Banner</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('banner') }}">Banner</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Banner
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Banner</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate action="{{ url('update-data-banner/'.$s->id) }}" method="post">
                                        @csrf
                                        @method('POST')
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Image Banner</label>
                                            <select class="select2 form-select @error('image_banner_id') is-invalid @enderror" id="select2-basic" name="image_banner_id">
                                                <option value="">-- Pilih Image Banner --</option>
                                                @foreach($image_banner as $item)
                                                    @if($item->id == $s->image_banner_id)
                                                        <option value="{{$item->id}}" selected>
                                                            {{$item->nama}}
                                                        </option>
                                                    @else
                                                        <option value="{{$item->id}}">
                                                            {{$item->nama}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>    
                                            <div class="invalid-feedback">Silahkan Pilih Image Banner</div>
                                        </div>  
                                        <div class="mb-1">
                                            <div class="from-group">
                                                <label for="status">Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="1" selected>Publish</option>
                                                    <option value="0">Draf</option>
                                                </select>
                                            </div>    
                                        </div>     
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Judul</label>

                                            <input type="text" name="judul_caption" id="basic-addon-name" class="form-control @error('judul_caption') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required  value="{{$s->judul_caption}}"/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan judul</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Caption</label>

                                            <input type="text" name="caption" id="basic-addon-name" class="form-control @error('caption') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required  value="{{$s->caption}}"/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan caption</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Button Caption</label>

                                            <input type="text" name="btn_caption" id="basic-addon-name" class="form-control @error('btn_caption') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required  value="{{$s->btn_caption}}"/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan button caption</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">URL</label>

                                            <input type="text" name="url" id="basic-addon-name" class="form-control @error('url') is-invalid @enderror" aria-label="Name" aria-describedby="basic-addon-name" required  value="{{$s->url}}"/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan url</div>
                                        </div>
                                        <button type="submit" name="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->
@endsection