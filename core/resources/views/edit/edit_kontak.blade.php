@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Kontak</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('kontak') }}">Kontak</a>
                                </li>
                                <li class="breadcrumb-item active">Edit Data Kontak
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div> --}}
        </div>
        <div class="content-body">
            <!-- Validation -->
            <section class="bs-validation">
                <div class="row">
                    <!-- Bootstrap Validation -->
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Edit Data Kontak</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate method="post" action="{{url('/kontak/'.$data_kon->id.'/update')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Email</label>

                                        <input type="text" value="{{ old('email',$data_kon->email) }}" name="email" id="basic-addon-name" class="form-control" placeholder="Email" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan email Anda</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Nomor Telepon</label>

                                        <input type="text" value="{{ old('nomor_telepon',$data_kon->nomor_telepon) }}" name="nomor_telepon" id="basic-addon-name" class="form-control" placeholder="Nomor Telepon" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan nomor telepon Anda</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Nomor Konsultasi</label>

                                        <input type="text" value="{{ old('nomor_konsultasi',$data_kon->nomor_konsultasi) }}" name="nomor_konsultasi" id="basic-addon-name" class="form-control" placeholder="Nomor Konsultasi" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan nomor telepon Anda</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Alamat</label>

                                        <textarea type="text" name="alamat" id="basic-addon-name" class="form-control" placeholder="Alamat" aria-label="Name" aria-describedby="basic-addon-name" required>{{ old('alamat',$data_kon->alamat) }}</textarea>
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan alamat Anda</div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Instagram</label>

                                        <input type="text" value="{{ old('instagram',$data_kon->instagram) }}" name="instagram" id="basic-addon-name" class="form-control" placeholder="Akun Instagram" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan akun instagram Anda</div>
                                    </div>
                                    
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">YouTube</label>

                                        <input type="text" value="{{ old('youtube',$data_kon->youtube) }}" name="youtube" id="basic-addon-name" class="form-control" placeholder="Channel YouTube" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan channel youTube Anda</div>
                                    </div>

                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Facebook</label>

                                        <input type="text" value="{{ old('facebook',$data_kon->facebook) }}" name="facebook" id="basic-addon-name" class="form-control" placeholder="Facebook" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan channel youTube Anda</div>
                                    </div>

                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Twitter</label>

                                        <input type="text" value="{{ old('twitter',$data_kon->twitter) }}" name="twitter" id="basic-addon-name" class="form-control" placeholder="twitter" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan channel youTube Anda</div>
                                    </div>
                                    
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">LinkedIn</label>

                                        <input type="text" value="{{ old('linkedin',$data_kon->linkedin) }}" name="linkedin" id="basic-addon-name" class="form-control" placeholder="LinkedIn" aria-label="Name" aria-describedby="basic-addon-name" required />
                                        <div class="valid-feedback">Looks good!</div>
                                        <div class="invalid-feedback">Silahkan masukkan linkedIn Anda</div>
                                    </div>
                                    
                                    </div>
                                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                </form>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /Bootstrap Validation -->

        </div>
        </section>
        <!-- /Validation -->

    </div>
</div>
</div>
@endsection