@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Pekerjaan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('pekerjaan') }}">Pekerjaan</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Pekerjaan
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Pekerjaan</h4>
                                    
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{ url('/pekerjaan/'.$data_pekerjaan->id.'/update') }}" enctype="multipart/form-data">
                                        @csrf
            
                                        {{-- <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Nama Klien</label>
                                            <input type="text" name="pre_produksi" value="{{ $user->name  }}" placeholder="" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <select class="select2 form-select" id="select2-basic" name="id_users">
                                                @foreach($pilihan_user as $pilih_user)
                                                @if ($data_pekerjaan->id_users == $pilih_user->id)
                                                    <option value="{{ $pilih_user->id }}" selected>
                                                        {{ $pilih_user->name }}
                                                    </option>
                                                @else
                                                    <option value="{{ $pilih_user->id }}">
                                                        {{ $pilih_user->name }}
                                                    </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                        </div> --}}
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Klien</label>
                                            <input type="text" name="id_users" value="{{ $data_pekerjaan->id_users }}" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan angka 1</div>
                                        </div> --}}
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Layanan</label>
                                            <input type="text" value="{{ $data_pekerjaan->jenis_paket }}" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required disabled/>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama User</label>
                                            <input type="text" value="{{ $data_pekerjaan->name }}" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required disabled/>
                                        </div>
                                         <div class="demo-inline-spacing mb-2">
                                                {{-- <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pekerjaan" id="mulai" value="Mulai" {{ old('status_pekerjaan',$data_pekerjaan->status_pekerjaan == 'Mulai')? "checked" : "" }} />
                                                    <label class="form-check-label" for="mulai">Mulai</label>
                                                </div> --}}
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pekerjaan" id="dalam_proses" value="Dalam Proses" {{ old('status_pekerjaan',$data_pekerjaan->status_pekerjaan == 'Dalam Proses') ? "checked" : ""}} />
                                                    <label class="form-check-label" for="dalam_proses">Dalam Proses</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pekerjaan" id="lunas" value="Pekerjaan Selesai" {{ old('status_pekerjaan',$data_pekerjaan->status_pekerjaan == 'Pekerjaan Selesai') ? "checked" : ""}} />
                                                    <label class="form-check-label" for="selesai">Selesai</label>
                                                </div>
                                            </div>
                                        <!-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">ID Order</label>
                                            <input type="text" name="id_order" value="{{ $data_pekerjaan->id_order }}" placeholder="Silahkan input angkat 1" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required disabled/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan angka 1</div>
                                        </div> -->
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Mulai</label>
                                            <input type="text" name="pre_produksi" value="{{ $data_pekerjaan->pre_produksi }}" placeholder="Silahkan input angkat 1" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan angka 1</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Dalam Proses</label>
                                            <input type="text" name="produksi" value="{{ $data_pekerjaan->produksi }}" placeholder="Silahkan input angkat 1" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan angka 1</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Selesai</label>
                                            <input type="text" name="post_produksi" value="{{ $data_pekerjaan->post_produksi }}" placeholder="Silahkan input angkat 1" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Silahkan masukkan angka 1</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Selesai</label>
                                            <input type="text" name="selesai" value="{{ $data_pekerjaan->selesai }}" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback"></div>
                                        </div> --}}
                                        <div class="mb-1">
                                            <label for="Foto" class="form-label">Lampiran Hasil</label>
                                            <input class="form-control @error('lampiran_hasil') is-invalid @enderror" type="file" id="Foto" name="lampiran_hasil">
                                            <small>File : jpg,jpeg,png,pdf harus kurang dari 10MB</small>
                                            <div class="invalid-feedback">File atau Size tidak sesuai! </div>
                                        </div>           
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Size Projek</label>
                                            <input type="text" name="size_project" value="{{ $data_pekerjaan->size_project }}" id="basic-addon-name" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback"></div>
                                        </div> --}}
                                        <!-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Diunggah</label>
                                            <input type="text" name="diunggah" value="{{ $data_pekerjaan->diunggah }}" id="basic-addon-name" placeholder="yyyy-mm-dd" class="form-control" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback"></div>
                                        </div> -->
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
@endsection