@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Edit Service Subkategori</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('servicesubkategori') }}">Service Subkategori</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Service Subkategori
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Layanan Subkategori</h4>
                                    
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{ url('servicesubkategori/'.$data_service_subkategori->id.'/update') }}">
                                        @csrf
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Kategori</label>

                                            <select class="select2 form-select @error('id_kategori') is-invalid @enderror" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($pilihan_kate as $pilih_kate)
                                                
                                                @if ($data_service_subkategori->id_kategori == $pilih_kate->id)
                                                <option value="{{ $pilih_kate->id }}" selected>
                                                    {{ $pilih_kate->nama_kategori }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_kate->id}}">
                                                    {{ $pilih_kate->nama_kategori }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Kategori Wajib Diisi!</div>
                                        </div>
                                        
                                        {{-- <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">Subkategori</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_subkategori">
                                                <option value="">-- Pilih Subkategori --</option>
                                                @foreach($pilihan_subKat as $pilih_sub)

                                                @if ($data_service_subkategori->id_subkategori == $pilih_sub->id)
                                                <option value="{{ $pilih_sub->id }}" selected>
                                                    {{ $pilih_sub->nama_subkategori }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_sub->id}}">
                                                    {{ $pilih_sub->nama_subkategori }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                        </div> --}}

                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic @error('id_services') is-invalid @enderror">Layanan</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_services">
                                                <option value="">-- Pilih Jenis Paket --</option>
                                                @foreach($pilihan_paket as $pilih_paket)
                                                
                                                @if ($data_service_subkategori->id_services == $pilih_paket->id)
                                                <option value="{{ $pilih_paket->id }}" selected>
                                                    {{ $pilih_paket->jenis_paket }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_paket->id }}">
                                                    {{ $pilih_paket->jenis_paket }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Jenis Paket Wajib Diisi!</div>
                                        </div>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
@endsection