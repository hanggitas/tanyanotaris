@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Order</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('order') }}">Order</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Order
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation class="needs-validation" novalidate-->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Order</h4>
                                </div>
                                <div class="card-body">
                                    <form  method="post" action="{{ url('/order/'.$order->id) }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Kode Bayar</label>
                                            <h5>BR-{{$order->id}}</h5>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">User</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_users" disabled>
                                                <option value="">-- Pilih User --</option>
                                                @foreach($pilihan_user as $pilih_user)
                                                
                                                @if ($order->id_users == $pilih_user->id)
                                                <option value="{{ $pilih_user->id }}" selected>
                                                    {{ $pilih_user->email }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_user->id}}">
                                                    {{ $pilih_user->email }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Metode Pembayaran</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_metode_pembayaran" disabled>
                                                <option value="1">Transfer Manual</option>
                                            </select>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                           
                                            <label class="form-label" for="basic-addon-name">Status Pembayaran</label>
                                            <div class="demo-inline-spacing">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pembayaran" id="menunggu" value="Menunggu" {{ old('status_pembayaran',$order->status_pembayaran == "Menunggu")? "checked" : "" }} />
                                                    <label class="form-check-label" for="menunggu">Menunggu</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pembayaran" id="lunas" value="Lunas" {{ old('status_pembayaran',$order->status_pembayaran == 'Lunas') ? "checked" : ""}} />
                                                    <label class="form-check-label" for="inlineRadio2">Lunas</label>
                                                </div>
                                            </div>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <br>
                            
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div> 
@endsection