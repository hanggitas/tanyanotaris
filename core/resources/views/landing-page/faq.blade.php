@extends('landing-page.index')
@section('konten')
<!-- Page title -->
<div class="page-title parallax parallax1">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<div class="page-title-heading">
    						<h1 class="title">FAQ</h1>
    					</div><!-- /.page-title-heading -->
    				</div><!-- /.col-md-12 -->
    			</div><!-- /.row -->
    		</div><!-- /.container -->
    	</div><!-- /.page-title -->

    	<section class="flat-row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-title">
                            <h2>Frequently Asked Questions (FAQ)</h2>
                        </div><!-- /.flat-title -->                 
                        <div class="flat-accordion">
                        @foreach( $faqs as $faq )
                            <div class="flat-toggle">
                                <div class="toggle-title">
                                    {{ $faq->pertanyaan }}
                                    <div class="fw-bold" style="float:right">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="toggle-content">
                                    <p class="fs-6">
                                        {{ $faq->jawaban }}
                                    </p>
                                </div>
                            </div><!-- /.flat-toggle -->
                            @endforeach
                        </div><!-- /.flat-accordion -->
                    </div>
                </div>
            </div><!-- /.container -->
        </section><!-- /.flat-row -->
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#F37A51" fill-opacity="1" d="M0,128L60,144C120,160,240,192,360,192C480,192,600,160,720,170.7C840,181,960,235,1080,250.7C1200,267,1320,245,1380,234.7L1440,224L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>
        </div>
		
@endsection