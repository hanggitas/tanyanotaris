@extends('landing-page.index')
@section('konten')
    <h2 class="title my-5" style="margin-left: 7rem;">Layanan Kami</h2>
        <section>
            <div class="container">
                <div class="d-flex flex-row bd-highlight mb-3 row">
                    <div class="col-md-3  mb-4">
                        <div class="card bd-highlight">
                            <div class="card-body shadow-sm bg-body rounded no-wrap">
                                <h5 class="mb-3">
                                    Kategori
                                </h5>    
                                <ul class="fs-6">
                                    <li class="{{ request()->is('layanan-kami') ? 'active' : '' }}">
                                        <a href="{{ route('layanan') }}">Semua</a>
                                    </li>
                                    <hr/>
                                    @foreach($kategori as $kat)
                                    <li class="{{ request()->is('layanan-kami/*') ? 'active' : '' }}">
                                        <a href="{{ url('/layanan-kami/'.$kat->id.'') }}"> {{ $kat->nama_kategori }}</a>
                                    </li>
                                    <hr/>
                                @endforeach
                                </ul>
                            </div><!-- /.widget-sort-by -->                 
                        </div><!-- /.sidebar -->
                    </div><!-- /.col-md-3 -->
                    <div class="col-md-9">
                        @foreach($services as $service)
                        <div class="card bd-highlight ms-3 mb-3 shadow-sm bg-body rounded p-0" id="layanan-kami{{ $service->id }}">                
                            <div class="card-body m-4">
                                <div class="row">
                                    <div class="d-flex flex-row bd-highlight">
                                        <div class="mx-3">
                                            <img src="{{ asset('aset/images/hand.svg') }}"/>
                                        </div>
                                        <div class="">
                                            <a href="{{ url('/layanan-kami/detail/'.$service->id.'') }}">
                                                <h5 class="card-title">{{ $service->jenis_paket }}</h5>
                                            </a>
                                            <p class="card-text fs-6">Mulai dari</p>
                                            <h4 class="fs-6" style="color: #F37A51;">Rp{{ number_format($service->harga, 0, '', '.') }}</h4>
                                        </div>
                                    </div>                 
                                </div>      
                            </div>
                        </div>
                        @endforeach
                        <div class="d-flex justify-content-center">
                            <div class="pagination flex-wrap mt-2">
                                {{ $services->links() }}
                            </div>
                            <!-- <ul class="flat-pagination">
                                <li class="prev">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#" title="">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
@endsection