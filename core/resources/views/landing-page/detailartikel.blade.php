@extends('landing-page.index')
@section('konten')
    <style lang="scss">
        .ql-align-right {
            text-align: right;
        }
        .ql-align-center {
            text-align: center;
        }
        .ql-align-left {
            text-align: left;
        }
        .ql-align-justify {
            text-align: justify;
        }
        .desc ol li {
            list-style-type: decimal;
            list-style-position: inside;
            text-indent: 3%;
        }
        em {
            font-style: italic;
        }
        </style>
    	<!-- Blog posts -->
    	<section class="blog-posts blog-detail">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-9">
    					<div class="post-wrap detail">
                            
    						<article class="post clearfix">
    							<div class="content-post">
    								<ul class="meta-post">
                                        @foreach($berita as $ber)
                                            </li>
                                                <div class="title-post">
                                                    <h1>{{ $ber->judul}}</h1>
                                                </div><!-- /.title-post -->
                                            </li class="date">
                                                <span>Posted By: Admin</span> |
                                                <span>{{ $ber->created_at }}</span>
                                                <div class="divider h1"></div>
                                            <li class="d-flex justify-content-center mt-3 mb-3">
                                                <div class="featured-post">
                                                @if($ber->gambar == NULL)
                                                    <img src="{{ asset('aset/images/blog/Rectangle 1740.png') }}" alt="" width="350px"> 
                                                @else
                                                    <img src="{{ asset('aset/images/blog/'.$ber->gambar) }}" alt="image">
                                                @endif
                                                </div>
                                            </li>
                                        @endforeach
                                        
    								</ul><!-- /.meta-post -->
    							</div>
                                <div class="desc">
                                    @foreach($berita as $ber)
                                        {!! $ber->deskripsi !!}
                                    @endforeach
                                </div>
    						</article><!-- /.post -->
    					</div><!-- /.post-wrap -->

    				</div><!-- /.col-md-9 -->
                    <div class="col-md-3">
                        <div class="sidebar">
                            <div class="widget widget_categories">
                                <h5 class="widget-title">Cari Artikel</h5>
                                <ul>
                                    <li>
                                        <form action="{{route('artikelKami')}}" method="GET">
                                            <div class="d-flex">
                                                <input type="text" name="search" placeholder="Tentang Artikel" value="{{$search}}">
                                                <button type="submit" class="btn btn-outline-danger">Cari</button>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                            <div class="widget widget_categories">
                                <h5 class="widget-title">kategori Artikel</h5>
                                <ul>
                                    @foreach($kategori as $key => $k)
                                    <li><a href="{{ route('kategoriartikel',$k->id) }}">{{ $k->nama_kategori }}</a></li>
                                    @endforeach
                                </ul>
                            </div><!-- /.widget-categories -->

                            <div class="widget widget-news-latest">
                                <h5 class="widget-title">Artikel Terbaru</h5>
                                <ul class="popular-news clearfix">
                                    @foreach ($new as $n)
                                    <li>
                                        <h6>
                                            <a href="{{ url('/artikel-detail/'.$n->id) }}">{{ $n->judul }}</a>
                                        </h6>
                                        <a class="post_meta">{{ $n->created_at->diffForHumans() }}</a>
                                    </li>
                                    @endforeach
                                </ul><!-- /.popular-news -->
                            </div><!-- /.widget-news-latest -->

                           
                        </div><!-- /.sidebar -->
                    </div><!-- /.col-md-3 -->
    			</div><!-- /.row -->
    		</div><!-- /.container -->
    	</section><!-- /.blog posts -->
@endsection