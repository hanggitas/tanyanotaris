@extends('landing-page.index')
@section('konten')
            <!-- Page title -->
            <div class="page-title parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-heading why">
                                <h1>Mengapa Kami?</h1>
                            </div><!-- /.page-title-heading -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.page-title -->
    
            <section class="flat-row main-shop shop-detail">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6" style="padding-left:70px;">
                            <div class="wrap-flexslider">
                                <div class="inner">
                                    <div class="flexslider style-1 has-relative">
                                            <img src="{{ asset('aset/images/shop/pidio.png') }}" alt="Image">
                                            <div class="flat-icon style-1">
                                                <a href="{{ asset('aset/images/shop/sh-detail/detail-01.jpg') }}" class="zoom-popup"><span
                                                        class="fa fa-search-plus"></span></a>
                                            </div>
                                    </div><!-- /.flexslider -->
                                </div>
                            </div>
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6" style="padding-right:25px;">
                            <div class="product-detail">
                                <div class="inner">
                                    <div class="product style3 flat-carousel-box has-bullets bullet-circle bullet40 clearfix has-arrows"
                                        data-auto="true" data-column="1" data-column2="1" data-column3="1" data-gap="0">
                                        <div class="owl-carousel owl-theme">
                                            <div class="content-detail">
                                                 <br>
                                                <br>
                                                {{--<br> --}}
                                                <h3 style="font-size: 25px;" class="product-title">Tanya Notaris berfokus pada pelayanan kenotariatan dan hukum perdata yang cepat, mudah, dan terjangkau, baik untuk kalangan individu, UMKM maupun company melalui platform digital.</h3>
                                            </div>
                                            <div class="content-detail">
                                                <br>
                                               <br>
                                               <h2>Pelayanan kami yang terintegrasi secara digital akan sangat memudahkan pengguna yang membutuhkan pelayanan hukum dengan cepat. Kami sangat memperhatikan kenyamanan pengguna dari segi waktu, biaya, dan kemudahan akses.</h2>
                                            </div>
                                            <div class="content-detail">
                                                 <br>
                                               {{--<br> --}}
                                               <h2>Tanya Notaris juga menjalankan fungsi penyebaran informasi dan edukasi terkait notaris dan hukum perdata melalui media digital maupun penyelenggaraan event. Kami sangat terbuka terhadap peluang kerjasama dalam penyelenggaraan edukasi terkait legal, seperti legal clinic, legal mentor, maupun legal workshop. </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.product-detail -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <section class="flat-row shop-detail-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-section">
                                <h2 class="title">Kapan Anda Perlu Menggunakan Layanan Kami?</h2>
                            </div>
                            <div class="flat-tabs style-1 ">
                                <div class="inner">
                                    <!-- <ul class="menu-tab">
                                        <li class="active">Description</li>
                                        <li>Additional information</li>
                                        <li>Reviews</li>
                                    </ul> -->
                                    <div class="content-tab">
                                        <div class="content-inner">
                                            <div
                                                class="flat-grid-box col2 border-width border-width-1 has-padding clearfix">
                                                <div class="grid-row image-left clearfix">
                                                    <div class="grid-item">
                                                        <div class="thumb text-center">
                                                            <img src="{{ asset('aset/images/shop/Rectangle 17.png') }}" alt="Image" width="60%">
                                                        </div>
                                                    </div><!-- /.grid-item -->
                                                    <div class="grid-item">
                                                        <div class="text-wrap">
                                                            <h6 class="title">1. Baru Merintis Usaha</h6>
                                                            <p>•	Belum mengetahui tahap dan ketentuan legalitas usaha</p>
                                                            <div class="divider h1"></div>
                                                            <p>•	Membuka usaha bersama partner dan memerlukan panduan atau kepastian legalitas<p>
                                                            <div class="divider h1"></div>
                                                            <p>•	Usaha anda baru masuk ke dalam kategori start-up<p>
                                                        </div>
                                                    </div>
                                                </div><!-- /.grid-row -->
    
                                                <div class="grid-row image-right padding-bottom-48 clearfix">
                                                    <div class="grid-item">
                                                        <div class="text-wrap">
                                                            <h6 class="title">2. Sedang Ditahap Scale-Up</h6>
                                                            <p>•	Ingin mencari investor atau pendanaan dan memerlukan bantuan legal</p>
                                                            <div class="divider h1"></div>
                                                            <p>•	Sudah mulai merekrut karyawan dalam jumlah banyak dan memerlukan bantuan legal terkait kontrak</p>
                                                            <div class="divider h1"></div>
                                                            <p>•	Mulai melakukan kerjasama usaha skala besar dengan klien</p>
                                                        </div>
                                                    </div>
                                                    <div class="grid-item">
                                                        <div class="thumb text-center">
                                                            <img src="{{ asset('aset/images/shop/Rectang.png') }}" alt="Image" width="60%">
                                                        </div>
                                                    </div><!-- /.grid-item -->
                                                </div><!-- /.grid-row -->

                                                <div class="grid-row image-left clearfix">
                                                    <div class="grid-item">
                                                        <div class="thumb text-center">
                                                            <img src="{{ asset('aset/images/shop/Rectangle 17.png') }}" alt="Image" width="60%">
                                                        </div>
                                                    </div><!-- /.grid-item -->
                                                    <div class="grid-item">
                                                        <div class="text-wrap">
                                                            <h6 class="title">3. Membutuhkan Efisiensi</h6>
                                                            <p>•	Perlu untuk menggunakan layanan legal tetapi tidak ruitn</p>
                                                            <div class="divider h1"></div>
                                                            <p>•	Perlu membuat perjanjian dan kontrak dalam skala besar<p>
                                                            <div class="divider h1"></div>
                                                            <p>•	Membutuhkan tim legal outsorced<p>
                                                        </div>
                                                    </div>
                                                </div><!-- /.grid-row -->
                                            </div><!-- /.flat-grid-box -->
                                        </div><!-- /.content-inner -->
                                        <div class="content-inner">
                                            <div class="inner max-width-40">
                                                <table>
                                                    <tr>
                                                        <td>Weight</td>
                                                        <td>1.73 kg</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dimensions</td>
                                                        <td>100 x 37 x 100 cm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Materials</td>
                                                        <td>80% cotton, 20% linen</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Size</td>
                                                        <td>One Size, XL, L, M, S</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div><!-- /.content-inner -->
                                        <div class="content-inner">
                                            <div class="inner max-width-83 padding-top-33">
                                                <ol class="review-list">
                                                    <li class="review">
                                                        <div class="thumb">
                                                            <img src="{{ asset('aset/images/avatar-1.png') }}" alt="Image">
                                                        </div>
                                                        <div class="text-wrap">
                                                            <div class="review-meta">
                                                                <h5 class="name">Sophia Rosla</h5>
                                                                <div class="flat-star style-1">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star-half-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="review-text">
                                                                <p>I wanted to thank you so much for the rug we have
                                                                    received it really is beautiful and expertly made. I
                                                                    will be recommending you to all our friends.</p>
                                                            </div>
                                                        </div>
                                                    </li><!--  /.review    -->
                                                    <li class="review">
                                                        <div class="thumb">
                                                            <img src="{{ asset('aset/images/avatar.png') }}" alt="Image">
                                                        </div>
                                                        <div class="text-wrap">
                                                            <div class="review-meta">
                                                                <h5 class="name">Jayne Haughton</h5>
                                                                <div class="flat-star style-1">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star-half-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="review-text">
                                                                <p class="line-height-28">Customer service is very important
                                                                    part of the buying experience to us and we must say that
                                                                    we have found Utility's to be impressive - we will
                                                                    certainly look to buy again in future.</p>
                                                            </div>
                                                        </div>
                                                    </li><!--  /.review    -->
                                                </ol><!-- /.review-list -->
                                                <div class="comment-respond review-respond" id="respond">
                                                    <div class="comment-reply-title margin-bottom-14">
                                                        <h5>Write a review</h5>
                                                        <p>Your email address will not be published. Required fields are
                                                            marked *</p>
                                                    </div>
                                                    <form novalidate="" class="comment-form review-form" id="commentform"
                                                        method="post" action="#">
                                                        <p class="flat-star style-2">
                                                            <label>Rating*:</label>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </p>
                                                        <p class="comment-form-comment">
                                                            <label>Review*</label>
                                                            <textarea class="" tabindex="4" name="comment"
                                                                required> </textarea>
                                                        </p>
                                                        <p class="comment-name">
                                                            <label>Name*</label>
                                                            <input type="text" aria-required="true" size="30" value=""
                                                                name="name" id="name">
                                                        </p>
                                                        <p class="comment-email">
                                                            <label>Email*</label>
                                                            <input type="email" size="30" value="" name="email" id="email">
                                                        </p>
                                                        <p class="comment-form-notify clearfix">
                                                            <input type="checkbox" name="check-notify" id="check-notify">
                                                            <label for="check-notify">Notify me of new posts by
                                                                email</label>
                                                        </p>
                                                        <p class="form-submit">
                                                            <button class="comment-submit">Submit</button>
                                                        </p>
                                                    </form>
                                                </div><!-- /.comment-respond -->
                                            </div>
                                        </div><!-- /.content-inner -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- /.shop-detail-content -->

            <section class="flat-row row-icon-box style-3">
                <div class="container">
                    <div class="col-md-12">
                        <div class="title-section margin-bottom-55">
                            <h2 class="title">Tentang Tanya Notaris</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flat-icon-box icon-left  style-2 clearfix">
                                <div class="inner flat-content-box" data-margin="0 0 0 20px" data-mobilemargin="0 0 0 0">
                                    <div class="icon-wrap">
                                        <img src="{{ asset('aset/images/shop/Rectangle 1757.png') }}" alt="image" style="margin-left: 20px">
                                    </div>
                                    <div class="text-wrap">
                                        {{-- <h5 class="heading"><a href="#">Free Shipping</a></h5> --}}
                                        <p class="desc">Tanya Notaris merupakan platform digital yang bergerak khusus di pelayanan kenotariatan dan hukum perdata.
                                            Melayani jasa legal untuk individu maupun instansi. Kami berfokus pada kualitas dan kenyamanan pelayanan serta memastikan biaya terjangkau untuk semua kalangan</p>
                                    </div>                                
                                </div>
                            </div>
                        </div><!-- /.col-md-4 -->
                        <div class="col-md-6">
                            <div class="flat-icon-box icon-left style-2 clearfix">
                                <div class="inner flat-content-box" data-margin="0 0 0 20px" data-mobilemargin="0 0 0 0">
                                    <div class="icon-wrap">
                                        <img src="{{ asset('aset/images/shop/Rectangle 1758.png') }}" alt="image" style="margin-left: 10px">
                                    </div>
                                    <div class="text-wrap">
                                        {{-- <h5 class="heading"><a href="#">Cash On Delivery</a></h5> --}}
                                        <p class="desc">Tanya Notaris juga bergerak dalam penyebaran informasi dan edukasi terkait dengan kenotariatan dan hukum perdata. Tanya Notaris siap menjadi garda terdepan agar akses terhadap informasi, edukasi, dan jasa layanan hukum yang mudah dan terjangkau harus dimiliki semua kalangan.</p>
                                    </div>                                    
                                </div>
                            </div>
                        </div><!-- /.col-md-4 -->
                    </div>
                </div>
            </section>

            

            {{-- <section class="flat-row shop-related">
                <div class="container">
                       <div class="col-md-12">
                            <div class="title-section margin-bottom-55">
                                <h2 class="title">Tentang Tanya Notaris</h2>
                            </div>
                        <div class="row" style="padding-left: 100px">
                            <div class=" mb-3 mr-3 col-6" style="max-width: 540px;">
                                <div class="row g-0">
                                    <div class="col-md-4">
                                        <img src="{{ asset('aset/images/shop/Rectangle 1757.png') }}" class="img-fluid rounded-start" alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <!-- <h5 class="card-title">Card title</h5> -->
                                            <p>Tanya Notaris merupakan platform digital yang bergerak khusus di pelayanan kenotariatan dan hukum perdata.</p>
                                            <p> melayani jasa legal untuk individu maupun instansi. Kami berfokus pada kualitas dan kenyamanan pelayanan serta memastikan biaya terjangkau untuk semua kalangan
                                            </p>
                                            <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins
                                                    ago</small>
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3 mr-3 col-6" style="max-width: 540px; padding-left: 50px">
                                <div class="row g-0">
                                    <div class="col-md-4">
                                        <img src="{{ asset('aset/images/shop/Rectangle 1758.png') }}" class="img-fluid rounded-start" alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <!-- <h5 class="card-title">Card title</h5> -->
                                            <p class="card-text" style="padding-left: 5px; padding-bottom: 50px;">Tanya Notaris juga bergerak dalam penyebaran informasi dan edukasi terkait dengan kenotariatan dan hukum perdata. Tanya Notaris siap menjadi garda terdepan agar akses terhadap informasi, edukasi, dan jasa layanan hukum yang mudah dan terjangkau harus dimiliki semua kalangan.
                                            </p>
                                            <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins
                                                    ago</small>
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section> --}}
            <br>
            <section class="flat-row  no-padding">
                <div class="container-fluid">
                    <div class="row equal sm-equal-auto">
                        <div class="col-md-12">
                            <div class="title-section">
                                <h2 class="title">Mengapa Layanan Kami Terpercaya?</h2>
                            </div>
                            {{-- <h2>Mengapa Layanan Kami Terpercaya?</h2> --}}
                            <div class="product-content clearfix">
                               
                                <div class="divider h0"></div>
                                <div class="flat-content-box clearfix" data-margin="0px 0px 0 0px"
                                    data-mobilemargin="0 0 0 0">
                                    
                                    
                                    {{-- <br>
                                    <p>Mengapa para perusahaan ternama, UMKM dan Startup menyukai kami</p>
                                    <br> --}}
                                    <br>
                                    <div class="product style3 flat-carousel-box has-bullets bullet-circle bullet40 clearfix"
                                        data-auto="false" data-column="3" data-column2="1" data-column3="1" data-gap="0">
    
                                        <div class="owl-carousel owl-theme">
                                            <div class="product-item">
                                                <br>
    
                                                <svg width="96" height="96" viewBox="0 0 96 96" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <rect width="96" height="96" rx="12" fill="url(#paint0_linear_183_0)" />
                                                    <path
                                                        d="M72.9475 46.6759C73.1648 47.3268 72.6799 47.9998 71.9938 47.9998H52.665C52.4638 47.9998 52.2777 47.9414 52.1208 47.8398C51.8431 47.6607 51.6589 47.3489 51.6589 46.9937V29.707H71.9938C72.6799 29.707 73.1648 30.3801 72.9475 31.031L70.4465 38.535C70.3771 38.7422 70.3771 38.9646 70.4465 39.1718L72.9475 46.6759Z"
                                                        fill="white" />
                                                    <path
                                                        d="M51.6584 46.577H62.4308C63.5534 46.577 64.4633 45.667 64.4633 44.5444V29.707H51.6584V46.577Z"
                                                        fill="#C7C7C7" />
                                                    <path
                                                        d="M61.9276 44.3409C61.823 44.4445 61.6952 44.525 61.5503 44.5753L52.1208 47.8398C51.8431 47.6607 51.6589 47.3489 51.6589 46.9938V44.3408H61.9276V44.3409Z"
                                                        fill="#777777" />
                                                    <path
                                                        d="M61.2213 26.0488C61.777 26.0488 62.2274 26.4992 62.2274 27.0549V43.6253C62.2274 43.901 62.1147 44.1575 61.9276 44.3416H44.9518V26.0488H61.2213Z"
                                                        fill="white" />
                                                    <path
                                                        d="M26.2521 52.8779V68.2317H24.0061C23.4504 68.2317 23 67.7814 23 67.2257V53.884C23 53.3283 23.4504 52.8779 24.0061 52.8779H26.2521Z"
                                                        fill="white" />
                                                    <path
                                                        d="M23 60.5879V67.2262C23 67.7818 23.4504 68.2322 24.0061 68.2322H26.2521V62.5152C24.986 62.0976 23.8779 61.453 23 60.5879Z"
                                                        fill="#C7C7C7" />
                                                    <path
                                                        d="M43.9351 73H41.9025C41.3413 73 40.8862 72.545 40.8862 71.9838V24.0163C40.8862 23.455 41.3413 23 41.9025 23H43.9351C44.4963 23 44.9514 23.455 44.9514 24.0163V71.9838C44.9513 72.545 44.4963 73 43.9351 73Z"
                                                        fill="#7C589F" />
                                                    <path
                                                        d="M43.9351 73.0001H41.9025C41.3413 73.0001 40.8862 72.5451 40.8862 71.9839V49.4229H44.9513V71.9838C44.9513 72.5451 44.4963 73.0001 43.9351 73.0001Z"
                                                        fill="#7C589F" />
                                                    <path
                                                        d="M51 56.0285H49.0162C50.1634 56.0285 51.0886 55.0782 51.0474 53.9219C51.0082 52.8205 50.0695 51.9635 48.9675 51.9635H40.8861V47.8984H38.1829C37.2992 47.8984 36.4396 48.1864 35.7342 48.7188L31.3097 52.0578C30.6043 52.5901 29.7446 52.8781 28.8609 52.8781H26.252V68.2319L46.935 68.2236C48.037 68.2236 48.9757 67.3666 49.0148 66.2652C49.056 65.1089 48.1308 64.1586 46.9836 64.1586H48.9674C50.0694 64.1586 51.0081 63.3016 51.0473 62.2002C51.0884 61.0438 50.1632 60.0936 49.0161 60.0936H51.0486C52.1958 60.0936 53.121 59.1433 53.0798 57.9869C53.0407 56.8855 52.1021 56.0285 51 56.0285Z"
                                                        fill="#EBEBEB" />
                                                    <path
                                                        d="M51.0001 56.0279H49.0164C50.1635 56.0279 51.0887 55.0776 51.0475 53.9213C51.0083 52.8199 50.0697 51.9629 48.9676 51.9629H46.7943C45.6171 55.1437 42.9004 58.3047 39.0897 60.5048C34.6505 63.0678 29.8245 63.6931 26.2522 62.5144V68.2313L46.9352 68.223C48.0373 68.223 48.9759 67.366 49.0151 66.2646C49.0562 65.1083 48.131 64.158 46.9839 64.158H48.9677C50.0698 64.158 51.0084 63.301 51.0476 62.1996C51.0887 61.0433 50.1635 60.093 49.0165 60.093H51.049C52.1961 60.093 53.1213 59.1427 53.0801 57.9863C53.0409 56.885 52.1022 56.0279 51.0001 56.0279Z"
                                                        fill="white" />
                                                    <defs>
                                                        <linearGradient id="paint0_linear_183_0" x1="0" y1="0" x2="96"
                                                            y2="96" gradientUnits="userSpaceOnUse">
                                                            <stop stop-color="#F37A51" />
                                                            <stop offset="1" stop-color="#D76A45" />
                                                        </linearGradient>
                                                    </defs>
                                                </svg>
                                                <div class="product-thumb clearfix">
                                                    <!-- <a href="#">
                                                        <img src="images/shop/flag-01.png" alt="image">
                                                    </a>
                                                    <span class="new">New</span> -->
                                                </div>
    
    
                                                <div class="product-info clearfix">
                                                    <span class="product-title"></span>
                                                    <div class="price">
                                                        <span class="amount">Proses pengumpulan data-data unutuk ketentuan dan persyaratan administrasi legal juga dilakukan langsung dengan administrator Tanya Notaris, tidak ada proses input secara online di website, sehingga aman dari kebocoran dan pencurian data.</span>
                                                        <!-- <ins></ins> -->
                                                    </div>
                                                    <!-- <ul class="flat-color-list width-14">
                                                        <li>
                                                            <a href="#" class="red"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="blue"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="black"></a>
                                                        </li>
                                                    </ul> -->
                                                </div>
                                                <!-- <div class="add-to-cart text-center">
                                                    <a href="#">ADD TO CART</a>
                                                </div> -->
                                                <!-- <a href="#" class="like"><i class="fa fa-heart-o"></i></a> -->
                                            </div><!-- /.product-item -->
                                            <div class="product-item">
                                                <br>
                                                <svg width="96" height="96" viewBox="0 0 96 96" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <rect width="96" height="96" rx="12" fill="#603D84" />
                                                    <path
                                                        d="M57.0689 34.019L51.1643 33.166C50.7226 33.102 50.3407 32.8262 50.1431 32.4285L47.5025 27.1092C47.0051 26.1068 45.5676 26.1068 45.0702 27.1092L42.4296 32.4285C42.2322 32.8262 41.8501 33.102 41.4084 33.166L35.5037 34.019C34.3913 34.1798 33.9472 35.5389 34.7521 36.3186L39.0249 40.4594C39.3445 40.769 39.4904 41.2154 39.415 41.6526L38.4063 47.4992C38.2163 48.6006 39.3792 49.4405 40.3742 48.9206L45.6555 46.1603C46.0506 45.9539 46.5227 45.9539 46.9178 46.1603L52.1989 48.9206C53.1939 49.4405 54.3568 48.6004 54.1668 47.4992L53.1581 41.6526C53.0827 41.2154 53.2286 40.769 53.5482 40.4594L57.821 36.3186C58.6255 35.5387 58.1814 34.1795 57.0689 34.019V34.019Z"
                                                        fill="#F9D335" />
                                                    <path
                                                        d="M43.1816 30.912L42.429 32.4293C42.2322 32.8253 41.8499 33.1016 41.4075 33.166L35.5044 34.0181C34.3904 34.1793 33.9481 35.5399 34.7517 36.3184L39.0246 40.4604C39.3443 40.769 39.4902 41.2156 39.416 41.6532L38.918 44.5314L37.2644 43.6656C36.8706 43.4584 36.3982 43.4584 36.0022 43.6656L30.7219 46.4264C29.7261 46.9443 28.5635 46.1062 28.7534 45.0035L29.7631 39.1578C29.8372 38.7202 29.6914 38.2735 29.3717 37.965L25.1011 33.8223C24.2951 33.0419 24.7399 31.6834 25.8515 31.5222L31.757 30.6703C32.1969 30.6059 32.5792 30.3294 32.7784 29.9336L35.4186 24.6126C35.9164 23.6109 37.3524 23.6109 37.8502 24.6126L40.4904 29.9336C40.6895 30.3296 41.0694 30.6059 41.5118 30.6703L43.1816 30.912Z"
                                                        fill="#F2C300" />
                                                    <path
                                                        d="M63.2005 37.9645C62.8809 38.2731 62.735 38.7197 62.8091 39.1573L63.8188 45.003C64.0087 46.106 62.8462 46.9439 61.8503 46.4259L56.57 43.6652C56.1741 43.4579 55.7016 43.4579 55.3078 43.6652L53.6542 44.5286L53.1586 41.6528C53.0821 41.2154 53.2281 40.7688 53.5476 40.46L57.8205 36.3182C58.6265 35.5399 58.1818 34.1793 57.0679 34.0179L51.1645 33.166C50.7222 33.1016 50.3401 32.8253 50.1431 32.4293L49.3904 30.912L51.0602 30.6703C51.5026 30.6059 51.8847 30.3294 52.0817 29.9336L54.7218 24.6126C55.2198 23.6109 56.658 23.6109 57.1535 24.6126L59.796 29.9336C59.9928 30.3296 60.3751 30.6059 60.8172 30.6703L66.7206 31.5222C67.8322 31.6834 68.2769 33.0419 67.4733 33.8223L63.2005 37.9645Z"
                                                        fill="#F2C300" />
                                                    <path
                                                        d="M62.4953 54.0452C62.024 54.0027 61.5502 53.9803 61.0762 53.9803C59.3814 53.9803 57.6817 54.252 56.0469 54.8004C55.3976 55.0171 54.7658 55.274 54.154 55.573L44.2683 60.3811L27.7264 51.4579C27.2275 51.1737 26.6859 51.0391 26.1495 51.0391C25.0513 51.0391 23.9833 51.6074 23.3967 52.6218C22.5241 54.1299 23.1695 55.726 24.5674 56.924L40.9215 71.8193C40.9314 71.8268 40.9416 71.8367 40.9516 71.8443C42.5486 73.0556 44.6797 73.3197 46.56 72.6145L59.7776 67.652C61.2442 67.161 62.7812 67.0539 64.2504 67.298C65.1077 67.44 65.9428 67.7018 66.7325 68.0782V55.0298C65.3659 54.5063 63.9394 54.1773 62.4953 54.0452V54.0452Z"
                                                        fill="white" />
                                                    <path
                                                        d="M72.9999 52.9653V69.3201C72.9999 69.9439 72.4926 70.4485 71.865 70.4485H67.8679C67.2403 70.4485 66.7329 69.9441 66.7329 69.3201V52.9653C66.7329 52.3435 67.2403 51.8369 67.8679 51.8369H71.8652C72.4926 51.8369 72.9999 52.3435 72.9999 52.9653V52.9653Z"
                                                        fill="#C7C7C7" />
                                                    <path
                                                        d="M73 52.9653V69.3201C73 69.9439 72.4926 70.4485 71.865 70.4485H70.9745C70.3468 70.4485 69.8395 69.9441 69.8395 69.3201V52.9653C69.8395 52.3435 70.3468 51.8369 70.9745 51.8369H71.865C72.4926 51.8369 73 52.3435 73 52.9653Z"
                                                        fill="white" />
                                                    <path
                                                        d="M62.4953 54.0453C62.024 54.0029 61.5502 53.9805 61.0762 53.9805C59.3814 53.9805 57.6817 54.2522 56.0469 54.8005C55.3976 55.0173 54.7659 55.2741 54.154 55.5732L44.2684 60.3812L42.7014 61.1439C41.6761 61.6574 41.0842 62.6892 41.0842 63.7562C41.0842 64.1998 41.1846 64.651 41.4002 65.0722C42.1323 66.5154 43.8999 67.0961 45.3515 66.3682L57.4881 60.4661C57.4881 60.4661 61.0045 58.8973 65.3184 59.1983L66.7323 59.3112V55.03C65.3659 54.5064 63.9394 54.1774 62.4953 54.0453V54.0453Z"
                                                        fill="#EBEBEB" />
                                                    <path
                                                        d="M47.9112 27.9341L45.6811 32.4286C45.4839 32.8272 45.1003 33.103 44.6604 33.1655L38.7546 34.0187C37.6428 34.1803 37.1985 35.5399 38.0026 36.3177L42.2764 40.4589C42.595 40.7691 42.7423 41.2151 42.6665 41.6525L41.6566 47.5001C41.6132 47.7565 41.6414 48 41.7281 48.2133L40.3735 48.92C39.3788 49.4414 38.2171 48.6011 38.4057 47.5001L39.4156 41.6525C39.4915 41.2151 39.3441 40.7691 39.0255 40.4589L34.7517 36.3177C33.9477 35.5399 34.3919 34.1803 35.5037 34.0187L41.4095 33.1655C41.8494 33.103 42.233 32.8272 42.4303 32.4286L45.07 27.1088C45.5684 26.1069 47.0053 26.1069 47.5016 27.1088L47.9112 27.9341Z"
                                                        fill="#F9E27D" />
                                                    <path
                                                        d="M53.5483 40.4589C53.2275 40.7691 53.0823 41.2151 53.1582 41.6525L54.166 47.5001C54.3567 48.6011 53.1929 49.4414 52.1981 48.92L50.8457 48.2133C50.9303 48 50.9606 47.7565 50.9151 47.5001L49.9073 41.6525C49.8315 41.2151 49.9767 40.7691 50.2974 40.4589L54.5691 36.3177C55.3753 35.5399 54.931 34.1803 53.8192 34.0187L47.9134 33.1655C47.4713 33.103 47.0899 32.8272 46.8927 32.4286L44.6604 27.9341L45.07 27.1088C45.5685 26.1069 47.0054 26.1069 47.5017 27.1088L50.1435 32.4286C50.3408 32.8272 50.7222 33.103 51.1643 33.1655L57.0701 34.0187C58.1819 34.1803 58.6262 35.5399 57.8199 36.3177L53.5483 40.4589Z"
                                                        fill="#F2C300" />
                                                    <path
                                                        d="M58.5162 64.0018L58.2912 64.0927C57.8476 64.272 57.6343 64.7746 57.8144 65.2157C57.9514 65.5505 58.2756 65.7532 58.6178 65.7532C58.7266 65.7532 58.8372 65.7328 58.944 65.6897L59.169 65.5988C59.6126 65.4195 59.8259 64.9168 59.6458 64.4758C59.4652 64.0349 58.9596 63.8234 58.5162 64.0018V64.0018Z"
                                                        fill="#603D84
                                                        " />
                                                    <path
                                                        d="M55.2795 65.3084L53.2713 66.1192C52.8276 66.2983 52.6142 66.8009 52.7943 67.242C52.9746 67.6835 53.4804 67.895 53.9236 67.7162L55.9318 66.9054C56.3754 66.7264 56.5889 66.2237 56.4088 65.7827C56.2287 65.3414 55.7227 65.13 55.2795 65.3084Z"
                                                        fill="#7C589F" />
                                                    <path
                                                        d="M28.7679 38.5831C28.8828 38.694 28.9353 38.855 28.9086 39.0114L27.8987 44.8577C27.5879 46.6622 29.4909 48.0386 31.1253 47.1888L36.4053 44.4281C36.5484 44.3535 36.7181 44.3533 36.8602 44.4279L37.9573 45.0023L37.5516 47.3536C37.2406 49.1558 39.1441 50.5366 40.7771 49.6839L46.0587 46.9231C46.2011 46.8486 46.3712 46.8486 46.5138 46.9231L51.7954 49.6839C53.4302 50.5373 55.3315 49.1542 55.0207 47.3538L54.6148 45.0008C55.5892 44.5533 55.7925 44.2331 56.1661 44.4281L61.4481 47.1897C62.2034 47.5827 63.1009 47.5172 63.7903 47.0184C64.4788 46.5202 64.8169 45.6922 64.673 44.8573L63.6637 39.0142C63.6368 38.8552 63.6893 38.694 63.8041 38.5831C63.8046 38.5826 63.805 38.5822 63.8056 38.5818L68.079 34.4389C69.3956 33.1603 68.6672 30.9337 66.845 30.6694L60.9429 29.8176C60.7826 29.7944 60.6443 29.695 60.5734 29.5524L57.9315 24.2324C57.1182 22.5891 54.7606 22.5895 53.9447 24.2316L51.3047 29.552C51.2337 29.695 51.0954 29.7944 50.9357 29.8176L49.8891 29.9691L48.2801 26.7281C47.9028 25.9682 47.1391 25.4959 46.2867 25.4959C46.265 25.5022 44.9261 25.4532 44.293 26.7279L42.6841 29.9691L41.6377 29.8174C41.4782 29.7944 41.3393 29.6935 41.2682 29.5524L38.6276 24.2307C37.8119 22.5902 35.4574 22.5898 34.6412 24.2314L32.0032 29.5481C31.93 29.6935 31.7908 29.7944 31.6328 29.8174L25.7266 30.6696C23.9046 30.9337 23.1769 33.162 24.4959 34.4393L28.7679 38.5831ZM52.8591 30.3151L55.499 24.9947C55.6789 24.633 56.1977 24.6332 56.3763 24.9943L59.0188 30.3158C59.3424 30.9667 59.9679 31.4179 60.693 31.5234L66.5955 32.3752C66.9961 32.4331 67.1588 32.9231 66.8682 33.2053L62.5952 37.3476C62.0723 37.8533 61.8326 38.583 61.9546 39.3031L62.9643 45.1488C63.0107 45.4181 62.8438 45.5716 62.7701 45.625C62.6958 45.6786 62.4968 45.7902 62.2539 45.6636L56.9736 42.9028C56.3265 42.5646 55.5532 42.5618 54.9048 42.9026L54.3072 43.2146C54.0251 41.489 53.8968 41.3246 54.1527 41.0777C54.1592 41.0714 58.3526 37.0069 58.3526 37.0069C59.773 35.728 59.0427 33.4337 57.1923 33.1653C51.0806 32.2534 51.256 32.3797 51.0431 32.1994C50.906 32.0826 50.9112 32.0018 50.6955 31.5943C50.8888 31.5282 52.2165 31.6081 52.8591 30.3151V30.3151ZM43.207 32.8102L45.8478 27.4909C46.0273 27.1291 46.5459 27.1291 46.7253 27.4909C49.5025 32.977 49.3319 32.977 49.8473 33.4518C50.5185 34.0711 51.3713 34.0387 51.6407 34.1058L56.9446 34.8722C56.9446 34.8722 56.9446 34.8722 56.9448 34.8724C57.3453 34.9301 57.5074 35.4194 57.2157 35.7013L53.9915 38.8265C53.1124 39.6851 52.314 40.2074 52.2765 41.3502C52.2614 41.7982 52.2696 41.4881 53.3123 47.6452C53.3807 48.0429 52.9622 48.345 52.6025 48.1582L47.3213 45.3977C46.672 45.0581 45.899 45.0596 45.2523 45.3975L39.9707 48.158C39.6118 48.3454 39.192 48.0438 39.2607 47.6449C39.2609 47.6432 39.2615 47.6398 39.2624 47.635C39.2618 47.6387 39.2613 47.6415 39.2609 47.6436C39.2615 47.6402 39.262 47.6374 39.2624 47.6344C39.2624 47.6339 39.2626 47.6335 39.2626 47.6329C39.2629 47.632 39.2628 47.6311 39.2631 47.6303C39.2635 47.6277 39.2639 47.6251 39.2646 47.6219C39.2641 47.624 39.2639 47.626 39.2635 47.6279C39.2644 47.6232 39.2652 47.6184 39.2659 47.6141C39.2665 47.6098 39.2674 47.6051 39.2685 47.5997C39.2685 47.6003 39.2683 47.6008 39.2683 47.6014C39.2685 47.6006 39.2685 47.5997 39.2687 47.5986C39.2691 47.596 39.2696 47.5934 39.27 47.5909C39.2696 47.5928 39.2694 47.5943 39.2691 47.5962C39.2713 47.5837 39.2739 47.5684 39.2772 47.5501C39.3318 47.2345 39.5459 45.9943 40.2708 41.796C40.3887 41.0996 40.1629 40.3582 39.63 39.8424L36.5627 36.8701C35.3269 35.6681 35.1078 35.5875 35.2335 35.2033C35.2617 35.1169 35.3568 34.9116 35.6284 34.8722L41.533 34.0191C42.2573 33.9146 42.8832 33.4626 43.207 32.8102V32.8102ZM25.9761 32.3754L31.8834 31.523C32.6029 31.4179 33.2275 30.9678 33.5558 30.3151L36.1958 24.9945C36.3763 24.6315 36.8936 24.6332 37.0728 24.9939L39.7154 30.3194C40.0422 30.9684 40.6668 31.4185 41.387 31.5232L41.8768 31.5941C41.6962 31.8835 41.665 32.2577 41.2832 32.3131L35.3794 33.1653C34.5352 33.2874 33.8475 33.8644 33.5842 34.6707C33.0522 36.3011 34.4875 37.2101 34.5751 37.3508C38.6437 41.3229 38.6235 41.1401 38.5604 41.5071L38.2657 43.2153L37.6697 42.9031C37.023 42.5631 36.2486 42.5622 35.5987 42.9022L30.3201 45.6623C30.0759 45.7894 29.8763 45.6782 29.8022 45.6246C29.7283 45.5711 29.5614 45.4177 29.6078 45.1486L30.6179 39.3001C30.7397 38.5822 30.4996 37.8516 29.9771 37.3474L25.7061 33.2051C25.4154 32.9235 25.5745 32.4333 25.9761 32.3754V32.3754Z"
                                                        fill="#7C589F" />
                                                    <defs>
                                                        <linearGradient id="paint0_linear_183_0" x1="0" y1="0" x2="96"
                                                            y2="96" gradientUnits="userSpaceOnUse">
                                                            <stop stop-color="#7C589F" />
                                                            <stop offset="1" stop-color="#603D84" />
                                                        </linearGradient>
                                                    </defs>
                                                </svg>
    
                                                <div class="product-thumb clearfix">
                                                    <!-- <a href="#">
                                                        <img src="images/shop/sh-3/14.jpg" alt="image">
                                                    </a> -->
                                                    <!-- <span class="new sale">Sale</span> -->
                                                </div>
                                                <div class="product-info clearfix">
                                                    <span class="product-title"></span>
                                                    <div class="price">
                                                        <!-- <del>
                                                            <span class="regular">$20.00</span>
                                                        </del> -->
                                                        <!-- <ins></ins> -->
                                                        <span class="amount">Layanan kami dikerjakan oleh notaris resmi dan ahli hukum perdata yang berpengalaman, sehingga hasil akhir pekerjaan memiliki kualitas yang baik dan professional. </span>
    
                                                    </div>
                                                </div>
                                                <!-- <div class="add-to-cart text-center">
                                                    <a href="#">ADD TO CART</a>
                                                </div>
                                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a> -->
                                            </div><!-- /.product-item -->
                                            <div class="product-item">
                                                <br>
                                                <svg width="96" height="96" viewBox="0 0 96 96" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <rect width="96" height="96" rx="12" fill="#333333" />
                                                    <path
                                                        d="M62.295 73.0002L62.0773 72.4134C61.8056 71.6786 61.2772 71.073 60.5959 70.7131L47.3344 63.7C46.0148 63.0024 44.9842 61.8419 44.434 60.4309L38.9539 46.3999C38.4228 45.0446 39.0739 43.5064 40.4037 42.9719C40.4101 42.9694 40.4164 42.9669 40.4228 42.9644C41.6833 42.4683 43.109 43.0517 43.6839 44.3061L48.3743 54.5308C48.7366 55.3227 49.3539 55.966 50.1224 56.3547L58.761 60.7246C59.6999 61.1986 60.8368 60.857 61.3724 59.9384C61.6999 59.3747 61.7327 58.7189 61.5198 58.1544C61.3479 57.6981 61.0162 57.301 60.5515 57.0621L53.0547 53.2071C52.0504 52.6901 51.6018 51.4714 52.0239 50.4061C52.2573 49.8145 52.7123 49.3864 53.2509 49.1745C53.7736 48.9689 54.3751 48.9681 54.9279 49.228L67.114 54.9327C67.2039 54.9735 67.2935 55.0182 67.3816 55.0634C69.557 56.1784 71.0596 58.3164 71.5124 60.7614L73.0002 68.787"
                                                        fill="white" />
                                                    <path
                                                        d="M62.3462 70.7129L49.0847 63.6998C47.765 63.0022 46.7345 61.8417 46.1843 60.4307L40.7042 46.3997C40.1731 45.0444 40.8242 43.5062 42.154 42.9717C42.1604 42.9692 42.1667 42.9667 42.1731 42.9642C42.1897 42.9577 42.2064 42.9534 42.223 42.9472C41.6581 42.7388 41.0222 42.7282 40.4228 42.9642C40.4164 42.9667 40.4101 42.9692 40.4037 42.9717C39.074 43.5062 38.4228 45.0444 38.9539 46.3997L44.434 60.4306C44.9843 61.8416 46.0148 63.0021 47.3344 63.6997L60.5959 70.7128C61.2773 71.0726 61.8057 71.6784 62.0773 72.4131L62.2949 72.9999L63.8216 72.399C63.5486 71.6709 63.0231 71.0703 62.3462 70.7129Z"
                                                        fill="#C7C7C7" />
                                                    <path
                                                        d="M56.9288 38.569C56.9253 38.5737 56.9206 38.5773 56.9172 38.582C56.1074 39.4081 54.8076 39.457 53.9395 38.6881L46.8674 32.4156C46.3201 31.9305 45.6259 31.6479 44.9001 31.617L36.7405 31.2665C35.8537 31.2284 35.0883 31.8924 34.9821 32.7924C34.918 33.3443 35.1128 33.8628 35.4675 34.2252C35.7545 34.5184 36.1454 34.7091 36.5853 34.7318L43.6877 35.0847C44.6398 35.1323 45.3971 35.9191 45.4286 36.8918C45.4473 37.4318 45.2384 37.9229 44.8931 38.2769C44.557 38.619 44.0915 38.8312 43.5769 38.8241L34.5212 38.7132H34.52L32.219 38.6846C32.1362 38.6846 32.0521 38.681 31.9682 38.6774C29.9087 38.5772 28.0255 37.4496 26.8517 35.7153L23 30.0222L24.3733 28.6192L29.8726 23L30.239 23.3791C30.6964 23.8523 31.3101 24.136 31.9577 24.1753L44.5886 24.9501C45.8452 25.0264 47.0331 25.564 47.9339 26.4639L56.8985 35.4053C57.7654 36.2684 57.7794 37.688 56.9288 38.569Z"
                                                        fill="white" />
                                                    <path
                                                        d="M34.52 38.7132L32.219 38.6846C32.1362 38.6846 32.0521 38.681 31.9682 38.6774C29.9087 38.5772 28.0255 37.4496 26.8517 35.7152L23 30.0221L24.3733 28.6191C25.1166 30.4382 28.2601 37.185 34.52 38.7132Z"
                                                        fill="#C7C7C7" />
                                                </svg>
    
                                                <div class="product-thumb clearfix">
                                                    <!-- <a href="#">
                                                        <img src="images/shop/sh-3/15.jpg" alt="image">
                                                    </a>
                                                    <span class="new">New</span> -->
                                                </div>
                                                <div class="product-info clearfix">
                                                    <span class="product-title"></span>
                                                    <div class="price">
                                                        <!-- <ins></ins> -->
                                                        <span class="amount">Tanya Notaris sangat mengedepankan transparansi terhadap penggunaan data dan proses pengerjaan. Seluruh informasi untuk kebutuhan administrasi sudah tertera di setiap fitur layanan, sehingga pengguna dapat mengetahui dan mempersiapkannya.</span>
    
                                                    </div>
                                                </div>
                                                <!-- <div class="add-to-cart text-center">
                                                    <a href="#">ADD TO CART</a>
                                                </div>
                                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a> -->
                                            </div><!-- /.product-item -->
    
    
    
    
                                        </div><!-- /.owl-carousel  -->
                                    </div>
                                </div><!-- /.flat-content-box -->
                                {{-- <div class="divider h97"></div> --}}
                            </div>
                        </div><!-- /.col-md-6 -->
                        {{-- <div class="col-md-6 half-background style-3">
                            <!-- <img src="images/gallery/tos.png"> -->
                            <div class="flat-image-box style-1 clearfix">
                                <!-- <div class="elm-btn">
                                    <a href="#" class="themesflat-button bg-white width-150">For woMan’s</a>
                                </div> -->
                            </div>
                        </div><!-- /.col-md-6 --> --}}
                    </div>
                </div><!-- /.container -->
            </section>
            {{-- <hr style="width:50%"> --}}
            <!-- END -->
@endsection