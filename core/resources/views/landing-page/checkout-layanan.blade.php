@extends('landing-page.index')
@section('konten')
    <h2 class="title my-5" style="margin-left: 7rem;">Checkout</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card shadow-sm bg-body rounded">
                    <p class="fs-6 fw-bold ms-3 mt-3">Konfirmasi Pesanan</p>
                    <hr/>
                    <div class="row p-3">
                        <div class="d-flex flex-row bd-highlight">
                            <div class="ms-4  me-3">
                                <img src="{{ asset('aset/images/hand.svg') }}"/>
                            </div>
                            <div class="mb-3">
                                <p class="card-title fs-6">{{ $services['jenis_paket']}}</p>
                                <h4 class="fs-6" style="color: #F37A51;">Rp{{ number_format($services['harga'], 0, '', '.') }}</h4>
                            </div>
                        </div> 
                    </div>    
                </div>         
            </div>
            <div class="col-md-4">
                <div class="card shadow-sm bg-body rounded">
                    <div class="card-body">
                        <p class="fw-bold fs-6">Ringkasan Pesanan</p>
                        <hr/>
                        <p class="fw-bold mb-1">Detail Item</p>
                        <div class="d-flex justify-content-between mb-1">
                        <span>Subtotal</span> <span>Rp{{ number_format($services['harga'], 0, '', '.') }}</span>
                        </div>
                        <div class="d-flex justify-content-between mb-1">
                        <span>Metode Pembayaran</span> <span><input class="form-check-input" type="radio" name="metode_bayar" value="{{ $metode[0]['id'] }}" required checked>{{ $metode[0]['metode_pembayaran'] }}</span>
                        </div>
                        <hr/>
                        <div class="d-flex justify-content-between mb-1 fw-bold">
                        <span>Total</span> <span>Rp{{ number_format($services['harga'], 0, '', '.') }}</span>
                        </div>
                        <hr/>
                        <p class="fw-bold mb-2">Transfer Pembayaran</p>
                        <img src="{{ asset('aset/images/'.$metode[0]['foto_provider'].'') }}" class="img-fluid" style="width:100px" />
                        <p class="mb-1">{{ $metode[0]['nama_pemilik_rekening']}}</p>
                        <p class="fw-bold mb-3">{{ $metode[0]['no_rekening']}}</p>
                        <form action="{{ route('order') }}" method="post">
                            @csrf
                            <input type="hidden" name="id_service_sub" value=" {{ $services['id_service_sub'] }}">
                            <input type="hidden" name="total_harga" value="{{ $services['harga'] }}">
                            <input type="hidden" name="status_pembayaran" value="Menunggu" />
                            <input type="hidden" name="metode_pembayaran" value="1" />
                            <button type="submit" class="btn-primary w-100 rounded">Buat Pesanan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection