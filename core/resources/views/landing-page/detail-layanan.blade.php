@extends('landing-page.index')
@section('konten')
    <div class="banner">
            <h2 class="my-5" style="margin-left: 7rem; color: white;">{{ $detail[0]['jenis_paket'] }}</h2>
        </div>
        
        <div class="container mt-5">
            <!-- <div class="d-flex justify-content-center">
                <a class="btn btn-primary rounded mx-3 mb-5" id="pills-deskripsi-tab" data-bs-toggle="pill" data-bs-target="#pills-deskripsi" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Deskripsi Layanan</a>
                <a class="btn btn-primary rounded mx-3 mb-5" id="pills-syarat-tab" data-bs-toggle="pill" data-bs-target="#pills-syarat" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Persyaratan</a>
                <a class="btn btn-primary rounded mx-3 mb-5" id="pills-tahap-tab" data-bs-toggle="pill" data-bs-target="#pills-tahap" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tahap Pengerjaan</a>                     
            </div> -->
            <div class="row">
                <div class="col-md-8 mb-5">
                    <p class="fw-bold fs-5">Deskripsi Layanan</p>
                    <p class="mt-3 fs-6">{{ $detail[0]['detail_paket'] }}</p>
                </div>
                <div class="col-md-8 mb-5">
                    <p class="fw-bold fs-5">Persyaratan</p>
                    <p class="mt-3 fs-6">{{ $detail[0]['syarat_layanan'] }}</p>
                </div>
                <div class="col-md-8 mb-5">
                    <p class="fw-bold fs-5">Tahapan Pengerjaan</p>
                    <p class="mt-3 fs-6">{{ $detail[0]['tahapan_pengerjaan'] }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card shadow-sm bg-body rounded" data-aos="fade-up-right">
                        <div class="row">
                            <div class="col-md-4">
                                <div>
                                    <img src="{{ asset('aset/images/footer_layanan.png') }}" alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <p class="fw-bold fs-5"> Kenapa harus menggunakan jasa notaris di Tanya Notaris?</p>
                                    <ul class="mt-3">
                                        <li class="list mb-3">
                                            <p style="margin-left: 2rem;">Semua layanan kami dikerjangan dengan standar yang tinggi secara in-house oleh para legal dan notaris resmi dan berpengalaman.</p>
                                        </li>
                                        <li class="list mb-3">
                                            <p style="margin-left: 2rem;">Terpecaya oleh lebih dari 5.000 UMKM, Inkubator, Akselerator dan terpilih menjadi mitra Pemerintah.</p>
                                        </li>
                                        <li class="list mb-3">
                                            <p style="margin-left: 2rem;">Jangkauan secara Nasional.</p>
                                        </li>
                                        <li class="list mb-3">
                                            <p style="margin-left: 2rem;">Data serta informasi Anda terjamin aman dan terlindungi.</p>
                                        </li>
                                        <li class="list mb-3">
                                            <p style="margin-left: 2rem;">Akses langsung ke para pakar kami.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>         
                </div>
                <div class="col-md-4">
                    <div class="card shadow-sm bg-body rounded" data-aos="fade-up-left">
                        <div class="card-body">
                            <p class="fw-bold fs-5">Pesan Layanan Sekarang!</p>
                            <hr/>
                            <p class="card-text mb-1">Mulai dari</p>
                            <h4 class="fs-5" style="color: #F37A51;">Rp{{ number_format($detail[0]['harga'], 0, '', '.') }}</h4>
                            <p class="card-text mt-3 mb-1">Estimasi Waktu</p>
                            <p class="fw-bold mb-3">{{ $detail[0]['estimasi_waktu'] }} hari (setelah persyaratan lengkap)</p>
                            @auth
                                <form action="{{ url('/checkout-layanan/'.$detail[0]['id_services'].'') }}" method="post">
                                    @csrf
                                    <button type="submit" class="btn-primary w-100 rounded">Pesan Layanan</button>
                                </form>
                            @else
                                <button type="submit" class="btn-primary w-100 rounded" onclick="alert('Silahkan login terlebih dahulu!');">Pesan Layanan</button>
                            @endauth
                            <p class="mt-2 text-center">Butuh bantuan? <a href="https://wa.me/6281319303492?text=Halo,%20Admin.%20Saya%20ingin%20bertanya." style="color: #F37A51;">Silahkan Hubungi Kami</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#F37A51" fill-opacity="1" d="M0,128L60,144C120,160,240,192,360,192C480,192,600,160,720,170.7C840,181,960,235,1080,250.7C1200,267,1320,245,1380,234.7L1440,224L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>
        </div>
      
@endsection