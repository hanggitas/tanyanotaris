<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Tanya Notaris</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link href="{{ asset('aset/stylesheets/bootstrap.css') }}" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    <!-- Theme Style -->
    <link href="{{ asset('aset/stylesheets/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/owl.carousel.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/flexslider.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/jquery-ui.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/magnific-popup.css') }}" rel="stylesheet"/>


    <!-- Colors -->
    <link href="{{ asset('aset/stylesheets/colors/color1.css') }}"  rel="stylesheet" id="colors" />

    <!-- Animation Style -->
    <link href="{{ asset('aset/stylesheets/animate.css') }}" rel="stylesheet" />
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">


    <!-- Favicon and touch icons  -->
    <link href="{{ asset('aset/images/uni.png') }}" rel="shortcut icon">

    <style>
        .btn-primary,
        .btn-primary:hover,
        .btn-primary:active,
        .btn-primary:visited,
        .btn-primary:focus {
            background-color: #F37A51;
            border-color: #F37A51;
        }
        .banner {
            display: flex;
            margin: 50px;
            margin-top: 0;
            margin-left: 0;
            margin-right: 0;
            height: 30vw;
            width: auto;
            min-height: 120px;
            max-height: 120px;
            border-bottom-left-radius: 2rem;
            border-bottom-right-radius: 2rem;
            background-color: #F37A51;
        }
        .list{
            background: url("{{ asset('aset/images/icon_checklist.svg') }}") no-repeat left top;
        }
    </style>

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header_sticky header-style-2 has-menu-extra">
	<!-- Preloader -->
    <div id="loading-overlay">
        <div class="loader"></div>
    </div> 

    <!-- Boxed -->
    <div class="boxed">
    	<div id="site-header-wrap">
            <!-- Header -->
            <header id="header" class="header clearfix">
                <div class="container-fluid clearfix container-width-93" id="site-header-inner">
                    <div id="logo" class="logo float-left">
                        <a href="{{ route('beranda') }}" title="logo">
                            <img src="{{ asset('aset/images/TanyaNotaris.png') }}" class="img-fluid m-3" alt="image" width="127" height="44" data-retina="{{ asset('aset/images/TanyaNotaris.png') }}" data-width="132" data-height="49">
                        </a>
                    </div><!-- /.logo -->
                    <div class="mobile-button"><span></span></div>
                    <ul class="navbar-nav mx-3 px-4 py-3" style="float: right;">
                        @auth    
                            <li class="nav-item dropdown me-5 ms-5">
                                <a class="dropdown-toggle fs-6" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Selamat Datang, {{ Auth::user()->name }}
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item fs-6" href="{{ route('dashboard') }}">Dashboard Saya</a></li>
                                    <li><hr class="dropdown-divider"></li>
                                    <li><a class="dropdown-item fs-6" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><i class="bi bi-box-arrow-right"></i> Logout</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </ul>
                            </li>
                        @else
                    
                            <li style="float: right;">
                                <a class="btn btn-primary mx-3 px-3 py-2 shadow" href="{{ route ('masuk')}}" style="font-size: 12px;">Masuk</a>
                            </li>      
                        @endauth
                    </ul>

                    <div class="nav-wrap">
                        <nav id="mainnav" class="mainnav">
                            <ul class="menu">
                                <li class="{{ request()->is('beranda') ? 'active' : '' }}">
                                    <a href="{{ route('beranda') }}">Beranda</a>
                                </li>
                                <li class="{{ request()->is('mengapa-kami') ? 'active' : '' }}">
                                    <a href="{{ route('mengapa') }}">Mengapa Kami</a>
                                </li>
                                <li class="{{ request()->is('layanan-kami') ? 'active' : '' }}">
                                    <a href="{{ route('layanan') }}">Layanan Kami</a>
                                </li>
                                <li class="{{ request()->is('faq-landing') ? 'active' : '' }}">
                                    <a href="{{ route('faq-landing') }}">FAQ</a>
                                </li>
                                <li class="{{ request()->is('artikel-kami') ? 'active' : '' }}" >
                                    <a href="{{ route('artikelKami') }}">Artikel Notaris</a>
                                </li>
                            </ul>
                        </nav><!-- /.mainnav -->
                    </div><!-- /.nav-wrap -->
                </div><!-- /.container-fluid -->
            </header><!-- /header -->
        </div><!-- /.flat-header-wrap -->
        
        @yield('konten')



    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="widget widget-brand">
                        <div class="logo logo-footer">
                            <a href="{{ route('beranda') }}"><img src="{{ asset('aset/images/TanyaNotaris.png') }}" alt="image" width="127" height="44"></a>
                        </div>
                        <ul class="flat-contact">
                            <li class="address">{{ $kontak[0]['alamat'] }}</li>
                        </ul><!-- /.flat-contact -->
                    </div><!-- /.widget -->
                </div><!-- /.col-md-3 -->
                <div class="col-sm-6 col-md-3">
                    <div class="widget widget-link ml-5 mt-3">
                        <h4 class="mb-2">Perusahaan</h4>
                        <ul>
                            <li>
                                <a href="{{ route('mengapa') }}">Mengapa Kami</a>
                            </li>
                            <li>
                                <a href="{{ route('layanan') }}">Layanan Kami</a>
                            </li>
                            <li>
                                <a href="{{ route('faq-landing') }}">FAQ</a>
                            </li>
                            <li >
                                <a href="{{ route('artikelKami') }}">Artikel Notaris</a>
                            </li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.col-md-3 -->
                @foreach ($kontak as $kontak)
                    <div class="col-sm-6 col-md-3">
                        <div class="widget widget-link ml-5 mt-3">
                            <h4 class="mb-2">Kontak Kami</h4>
                            <ul>
                                <li>
                                    <p>{{  $kontak->email }}</p>
                                </li>
                                <li>
                                    <p>0{{  $kontak->nomor_telepon}}</p>
                                </li>
                            </ul>
                        </div><!-- /.widget -->
                    </div><!-- /.col-md-3 -->
                    <div class="col-sm-6 col-md-3">
                        <div class="widget widget-link ml-5 mt-3">
                            <h4 class="mb-2">Follow Us</h4>
                            <ul class="flat-social">
                                <li><a href="{{  $kontak->facebook}}"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="{{  $kontak->twitter }}"><i class="fa fa-twitter"></i></a></li>
                                {{-- <li><a href="#"><i class="fa fa-google"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li> --}}
                                <li><a href="{{  $kontak->linkedin }}"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="{{  $kontak->instagram}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg></a></li>
                            </ul>
                        </div><!-- /.widget -->
                    </div><!-- /.col-md-3 -->
                @endforeach
        
            
            </div><!-- /.row -->
        </div><!-- /.container -->
    </footer><!-- /.footer -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="copyright text-center">Copyright @2022 <a href="#">tanyanotaris</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    </div>
    <!-- Javascript -->
    <script src="{{ asset('aset/javascript/jquery.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/tether.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/bootstrap.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.easing.js') }}"></script>
    <script src="{{ asset('aset/javascript/parallax.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery-waypoints.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery-countTo.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.countdown.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('aset/javascript/images-loaded.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/magnific.popup.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.hoverdir.js') }}"></script>
    <script src="{{ asset('aset/javascript/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/equalize.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/gmap3.min.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIEU6OT3xqCksCetQeNLIPps6-AYrhq-s&region=GB"></script>
    <script src="{{ asset('aset/javascript/jquery-ui.js') }}"></script>

    <script src="{{ asset('aset/javascript/jquery.cookie.js') }}"></script>
    <script src="{{ asset('aset/javascript/main.js') }}"></script>

    <!-- Revolution Slider -->
    <script src="{{ asset('aset/rev-slider/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/rev-slider.js') }}"></script>
    <!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
      AOS.init({
        // Global settings:
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)

        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        once: false, // whether animation should happen only once - while scrolling down
        mirror: true, // whether elements should animate out while scrolling past them

        });
    </script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $.ajax({
                type: "GET",
                url: "{{route('getIP')}}",
                success: function(data)
                {
                    // console.log(data);
                    if(data.success == false)
                    {
                        alert('Bot detected, you are not allowed!');
                        window.location = "{{url('/bot')}}";
                    }
                }
            });
        });
    </script>
    <noscript>Sorry, your browser does not support JavaScript!</noscript>
</body>
</html>