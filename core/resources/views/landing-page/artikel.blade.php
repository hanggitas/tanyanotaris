@extends('landing-page.index')
@section('konten')
            <!-- Page title -->
            <div class="page-title parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-heading why">
                                <h1 class="title">Artikel Kami</h1>
                            </div><!-- /.page-title-heading -->
                            <!-- <div class="breadcrumbs">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="blog-list-2.html">Blog</a></li>
                                </ul>
                            </div> /.breadcrumbs -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.page-title -->
    
            <!-- Blog posts -->
            <section class="blog-posts style2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="post-wrap style2">
                                @foreach ($berita as $b)
                                <article class="post clearfix">
                                    <div class="featured-post">
                                        @if($b->gambar == NULL)
                                        <img src="{{ asset('aset/images/blog/Rectangle 1740.png') }}" alt="" width="350px"> 
                                        @else
                                        <img src="{{ asset('aset/images/blog/'.$b->gambar) }}" alt="image" width="350px">
                                        @endif
                                    </div>
                                    <div class="content-post">
                                        <div class="title-post">
                                            <h2>{{ $b->judul }}</a></h2>
                                        </div><!-- /.title-post -->
                                        <ul class="meta-post">
                                            <li class="author">
                                                <span>Posted By: </span><a href="#">Admin</a>
                                            </li>
                                            <li class="comment">
                                                <a href="#">{{ date('j M Y', strtotime($b->created_at)) }}</a>
                                            </li>
                                        </ul><!-- /.meta-post -->
                                        <div class="entry-post">
                                            <p>{{ substr(strip_tags($b->deskripsi), 0, 200) }}{{ strlen(strip_tags($b->deskripsi)) > 200 ? "..." : "" }}</p>
                                            <div class="more-link">
                                                <a href="{{ url('/artikel-detail/'.$b->id) }}">Read More</a>
                                            </div>
                                        </div>
                                    </div><!-- /.content-post -->
                                </article><!-- /.post -->
                                @endforeach    
                            </div><!-- /.post-wrap -->
                            <div class="blog-pagination left clearfix">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $berita->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div><!-- /.col-md-9 -->
                        <div class="col-md-3">
                            <div class="sidebar">
                                <div class="widget widget_categories">
                                    <h5 class="widget-title">Cari Artikel</h5>
                                    <ul>
                                        <li>
                                            <form action="{{route('artikelKami')}}" method="GET">
                                                <div class="d-flex">
                                                    <input type="text" name="search" placeholder="Tentang Artikel" value="@if($search){{$search}}@endif">
                                                    <button type="submit" class="btn btn-outline-danger">Cari</button>
                                                </div>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget widget_categories">
                                    <h5 class="widget-title">kategori Artikel</h5>
                                    <ul>
                                        @foreach($kategori as $key => $k)
                                        <li ><a href="{{ route('kategoriartikel',$k->id) }}">{{ $k->nama_kategori }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- /.widget-categories -->
    
                                <div class="widget widget-news-latest">
                                    <h5 class="widget-title">Artikel Terbaru</h5>
                                    <ul class="popular-news clearfix">
                                        @foreach ($new as $n)
                                        <li>
                                            <h6>
                                                <a href="{{ url('/artikel-detail/'.$n->id) }}">{{ $n->judul }}</a>
                                            </h6>
                                            <a class="post_meta">{{ $n->created_at->diffForHumans() }}</a>
                                        </li>
                                        @endforeach
                                    </ul><!-- /.popular-news -->
                                </div><!-- /.widget-news-latest -->
                            </div><!-- /.sidebar -->
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.blog posts -->
    
            <!-- NEW LATEST -->
            <section class="flat-row row-new-latest style-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-section margin-bottom-51">
                                <h2 class="title">Artikel Terbaru</h2>
                            </div>
                            <div class="new-latest-wrap">
                                <div class="flat-new-latest post-wrap flat-carousel-box style4 data-effect clearfix"
                                    data-auto="false" data-column="3" data-column2="2" data-column3="1" data-gap="30">
                                    <div class="owl-carousel owl-theme">
                                        @foreach ($new as $baru)
                                        <article class="post clearfix">
                                            <div class="featured-post data-effect-item">
                                                @if($baru->gambar == NULL)
                                                <img src="{{ asset('aset/images/blog/Rectangle 1740.png') }}" alt="" width="300" height="300"> 
                                                @else
                                                <img src="{{ asset('aset/images/blog/'.$baru->gambar) }}" alt="image" width="300" height="300">
                                                @endif
                                                <div class="overlay-effect bg-color-2"></div>
                                            </div>
                                            <div class="content-post">
                                                <ul class="meta-post">
                                                    <li class="date">
                                                        {{ $baru->created_at->diffForHumans() }}
                                                    </li>
                                                    <li class="author">
                                                        <a href="#"> By Admin</a>
                                                    </li>
                                                </ul><!-- /.meta-post -->
                                                <div class="title-post">
                                                    <h2>{{ $baru->judul }}</h2>
                                                </div><!-- /.title-post -->
                                                <div class="entry-post">
                                                    <div class="more-link">
                                                        <a href="{{ url('/artikel-detail/'.$baru->id) }}">Continue Reading </a>
                                                    </div>
                                                </div>
                                            </div><!-- /.content-post -->
                                        </article><!-- /.post -->
                                        @endforeach
                                    </div><!-- /.owl-carousel -->
                                    <!-- <a href="#"
                                    class="themesflat-button has-padding-36 bg-accent has-shadow margin-top-8 margin-right-"><span>Lihat
                                        Semua
                                        Artikel</span></a> -->
                                    <!-- <br>
                                    <div class="elm-btn text-center">
                                        <a href="blog-list-2.html"
                                            class="themesflat-button outline bg-accent margin-top-8">Lihat Semua
                                            Artikel</a>
                                    </div> -->
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
            </section>
            <!-- END NEW LATEST -->
@endsection