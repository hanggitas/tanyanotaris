<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>TanyaNotaris</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link href="{{ asset('aset/stylesheets/bootstrap.css') }}" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    <!-- Theme Style -->
    <link href="{{ asset('aset/stylesheets/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/owl.carousel.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/flexslider.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/jquery-ui.css') }}" rel="stylesheet" />
    <link href="{{ asset('aset/stylesheets/magnific-popup.css') }}" rel="stylesheet"/>


    <!-- Colors -->
    <link href="{{ asset('aset/stylesheets/colors/color1.css') }}"  rel="stylesheet" id="colors" />

    <!-- Animation Style -->
    <link href="{{ asset('aset/stylesheets/animate.css') }}" rel="stylesheet" />


    <!-- Favicon and touch icons  -->
    <link href="{{ asset('aset/images/logo icon.png') }}" rel="shortcut icon">

    <style>
        .btn-primary,
        .btn-primary:hover,
        .btn-primary:active,
        .btn-primary:visited,
        .btn-primary:focus {
            background-color: #F37A51;
            border-color: #F37A51;
        }
        .banner {
            display: flex;
            margin: 50px;
            margin-top: 0;
            margin-left: 0;
            margin-right: 0;
            height: 30vw;
            width: auto;
            min-height: 120px;
            max-height: 120px;
            border-bottom-left-radius: 2rem;
            border-bottom-right-radius: 2rem;
            background-color: #F37A51;
        }
        .list{
            background: url("{{ asset('aset/images/icon_checklist.svg') }}") no-repeat left top;
        }
    </style>

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header_sticky header-style-2 has-menu-extra">
	<!-- Preloader -->
    <div id="loading-overlay">
        <div class="loader"></div>
    </div> 

    <!-- Boxed -->
    <div class="boxed">
    	<div id="site-header-wrap">
            <!-- Header -->
            <header id="header" class="header clearfix">
                <div class="container-fluid clearfix container-width-93" id="site-header-inner">
                    <div id="logo" class="logo float-left">
                        <a href="{{ route('beranda') }}" title="logo">
                            <img src="{{ asset('aset/images/TanyaNotaris.png') }}" class="img-fluid m-3" alt="image" width="127" height="44" data-retina="{{ asset('aset/images/TanyaNotaris.png') }}" data-width="132" data-height="49">
                        </a>
                    </div><!-- /.logo -->
                    <div class="mobile-button"><span></span></div>
                    <ul class="navbar-nav mx-3 px-4 py-3" style="float: right;">
                        @auth    
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle fs-6" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Selamat Datang, {{ Auth::user()->name }}
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item fs-6" href="{{ route('dashboard') }}">Dashboard Saya</a></li>
                                    <li><hr class="dropdown-divider"></li>
                                    <li><a class="dropdown-item fs-6" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><i class="bi bi-box-arrow-right"></i> Logout</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </ul>
                            </li>
                        @else
                        <ul style="float: right;">
                            <li class="">
                                <a class="btn btn-primary mx-3 px-4 py-3 shadow" href="{{ route ('masuk')}}">Masuk</a>
                            </li>
                        </ul>
                        @endauth
                    </ul>
               
                    <div class="nav-wrap">
                        <nav id="mainnav" class="mainnav">
                            <ul class="menu">
                                <li class="{{ request()->is('beranda') ? 'active' : '' }}">
                                    <a href="{{ route('beranda') }}">Beranda</a>
                                </li>
                                <li class="{{ request()->is('mengapa-kami') ? 'active' : '' }}">
                                    <a href="{{ route('mengapa') }}">Mengapa Kami</a>
                                </li>
                                <li class="{{ request()->is('layanan-kami') ? 'active' : '' }}">
                                    <a href="{{ route('layanan') }}">Layanan Kami</a>
                                </li>
                                <li class="{{ request()->is('faq-landing') ? 'active' : '' }}">
                                    <a href="{{ route('faq-landing') }}">FAQ</a>
                                </li>
                                <li class="{{ request()->is('artikel-kami') ? 'active' : '' }}" >
                                    <a href="{{ route('artikel') }}">Artikel Notaris</a>
                                </li>
                            </ul>
                        </nav><!-- /.mainnav -->
                    </div><!-- /.nav-wrap -->
                </div><!-- /.container-fluid -->
            </header><!-- /header -->
        </div><!-- /.flat-header-wrap -->

        @yield('konten')

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="copyright text-center">Copyright @2022 <a href="#">tanyanotaris</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    </div>

    <!-- Javascript -->
    <script src="{{ asset('aset/javascript/jquery.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/tether.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/bootstrap.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.easing.js') }}"></script>
    <script src="{{ asset('aset/javascript/parallax.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery-waypoints.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery-countTo.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.countdown.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('aset/javascript/images-loaded.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/magnific.popup.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/jquery.hoverdir.js') }}"></script>
    <script src="{{ asset('aset/javascript/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/equalize.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/gmap3.min.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIEU6OT3xqCksCetQeNLIPps6-AYrhq-s&region=GB"></script>
    <script src="{{ asset('aset/javascript/jquery-ui.js') }}"></script>

    <script src="{{ asset('aset/javascript/jquery.cookie.js') }}"></script>
    <script src="{{ asset('aset/javascript/main.js') }}"></script>

    <!-- Revolution Slider -->
    <script src="{{ asset('aset/rev-slider/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('aset/javascript/rev-slider.js') }}"></script>
    <!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('aset/rev-slider/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
      AOS.init({
        // Global settings:
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)

        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        once: false, // whether animation should happen only once - while scrolling down
        mirror: true, // whether elements should animate out while scrolling past them

        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <noscript>Sorry, your browser does not support JavaScript!</noscript>
</body>

</html>

<!-- PRODUCT -->
<!-- <section class="flat-row main-shop no-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="title-section margin-bottom-20 margin-top-50">
                        <h2 class="title">Artikel Notaris</h2>
                    </div>
                    <div class="product-content product-fourcolumn clearfix">
                        <ul class="product style2 clearfix">
                            <li class="product-item ">
                                <div class="product-thumb clearfix">
                                    <a href="#">
                                        <img src="images/shop/sh-4/17.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <ins>
                                            <span class="amount">$19.00</span>
                                        </ins>
                                    </div>
                                    <ul class="flat-color-list width-14">
                                        <li>
                                            <a href="#" class="red"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="blue"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="black"></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item man accessories">
                                <div class="product-thumb clearfix">
                                    <a href="#">
                                        <img src="images/shop/sh-4/18.jpg" alt="image">
                                    </a>
                                    <span class="new">New</span>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <ins>
                                            <span class="amount">$10.00</span>
                                        </ins>
                                    </div>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item kid woman">
                                <div class="product-thumb clearfix">
                                    <a href="#" class="product-thumb">
                                        <img src="images/shop/sh-4/19.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <ins>
                                            <span class="amount">$20.00</span>
                                        </ins>
                                    </div>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item man accessories">
                                <div class="product-thumb clearfix">
                                    <a href="#" class="product-thumb">
                                        <img src="images/shop/sh-4/20.jpg" alt="image">
                                    </a>
                                    <span class="new sale">Sale</span>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <del>
                                            <span class="regular">$90.00</span>
                                        </del>
                                        <ins>
                                            <span class="amount">$60.00</span>
                                        </ins>
                                    </div>
                                    <ul class="flat-color-list width-14">
                                        <li>
                                            <a href="#" class="red"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="blue"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="black"></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item kid woman">
                                <div class="product-thumb clearfix">
                                    <a href="#" class="product-thumb">
                                        <img src="images/shop/sh-4/21.jpg" alt="image">
                                    </a>
                                    <span class="new">New</span>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <ins>
                                            <span class="amount">$139.00</span>
                                        </ins>
                                    </div>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item man accessories">
                                <div class="product-thumb clearfix">
                                    <a href="#" class="product-thumb">
                                        <img src="images/shop/sh-4/22.jpg" alt="image">
                                    </a>
                                    <span class="new sale">Sale</span>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <del>
                                            <span class="regular">$150.00</span>
                                        </del>
                                        <ins>
                                            <span class="amount">$120.00</span>
                                        </ins>
                                    </div>
                                    <ul class="flat-color-list width-14">
                                        <li>
                                            <a href="#" class="red"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="blue"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="black"></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item kid woman">
                                <div class="product-thumb clearfix">
                                    <a href="#" class="product-thumb">
                                        <img src="images/shop/sh-4/23.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <ins>
                                            <span class="amount">$110.00</span>
                                        </ins>
                                    </div>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                            <li class="product-item man accessories">
                                <div class="product-thumb clearfix">
                                    <a href="#" class="product-thumb">
                                        <img src="images/shop/sh-4/24.jpg" alt="image">
                                    </a>
                                    <span class="new">New</span>
                                </div>
                                <div class="product-info clearfix">
                                    <span class="product-title">Cotton White Underweaer Block Out Edition</span>
                                    <div class="price">
                                        <ins>
                                            <span class="amount">$90.00</span>
                                        </ins>
                                    </div>
                                </div>
                                <div class="add-to-cart text-center">
                                    <a href="#">ADD TO CART</a>
                                </div>
                                <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                            </li>
                        </ul>
                        <div class="elm-btn text-center">
                            <a href="#" class="themesflat-button outline ol-accent margin-top-8">LOAD MORE</a>
                        </div>
                    </div>
                    <div class="divider h56"></div>
                </div>
            </div>
        </div>
    </section>
    END PRODUCT -->

<!-- COUNTDOWN
    <div class="flat-row row-countdown no-padding bg-section row-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="flat-content-box clearfix" data-margin="0 0 0 0" data-mobilemargin="0 0 0 0">
                        <div class="flat-countdown-wrap text-center">
                            <div class="inner">
                                <div class="divider h115 clearfix"></div>
                                <h2 class="heading font-size-40 line-height-48">Deal Of The Week</h2>
                                <p class="desc font-size-18 font-weight-400 line-height-48">Special Discount Limited
                                    Time Only</p>
                                <div class="divider h42 clearfix"></div>
                                <div class="wrap-countdown no-margin-bottom">
                                    <div class="countdown style-1"></div>
                                </div>
                                <div class="divider h115 clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    END COUNTDOWN -->