<form action="{{ route('custom_order') }}" method="post">
    <div class="modal fade text-start" id="inlineForm" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Masukkan Paket Custom</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                
                    @csrf
                    <div class="modal-body">
                        <label>Kategori </label>
                        <div class="mb-1">
                            <select class="select2 form-select" id="select2-basic" name="id_kategori">
                                <option value="">-- Pilih Kategori --</option>
                                @foreach($pilihan_kategori as $pilih_kategori)
                                
                                <option value="{{ $pilih_kategori->id }}">
                                    {{ $pilih_kategori->nama_kategori }}
                                </option>
                                
                                @endforeach
                            </select>
                        </div>

                        {{-- <label>Subkategori </label>
                        <div class="mb-1">
                            <select class="select2 form-select" id="select2-basic" name="id_subkategori">
                                <option value="">-- Pilih Subkategori --</option>
                                @foreach($pilihan_subkategori as $pilih_subKat)
                                
                                <option value="{{ $pilih_subKat->id }}">
                                    {{ $pilih_subKat->nama_subkategori }}
                                </option>
                                
                                @endforeach
                            </select>
                        </div> --}}

                        {{-- <label>Jenis Paket </label>
                        <div class="mb-1">
                            <input type="text" placeholder="" class="form-control" />
                        </div> --}}

                        <label>Detail Paket </label>
                        <div class="mb-1">
                            <input type="text" name="detail_paket" placeholder="" class="form-control" />
                        </div>

                        <label>Total Harga </label>
                        <div class="mb-1">
                            <input type="text" name="harga" placeholder="" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                    </div>
            </div>
        </div>
    </div>
</form>