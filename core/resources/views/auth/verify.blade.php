@extends('landing-page.index-auth')

@section('konten')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card shadow-sm bg-body rounded mt-5">
                <div class="card-header">{{ __('Verifikasi email anda') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Link verifikasi telah dikirim, silahkan cek email anda.') }}
                        </div>
                    @endif

                    {{ __('Silahkan cek email anda untuk link verifikasi email.') }}
                    {{ __('Jika anda tidak menerima email verifikasi,') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <center>
                            <button type="submit" class="btn-primary btn-sm mt-3">Kirim ulang</button>.
                        </center>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
