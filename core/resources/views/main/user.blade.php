@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Filter</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate action="{{route('user')}}" method="get">
                                    @csrf
                                    <div class="row">
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Email</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="email" value="@if(isset($old_value['email'])) {{$old_value['email']}} @endif">
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Nama</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="nama" value="@if(isset($old_value['nama'])) {{$old_value['nama']}} @endif">
                                        </div>
                                        <div class="mb-1 col-md-4">
                                            <label class="d-block form-label mb-1" for="validationBioBootstrap">Status User</label>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="verifikasi_email"  value="1" @if(isset($old_value['verifikasi_email']) && $old_value['verifikasi_email'] == 1)
                                                    {{"checked"}}
                                                @endif/>
                                                <label class="form-check-label" for="menunggu">Aktif</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="verifikasi_email" value="0" @if(isset($old_value['verifikasi_email']) && $old_value['verifikasi_email'] == 0)
                                                {{"checked"}}
                                            @endif/>
                                                <label class="form-check-label" for="inlineRadio2">Tidak Aktif</label>
                                            </div>
                                        </div>
                                        <div class="col-md-1 mt-2">
                                            <button type="submit" class="btn btn-primary" id="validationBioBootstrap">Cari</button>
                                        </div>
                                    </div>
                                    <hr>
                                </form>
                            </div>
                        </div>

                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel User</h4>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Email</th>
                                            <th>Nama</th>
                                            <th>Nomor HP</th>
                                            <th>Alamat</th>
                                            <th>Status User</th>
                                            <th>Tanggal Aktif</th>
                                             <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no=1;
                                            @endphp --}}

                                            @foreach ($userss as $key => $users)
                                            <td>{{ $userss->firstItem() + $key }}</td>
                                            <td>{{ $users->email }}</td>
                                            <td>{{ $users->name }}</td>
                                            <td>{{ $users->no_hp_user }}</td>
                                            <td>{{ $users->alamat_user }}</td>
                                            <td>@if($users->email_verified_at != null) {{"Aktif"}} @else {{"Tidak Aktif"}} @endif</td>
                                            <td>{{date('d-m-Y H:i:s', strtotime($users->email_verified_at))}}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('edit-user/'.$users->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" onclick="deleteF('{{url('delete-user/'.$users->id)}}')">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $userss->firstItem() }} to {{ $userss->lastItem() }} of {{ $userss->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $userss->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

               

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function deleteF(url){
            if(confirm('Apakah anda yakin?'))
            {
                window.location.href = url;
            }
        }
    </script>
@endsection
