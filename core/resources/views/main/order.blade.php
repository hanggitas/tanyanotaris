@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Filter</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate action="{{route('order')}}" method="get">
                                    @csrf
                                    <div class="row">
                                        <div class="mb-1  col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Kode Bayar</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="basic-addon3">KB-</span>
                                                <input type="text" class="form-control" id="basic-url3" aria-describedby="basic-addon3" name="id" value="@if(isset($old_value['id'])) {{$old_value['id']}} @endif">
                                            </div>
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">User</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="email" value="@if(isset($old_value['email'])) {{$old_value['email']}} @endif">
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Layanan</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="jenis_paket" value="@if(isset($old_value['jenis_paket'])) {{$old_value['jenis_paket']}} @endif">
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label mb-1" for="validationBioBootstrap">Status Pembayaran</label>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_pembayaran" id="menunggu" value="Menunggu"  @if(isset($old_value['status_pembayaran'])) @if($old_value['status_pembayaran'] == 'Menunggu')
                                                    {{"checked"}}
                                                @endif @endif/>
                                                <label class="form-check-label" for="menunggu">Menunggu</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_pembayaran" id="lunas" value="Lunas"  @if(isset($old_value['status_pembayaran'])) @if($old_value['status_pembayaran'] == 'Lunas')
                                                {{"checked"}}
                                            @endif @endif/>
                                                <label class="form-check-label" for="inlineRadio2">Lunas</label>
                                            </div>
                                        </div>
                                        <div class="col-md-1 mt-2">
                                            <button type="submit" class="btn btn-primary" id="validationBioBootstrap">Cari</button>
                                        </div>
                                    </div>
                                    <hr>
                                </form>
                            </div>
                        </div>
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Order</h4>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Kode Bayar</th>
                                            <th>User</th>
                                            <th>Metode Pembayaran</th>
                                            <th>Layanan</th>
                                            <th>Bukti Pembayaran</th>
                                            <th>Waktu</th>
                                            <th>Total Harga</th>
                                            <th>Status Pembayaran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @foreach ($orders as $key => $order)
                                            <td>KB-{{ $order->id }}</td>
                                            <td>{{ $order->email }}</td>
                                            <td>{{ $order->metode_pembayaran }}</td>
                                            <td>{{ $order->jenis_paket }}</td>
                                            <td>
                                                @if($order->bukti_bayar_lunas)
                                                    {{-- <a href="{{ $order->bukti_bayar_lunas }}" target="_blank" alt="" style="width: 5cm;">Lihat Bukti Pelunasan</a> --}}
                                                <div class="vertical-modal-ex">
                                                    <a href="" data-bs-toggle="modal" data-bs-target="#lunas-{{ $order->id }}">Lihat Bukti Pelunasan</a>
                                                    <div class="modal fade" id="lunas-{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Preview bukti bayar lunas</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <img src="{{ $order->bukti_bayar_lunas }}" class="img-fluid rounded mx-auto d-block"/>                     
                                                                </div>                                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </td>
                                            <td>{{ date('d-m-Y',strtotime($order->date_time)) }}</td>
                                            <td>
                                                @if($order->total_harga)
                                                Rp{{ number_format($order->total_harga, 0, '', '.') }}
                                                @endif
                                            </td>
                                            <td>{{ $order->status_pembayaran }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('edit-order/'.$order->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('delete-order/'.$order->id) }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            
                            
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $orders->firstItem() }} to {{ $orders->lastItem() }} of {{ $orders->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $orders->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>

    {{-- @include('form.modal_custom') --}}
@endsection