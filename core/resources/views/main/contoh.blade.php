<html>
    <style type="text/css">
    </style>

    <body>
        <h3>Tanya Notaris</h3>
        <p>{{ $contact->alamat }}</p>
        <p>0{{ $contact->nomor_telepon }}</p>
        <p>{{ $contact->email }}</p>
        <p>Date Issued: {{ $order->date_time }}</p>
        <hr/>

        <h6 class="">Invoice To:</h6>
        <h6 class="">{{ $user->name }}</h6>
        <p class="">{{ $user->alamat_user }}</p>
        <p class="">0{{ $user->no_hp_user }}</p>
        <p class="">{{ $user->email }}</p>
        <br/>
        <table>
            <thead style="background-color: lightgray;">
                <tr>
                    <th width="200">Nama Paket</th>
                    <th width="180">Harga</th>
                    <th width="90">Qty</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">{{ $order->jenis_paket }}</td>
                    <td align="center">Rp{{ number_format( $order->harga, 0, '', '.') }}</td>
                    <td align="center">1</td>
                </tr>
            </tbody>
        </table>
        <br/>
        <p>Total:</p>
        <br/>
        <p>Rp{{ number_format( $order->total_harga, 0, '', '.') }}</p>
        <hr/>
        <p>Note: It was a pleasure working with you. We hope you will keep us in mind for future projects. Please leave a feedback in feedback section to let us know how you feel. Thank You!</p>
    </body>
</html>