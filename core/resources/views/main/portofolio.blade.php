@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        {{-- <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                
            </div>
        </div> --}}
        <div class="content-body">
            <!-- Basic Tables start -->
            <div class="row" id="basic-table">
                <div class="col-12">
                    @if(session()->get('success'))
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success</h4>
                        <div class="alert-body">
                            {{ session()->get('success') }}
                        </div>
                    </div>
                    @endif
                    @if(session()->get('error'))
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Error</h4>
                        <div class="alert-body">
                            {{ session()->get('error') }}
                        </div>
                    </div>
                    @endif
                    @if(session()->get('status'))
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Success</h4>
                        <div class="alert-body">
                            {{ session()->get('status') }}
                        </div>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Portofolio</h4>
                            <a href="{{ route('tambah_data_portofolio') }}" class="btn btn-primary">Tambah Portofolio</a>
                        </div>
                     
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Klien</th>
                                        <th>Projek Klien</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        {{-- @php
                                            $no = 1;
                                        @endphp --}}

                                        @foreach($left_join as $key => $join)
                                        <td>{{ $left_join->firstItem() + $key }}</td>
                                        <td>{{ $join->name }}</td>
                                        <td>
                                            {{-- <img src="{{ asset('app-assets/images/docs/'.$join->projek) }}" class="" alt="{{ $join->name }}" style="width: 5cm;"> --}}
                                                <a href="{{ asset('app-assets/images/docs/'.$join->projek) }}" target="_blank" alt="{{ $join->name }}" style="width: 5cm;">Lihat Projek</a>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                    <i data-feather="more-vertical"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a class="dropdown-item" href="{{ url('edit-portofolio/'.$join->id) }}">
                                                        <i data-feather="edit-2" class="me-50"></i>
                                                        <span>Edit</span>
                                                    </a>
                                                    <a class="dropdown-item" href="{{ url('delete-portofolio/'.$join->id) }}">
                                                        <i data-feather="trash" class="me-50"></i>
                                                        <span>Hapus</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="card-body">
                            {{-- <p class="card-text">Pagination with icon and text</p> --}}
                            showing {{ $left_join->firstItem() }} to {{ $left_join->lastItem() }} of {{ $left_join->total() }}
                            <nav aria-label="Page navigation">
                                <ul class="pagination mt-1">
                                    {{ $left_join->links() }} 
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Tables end -->
        </div>
    </div>
</div>
@endsection