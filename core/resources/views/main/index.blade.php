@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('main.content')
@extends('main.footer')
@section('content')
<div class="app-content content ">
  <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
      <div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
          
          <div class="content-body">
              <!-- Dashboard Ecommerce Starts -->
              <section id="dashboard-ecommerce">
                  <div class="row match-height">
                      <!-- Greetings Card starts -->
                      <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="card card-congratulations">
                              <div class="card-body text-center">
                                  <img src="{{asset('/app-assets/images/elements/decore-left.png')}}" class="congratulations-img-left" alt="card-img-left" />
                                  <img src="{{asset('/app-assets/images/elements/decore-right.png')}}" class="congratulations-img-right" alt="card-img-right" />
                                  <div class="avatar avatar-xl bg-primary shadow">
                                      <div class="avatar-content">
                                          <i data-feather="award" class="font-large-1"></i>
                                      </div>
                                  </div>
                                  <div class="text-center">
                                      <h1 class="mb-1 text-white">Welcome, {{Auth::user()->name}}</h1>
                                      <p class="card-text m-auto w-75">
                                          Tanya Notaris <strong>Dashboard</strong> Administrator
                                      </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Greetings Card ends -->
                      <!-- <div class="col-lg-8 col-12">
                            <div class="card card-revenue-budget">
                                <div class="row mx-0">
                                    <div class="col-md-8 col-12 revenue-report-wrapper">
                                        <div class="d-sm-flex justify-content-between align-items-center mb-3">
                                            <h4 class="card-title mb-50 mb-sm-0">Revenue Report</h4>
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex align-items-center me-2">
                                                    <span class="bullet bullet-primary font-small-3 me-50 cursor-pointer"></span>
                                                    <span>Earning</span>
                                                </div>
                                                <div class="d-flex align-items-center ms-75">
                                                    <span class="bullet bullet-warning font-small-3 me-50 cursor-pointer"></span>
                                                    <span>Expense</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="revenue-report-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                      
              </section>
              <!-- Dashboard Ecommerce ends -->
      </div>
    </div>
  </div>  
</div>    
@endsection