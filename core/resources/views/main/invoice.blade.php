@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')    
@section('content')
  <!-- BEGIN: Content-->
  <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="invoice-preview-wrapper">
                    <div class="row invoice-preview justify-content-center">
                        <!-- Invoice -->
                        <div class="col-xl-9 col-md-8 col-12">
                            <div class="card invoice-preview-card">
                                <div class="card-body invoice-padding pb-0">
                                    <!-- Header starts -->
                                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                                        <div>
                                            <div class="logo-wrapper">
                                                <h3 class="text-primary invoice-logo">Tanya Notaris</h3>
                                            </div>
                                            <p class="card-text mb-25">{{ $contact->alamat }}</p>
                                            <p class="card-text mb-25">0{{ $contact->nomor_telepon }}</p>
                                            <p class="card-text mb-0">{{ $contact->email }}</p>
                                        </div>
                                    </div>
                                    <!-- Header ends -->
                                </div>

                                <hr class="invoice-spacing" />

                                <!-- Address and Contact starts -->
                                <div class="card-body invoice-padding pt-0">
                                    <div class="row invoice-spacing">
                                        <div class="col-xl-8 me-1">
                                            <h6 class="mb-2">Invoice To:</h6>
                                            <h6 class="mb-25">{{ $user->name }}</h6>
                                            <p class="card-text mb-25">{{ $user->alamat_user }}</p>
                                            <p class="card-text mb-25">0{{ $user->no_hp_user }}</p>
                                            <p class="card-text mb-2">{{ $user->email }}</p>
                                        </div>
                                
                                <!-- Invoice Description starts -->
                                    <table class="table mx-0">
                                        <thead>
                                            <tr>
                                                <th class="py-1">Nama Paket</th>
                                                <th class="py-1">Harga</th>
                                                <th class="py-1">Qty</th>
                                                <th class="py-1">Status</th>
                                                <th class="py-1">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="border-bottom">
                                                <td class="py-1">
                                                    <p class="card-text fw-bold mb-25">{{ $order->jenis_paket }}</p>
                                                    <!-- <p class="card-text text-wrap">
                                                        Developed a full stack native app using React Native, Bootstrap & Python
                                                    </p> -->
                                                </td>
                                                <td class="py-1">
                                                    <span class="fw-bold text-wrap">Rp{{ number_format( $order->harga, 0, '', '.') }}</span>
                                                </td>
                                                <td class="py-1">
                                                    <span class="fw-bold">1</span>
                                                </td>
                                                <td class="py-1">
                                                    <span class="fw-bold">{{ $order->status_pembayaran }}</span>
                                                </td>
                                                <td class="py-1">
                                                    <span class="fw-bold">{{ $order->date_time }}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="card-body invoice-padding pb-0">
                                    <div class="row invoice-sales-total-wrapper">
                                        <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
                                        </div>
                                        <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                                                <div class="invoice-total-item">
                                                    <p class="invoice-total-title ">Total:</p>
                                                    <p class="invoice-total-amount">Rp{{ number_format( $order->total_harga, 0, '', '.') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- Invoice Description ends -->

                                <hr class="invoice-spacing" />

                                <!-- Invoice Note starts -->
                                <div class="card-body invoice-padding pt-0">
                                    <div class="row">
                                        <div class="col-12">
                                            <span class="fw-bold">Note:</span>
                                            <span>It was a pleasure working with you. We hope you will keep us in mind for future service. Thank You!</span>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="text-center mb-2 mt-1">
                                    <a class="btn btn-primary round text-center" href="{{ url('download-pdf/'.$order->id.'') }}">
                                        Download Invoice
                                    </a>
                                </div> --}}
                                <!-- Invoice Note ends -->
                            </div>
                        </div>
                                </div>
                        <!-- /Invoice -->
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection