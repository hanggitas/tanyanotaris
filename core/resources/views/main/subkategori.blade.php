@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Subkategori</h4>
                                <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('tambah_data_subkategori')}}" >Tambah Subkategori</a>
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Jenis Kategori</th>
                                            <th>Nama Subkategori</th>
                                            <th>Gambar</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                                $no = 1;
                                            @endphp --}}

                                            @foreach($left_join as $key => $join)

                                            <td>{{ $left_join->firstItem() + $key }}</td>
                                            <td>{{ $join->nama_kategori }}</td>
                                            <td>{{ $join->nama_subkategori }}</td>
                                            <td>
                                                <img src="{{ asset('app-assets/images/pages/eCommerce/'.$join->foto) }}" class="img-thumbnail" alt="{{ $join->nama_subkategori }}" style="width: 5cm;">
                                                {{-- <a href="{{ asset('app-assets/images/pages/eCommerce/'.$join->foto) }}" target="_blank" alt="{{ $join->nama_subkategori }}" style="width: 5cm;">Lihat Gambar</a> --}}
                                                {{-- <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#primary">
                                                    Lihat Gambar
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade text-start modal-primary" id="primary" tabindex="-1" aria-labelledby="myModalLabel160" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel160">{{ $join->nama_subkategori }}</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{ asset('app-assets/images/pages/eCommerce/'.$join->foto) }}" alt="{{ $join->nama_subkategori }}" class="img-fluid rounded mx-auto d-block">
                                                            </div>
                                                            <div class="modal-footer">
                                                                {{-- <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Accept</button> --}}
                                                            {{-- </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('edit-subkategori/'.$join->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('delete-subkategori/'.$join->id) }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $left_join->firstItem() }} to {{ $left_join->lastItem() }} of {{ $left_join->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $left_join->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection
