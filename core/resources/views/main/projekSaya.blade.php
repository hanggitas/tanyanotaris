@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Status Pekerjaan</h4>
                                {{-- <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('form_pekerjaan')}}" >Tambah Pekerjaan</a> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Kode Bayar</th>
                                            <th>Layanan</th>
                                            <th>Status Pekerjaan</th>
                                            <th>Lampiran Hasil</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_pekerjaan as $key => $kerja)
                                            <td>KB-{{ $kerja->id_order }}</td>
                                            <td>{{ $kerja->jenis_paket }}</td>
                                            <td>{{ $kerja->status_pekerjaan}}</td>
                                            <td>
                                                @if($kerja->link_project)
                                                <a class="btn btn-sm btn-primary" href="{{ url('/open/lampiran/'.$kerja->link_project)}}">
                                                    <span>Lihat Lampiran</span>
                                                </a>
                                                @else
                                                    <span>N/A</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_pekerjaan->firstItem() }} to {{ $data_pekerjaan->lastItem() }} of {{ $data_pekerjaan->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_pekerjaan->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function deleteF(url){
        if(confirm('Apakah anda yakin?'))
        {
            window.location.href = url;
        }
    }
</script>
@endsection