@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Preview</h4>
                                <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('formpreview')}}" >Tambah Preview</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Order</th>
                                            <th>Link Finish Project</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_preview as $key => $dp)
                                            <td>{{ $data_preview->firstItem() + $key }}</td>
                                            <td>{{ $dp->id_order }}</td>
                                            <td>{{ $dp->link_finish_project }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('/preview/'.$dp->id.'/edit') }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('/preview/'.$dp->id.'/delete') }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_preview->firstItem() }} to {{ $data_preview->lastItem() }} of {{ $data_preview->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_preview->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
@endsection