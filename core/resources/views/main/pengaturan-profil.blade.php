@extends('main.dashboard')
@extends('main.header')
@extends('main.footer')
@extends('main.menu')
@section('content')
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <div class="row">
                    <div class="col-12">

                        <!-- profile -->
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h4 class="card-title">Detail Profil</h4>
                                <a href="{{ route('ubahProfil') }}" class="btn btn-primary">
                                    <span class="">Ubah</span>
                                    <i data-feather='edit'></i>
                                </a>
                                
                            </div>
                            <div class="card-body py-2 my-25">
                                <!-- header section -->
                                <div class="d-flex">
                                    @if(Auth::user()->foto == NULL)
                                    <img src="{{asset('app-assets/images/avatars/2.png') }}" class="uploadedAvatar rounded me-50" alt="profile image" height="100" width="100" />   
                                    @else
                                     <img src="{{asset('app-assets/images/avatars/'.Auth::user()->foto) }}" class="uploadedAvatar rounded me-50" alt="profile image" height="100" width="100" />
                                    @endif   
                                    <!-- upload and reset button -->
                                    <!-- <div class="d-flex align-items-end mt-75 ms-1">
                                        <div>
                                            <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75">Ubah Foto</label>
                                            <input type="file" id="account-upload" hidden accept="image/*" />
                                            <button type="button" id="account-reset" class="btn btn-sm btn-outline-secondary mb-75">Reset</button>
                                            <p class="mb-0">Jenis file yang diizinkan: png, jpg, jpeg.</p>
                                        </div>
                                    </div> -->
                                    <!--/ upload and reset button -->
                                </div>
                                <!--/ header section -->

                                <!-- form -->
                                <form class="validate-form mt-2 pt-50" method="POST" action="#">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="accountEmail">Nama</label>
                                            <!-- <p id="accountFirstName" name="name" value="{{ Auth::user()->name }}" data-msg="Please">{{ Auth::user()->name }}</p> -->
                                            <input type="text" class="form-control" id="accountFirstName" name="name" value="{{ Auth::user()->name }}" data-msg="Please" />
                                        </div>
                                        <!-- <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="accountLastName">Last Name</label>
                                            <input type="text" class="form-control" id="accountLastName" name="lastName" placeholder="Doe" value="Doe" data-msg="Please enter last name" />
                                        </div> -->
                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="accountEmail">Email</label>
                                            <input type="email" class="form-control" id="accountEmail" name="email" placeholder="Email" value="{{ Auth::user()->email }}" />
                                        </div>

                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="accountOrganization">Alamat</label>
                                            <input type="text" class="form-control" id="accountOrganization" name="alamat_user" placeholder="Organization name" value="{{ Auth::user()->alamat_user }}"/>
                                        </div>

                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="accountPhoneNumber">Nomor Telepon</label>
                                            <input type="text" class="form-control account-number-mask" id="accountPhoneNumber" name="no_hp_user" placeholder="Phone Number" value="0{{ Auth::user()->no_hp_user }}"/>
                                        </div>
                                       
                                        <!-- <div class="col-12">
                                            <button type="submit" class="btn btn-primary mt-1 me-1">Simpan Perubahan</button>
                                            <button type="reset" class="btn btn-outline-secondary mt-1">Buang Perubahan</button>
                                        </div> -->
                                    </div>
                                </form>
                                <!--/ form -->
                            </div>
                        </div>

                        <!--/ profile -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection