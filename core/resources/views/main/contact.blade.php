@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Kontak</h4>
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Email</th>
                                            <th>Nomor Telepon</th>
                                            <th>Nomor Konsultasi</th>
                                            <th>Alamat</th>
                                            <th>Instagram</th>
                                            <th>Youtube</th>
                                            <th>Twitter</th>
                                            <th>Facebook</th>
                                            <th>LinkedIn</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_kontak as $key => $kontak)
                                            <td> {{ $data_kontak->firstItem() + $key }}</td>
                                            <td> {{ $kontak->email }}</td>
                                            <td> {{ $kontak->nomor_telepon }} </td>
                                            <td> {{ $kontak->nomor_konsultasi }} </td>
                                            <td> {{ substr($kontak->alamat, 0, 25) }}.... </td>
                                            <td> {{ $kontak->instagram}}</td>
                                            <td> {{ $kontak->youtube }}</td>
                                            <td> {{ $kontak->twitter }}</td>
                                            <td> {{ $kontak->facebook }}</td>
                                            <td> {{ $kontak->linkedin }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ url('/kontak/'.$kontak->id.'/edit') }}">
                                                    <span>Edit</span>
                                                </a>
                                            </td>
                                        </tr>    
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_kontak->firstItem() }} to {{ $data_kontak->lastItem() }} of {{ $data_kontak->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_kontak->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection