@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div>
            <div class="content-body">
                @if(session()->get('errors'))
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Upload Gagal!</h4>
                        <div class="alert-body">
                            {{ session()->get('errors')->first() }}
                        </div>
                    </div>
                @endif
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Transaksi Saya</h4>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Kode Bayar</th>
                                            <th>Layanan</th>
                                            <th>Harga</th>
                                            <th>Waktu</th>
                                            <th>Bukti Bayar</th>
                                            <th>No Rekening</th>
                                            <th>Status Pembayaran</th>
                                            <th>Konfirmasi Pembayaran</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @foreach ($transactions as $transaction)
                                            <td>KB-{{ $transaction->id }}</td>
                                            <td>
                                                @if($transaction->jenis_paket)
                                                    {{ $transaction->jenis_paket }}
                                                @else
                                                    <p>Paket Custom</p>
                                                @endif

                                            </td>
                                            <td>
                                                @if($transaction->harga)
                                                    Rp{{ number_format($transaction->harga, 0, '', '.') }}
                                                @else
                                                    <p>Dalam tahap negosiasi</p>
                                                @endif
                                            </td>
                                            <td>{{ $transaction->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                @if($transaction->bukti_bayar_lunas)
                                                    <div class="vertical-modal-ex">
                                                        <!-- Button trigger modal -->
                                                        <a href="" data-bs-toggle="modal" data-bs-target="#lihat-bukti-{{$transaction->id}}">
                                                            Lihat bukti bayar
                                                        </a>

                                                        <!-- Modal -->
                                                        <div class="modal fade" id="lihat-bukti-{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalScrollableTitle">Preview bukti bayar</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <img src="{{ $transaction->bukti_bayar_lunas }}" class="img-fluid rounded mx-auto d-block"/>                     
                                                                    </div>                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                       
                                                @else  
                                                <div class="vertical-modal-ex">
                                                    <button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#upload-bukti-{{$transaction->id}}">
                                                        <i data-feather='upload'></i>
                                                        <span>Unggah</span>
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="upload-bukti-{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Unggah Bukti Pembayaran</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="{{ route('upload-bukti-bayar') }}" method="post" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <p>Pilih file:</p>
                                                                        <input type="hidden" name="id" value="{{ $transaction->id }}">
                                                                        <input type="file" name="bukti_bayar_lunas" class="form-control @error('bukti_bayar_lunas') is-invalid @enderror" aria-describedby="inputGroupFileAddon04" aria-label="Upload" required>
                                                                        @error('bukti_bayar_lunas')
                                                                            <div class="invalid-feedback">
                                                                                {{ $message }}
                                                                            </div>  
                                                                        @enderror
                                                                        <br/>
                                                                        <p class="text-danger">
                                                                            Ekstensi file: .jpg, .jpeg, .png
                                                                            <br/>
                                                                            Ukuran gambar maks. 1 mb
                                                                        </p>        
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-primary">Unggah</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Vertical modal end-->
                                                @endif
                                            </td>
                                            <td>{{$metode_pembayaran->nama_provider}} - {{$metode_pembayaran->no_rekening}}</td>
                                            <td>
                                                @if($transaction->sisa_bayar == 0)
                                                    <a type="button" class="btn btn-outline-success round btn-sm" href="{{ url('invoice/'.$transaction->id.'') }}">Cek Status</a>
                                                @else
                                                    <a type="button" class="btn btn-outline-info round btn-sm" href="">Follow up</a>
                                                @endif          
                                            </td>
                                            <td><a type="button" class="btn btn-outline-info round btn-sm" href="https://wa.me/{{$contact->nomor_telepon}}">Konfirmasi</a></td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $transactions->firstItem() }} to {{ $transactions->lastItem() }} of {{ $transactions->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $transactions->links() }} 
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection 
