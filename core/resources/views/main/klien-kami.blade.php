@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Filter</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate action="{{route('klien-kami')}}" method="get">
                                    @csrf
                                    <div class="row">
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Nama Klien</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="nama" value="@if(isset($old_value['nama'])) {{$old_value['nama']}} @endif">
                                        </div>
                                        <div class="col-md-1 mt-2">
                                            <button type="submit" class="btn btn-primary" id="validationBioBootstrap">Cari</button>
                                        </div>
                                    </div>
                                    <hr>
                                </form>
                            </div>
                        </div>
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Klien Kami</h4>
                                <a href="{{ route('tambah-klien-kami') }}" class="btn btn-primary">Tambah Data</a>
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Klien</th>
                                            <th>Logo</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no=1;
                                            @endphp --}}

                                            @foreach ($klien_kami as $key => $k)
                                            <td>{{ $klien_kami->firstItem() + $key }}</td>
                                            <td>{{ $k->nama }}</td>
                                            <td>
                                                <img src="{{ asset('app-assets/images/pages/eCommerce/'.$k->logo) }}" class="img-thumbnail" alt="{{ $k->nama }}" style="width: 5cm;">
                                                {{-- <a href="{{ asset('app-assets/images/pages/eCommerce/'.$k->foto) }}" target="_blank" alt="" style="width: 5cm;">Lihat Gambar</a> --}}
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('edit-klien-kami/'.$k->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" onclick="deleteF('{{ url('delete-klien-kami/'.$k->id) }}')">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $klien_kami->firstItem() }} to {{ $klien_kami->lastItem() }} of {{ $klien_kami->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $klien_kami->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function deleteF(url){
        if(confirm('Apakah anda yakin?'))
        {
            window.location.href = url;
        }
    }
</script>
@endsection