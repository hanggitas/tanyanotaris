@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Filter</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate action="{{route('pekerjaan')}}" method="get">
                                    @csrf
                                    <div class="row">
                                        <div class="mb-1  col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Kode Bayar</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="basic-addon3">KB-</span>
                                                <input type="text" class="form-control" id="basic-url3" aria-describedby="basic-addon3" name="id" value="@if(isset($old_value['id'])) {{$old_value['id']}} @endif">
                                            </div>
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">User</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="email" value="@if(isset($old_value['email'])) {{$old_value['email']}} @endif">
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Layanan</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="jenis_paket" value="@if(isset($old_value['jenis_paket'])) {{$old_value['jenis_paket']}} @endif">
                                        </div>
                                        <div class="col-md-1 mt-2">
                                            <button type="submit" class="btn btn-primary" id="validationBioBootstrap">Cari</button>
                                        </div>
                                    </div>
                                    <hr>
                                </form>
                            </div>
                        </div>
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pekerjaan</h4>
                                {{-- <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('form_pekerjaan')}}" >Tambah Pekerjaan</a> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Kode Bayar</th>
                                            <th>User</th>
                                            <th>Layanan</th>
                                            <th>Status Pekerjaan</th>
                                            <th>Lampiran Hasil</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_pekerjaan as $key => $kerja)
                                            <td>KB-{{ $kerja->id_order }}</td>
                                            <td>{{ $kerja->email }}</td>
                                            <td>{{ $kerja->jenis_paket }}</td>
                                            <td>{{ $kerja->status_pekerjaan}}</td>
                                            <td>
                                                @if($kerja->link_project)
                                                <a class="btn btn-sm btn-primary" href="{{ url('/open/lampiran/'.$kerja->link_project)}}">
                                                    <span>Lihat Lampiran</span>
                                                </a>
                                                @else
                                                    <span>N/A</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('/pekerjaan/'.$kerja->id.'/edit') }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" onclick="deleteF('{{ url('/pekerjaan/'.$kerja->id.'/delete') }}')">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_pekerjaan->firstItem() }} to {{ $data_pekerjaan->lastItem() }} of {{ $data_pekerjaan->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_pekerjaan->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function deleteF(url){
        if(confirm('Apakah anda yakin?'))
        {
            window.location.href = url;
        }
    }
</script>
@endsection