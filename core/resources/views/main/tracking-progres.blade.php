@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 class="mt-3">Progres Proyek</h1>
            <p class="mt-1 mb-1 pb-75">
                Tempat melihat seberapa jauh progres pengerjaan proyek anda.
            </p>
        </div>
    </div>
               
                <!-- Timeline Starts -->
                <section class="basic-timeline">
                    <div class="row justify-content-center mt-5">
                        @foreach($pekerjaan as $item)
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Progres {{ $item->jenis_paket }}</h4>
                                </div> 
                                <div class="card-body">
                                    <ul class="timeline">
                                    @if($item->pre_produksi == 1) 
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h6>Pre-Produksi</h6>
                                                    <span class="timeline-event-time">{{ $item->updated_at->format('d-m-Y') }}</span>
                                                </div>
                                                <p>Proyek anda sedang dalam tahap pre-produksi</p>
                                                
                                            </div>
                                        </li>
                                    @endif
                                    @if($item->produksi == 1)
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h6>Produksi</h6>
                                                    <span class="timeline-event-time">{{ $item->updated_at->format('d-m-Y') }}</span>
                                                </div>
                                                <p>Proyek anda sedang dalam tahap produksi</p>
                                                
                                            </div>
                                        </li>
                                        @endif
                                        @if($item->post_produksi == 1)
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h6>Post-Produksi</h6>
                                                    <span class="timeline-event-time">{{ $item->updated_at->format('d-m-Y') }}</span>
                                                </div>
                                                <p>Proyek anda sedang dalam tahap post-produksi.</p>
                                            </div>
                                        </li>
                                        @endif
                                        @if($item->post_produksi == 1)
                                        <li class="timeline-item">
                                            <span class="timeline-point timeline-point-indicator"></span>
                                            <div class="timeline-event">
                                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                    <h6>Selesai</h6>
                                                    <span class="timeline-event-time">{{ $item->updated_at->format('d-m-Y') }}</span>
                                                </div>
                                                <p>Proyek anda telah selesai.</p>
                                                <!-- <a href="" class="btn btn-outline-primary btn-sm">Lihat Proyek</a> -->
                                            </div>
                                        </li>
                                        @endif
                                    </ul>
                                </div> 
                            </div>     
                            </div>
                        @endforeach    
                        </div>
                    </section>
</div>
@endsection