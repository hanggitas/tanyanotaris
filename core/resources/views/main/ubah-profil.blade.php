@extends('main.dashboard')
@extends('main.header')
@extends('main.footer')
@extends('main.menu')
@section('content')
 <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                @if(session()->get('errors'))
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Upload Gagal!</h4>
                        <div class="alert-body">
                            {{ session()->get('errors')->first() }}
                        </div>
                    </div>
                @endif
                @if(session()->get('notif'))
                <div class="alert alert-warning" role="alert">
                    <h4 class="alert-heading">Warning</h4>
                    <div class="alert-body">
                        {{ session()->get('notif') }}
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <!-- profile -->
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h4 class="card-title">ubah Profil</h4>
                            </div>
                            <div class="card-body">
                                <!-- form -->
                                <form class="validate-form mt-2 pt-50" method="post" action="{{ route('editProfil') }}" enctype="multipart/form-data">
                                    @csrf       
                                    <div class="row">
                                        <div class="col-12 col-sm-12 mb-1">
                                            <input type="hidden" name="id" value="{{ Auth::user()->id }}"/>
                                            <label class="form-label">Logo User</label>
                                            <input type="file" name="foto" class="form-control" aria-describedby="inputGroupFileAddon04" aria-label="Upload">
                                        </div>

                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="nama">Nama</label>
                                            <input type="text" class="form-control" id="nama" name="name" value="{{ Auth::user()->name }}" data-msg="Please"/>
                                        </div>

                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" disabled/>
                                        </div>

                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="alamat">Alamat</label>
                                            <input type="text" class="form-control" id="alamat" name="alamat_user" value="{{ Auth::user()->alamat_user }}"/>
                                        </div>

                                        <div class="col-12 col-sm-6 mb-1">
                                            <label class="form-label" for="no_hp">Nomor Telepon</label>
                                            <input type="text" class="form-control" id="no_hp" name="no_hp_user" value="0{{ Auth::user()->no_hp_user }}"/>
                                        </div>
                                        <br><br>
                                        
                                        <div class="col-12 col-sm-6 mb-1">
                                            <p id="notif-pw-blank"><strong style="color: white;">Password tidak sama</strong></p>
                                            <label class="form-label" for="pass">Password Baru</label>
                                            <input type="password" class="form-control" id="pass" name="pass_baru" value=""/>
                                        </div>
                                        
                                        <div class="col-12 col-sm-6 mb-1">
                                            <p id="notif-pw"><strong style="color: red;">Password tidak sama</strong></p>
                                            <label class="form-label" for="konfirm">Konfirmasi Password</label>
                                            <input type="password" class="form-control" id="konfirm" name="konfirm_pass" value=""/>
                                        </div>
                                       
                                        <div class="col-12">
                                            <button id="button_submit" type="submit" class="btn btn-primary mt-3 me-1">
                                                Simpan Perubahan
                                                <i data-feather='save'></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!--/ form -->
                            </div>
                        </div>
                        <!--/ profile -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $("#notif-pw").hide();
        $("#notif-pw-blank").hide();
    });
</script>
<script>
    $("#konfirm").keyup(function(){
        var new_pass = $("#pass").val();
        var konfirm = $("#konfirm").val();

        if(konfirm == null || konfirm == '')
        {
            $("#notif-pw").hide();
            $("#notif-pw-blank").hide();
            $("#button_submit").removeAttr("disabled");
        }

        if(konfirm == new_pass)
        {
            $("#notif-pw").hide();
            $("#notif-pw-blank").hide();
            $("#button_submit").removeAttr("disabled");
        }
        else
        {
            $("#notif-pw").show();
            $("#notif-pw-blank").show();
            $("#button_submit").attr("disabled", true);
        }
    });
    $("#pass").keyup(function(){
        var new_pass = $("#pass").val();
        var konfirm = $("#konfirm").val();

        if(konfirm == null || konfirm == '')
        {
            $("#notif-pw").hide();
            $("#notif-pw-blank").hide();
            $("#button_submit").removeAttr("disabled");
        }

        if(konfirm == new_pass)
        {
            $("#notif-pw").hide();
            $("#notif-pw-blank").hide();
            $("#button_submit").removeAttr("disabled");
        }
        else
        {
            $("#notif-pw").show();
            $("#notif-pw-blank").show();
            $("#button_submit").attr("disabled", true);
        }
    });
</script>
@endsection