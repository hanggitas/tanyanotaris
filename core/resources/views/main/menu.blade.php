@section('menu')
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="">
                <span class="brand-logo">
                    <img src="{{ asset('app-assets/images/ico/Logo.png') }}"/>
                </span>
                    <h2 class="brand-text">Tanya Notaris</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @if(Auth::user()->role == 'admin')
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('dashboard') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('beranda') }}"><i data-feather='home'></i></i><span class="menu-title text-truncate" data-i18n="Datatable">Home Page</span></a>
                </li> 
                <li class=" navigation-header"><span>Menu</span><i data-feather="more-horizontal"></i>
                </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('user') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Manajemen User</span></a>
                </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Daftar Layanan">Daftar Layanan</span></a>
                    <ul class="menu-content">
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('servis') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Layanan</span></a>
                        </li>
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('kategori') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Kategori</span></a>
                        </li>
                    </ul>
                </li>


                    <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="layout"></i><span class="menu-title text-truncate" data-i18n="Daftar Layanan">Daftar Konten</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="{{ route('klien-kami')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Klien Kami</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('kategori-artikel')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Kategori Artikel</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('artikel')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Artikel</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('image-banner')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Image Banner</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('banner')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Banner</span></a>
                        </li>
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('faq') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">FAQ</span></a>
                        </li>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('testimoni')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Testimoni</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('pencapaian')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Pencapaian</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="{{ route('kontak')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Kontak</span></a>
                        </li>
                    </ul>
                </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('order') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Order</span></a>
                    </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('pekerjaan') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Pekerjaan</span></a>
                </li>
              
            @else

                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('beranda') }}"><i data-feather='home'></i></i><span class="menu-title text-truncate" data-i18n="Datatable">Home Page</span></a>
                </li> 
                <li class=" navigation-header"><span>Menu</span><i data-feather="more-horizontal"></i>
                </li>
                          
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('projek') }}"><i data-feather='folder'></i><span class="menu-title text-truncate" data-i18n="Datatable">Status Pekerjaan</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('transaksi') }}"><i data-feather='file-text'></i></i><span class="menu-title text-truncate" data-i18n="Datatable">Transaksi</span></a>
                    </li>       
            @endif
        </ul>
    </div>
</div>
@endsection