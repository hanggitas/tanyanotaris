@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Filter</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate action="{{route('artikel')}}" method="get">
                                    @csrf
                                    <div class="row">
                                        <div class="mb-1 col-md-3">
                                            <label class="form-label" for="basic-addon-name">Kategori</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_kategori">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach($kategori_all as $item)
                                                    @if(isset($old_value['id_kategori']))
                                                        @if($item->id == $old_value['id_kategori'])
                                                            <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                                                        @endif
                                                    @endif
                                                        <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                @endforeach
                                            </select>    
                                            <div class="invalid-feedback">Silahkan Pilih Kategori</div>
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label class="d-block form-label" for="validationBioBootstrap">Judul</label>
                                            <input type="text" class="form-control" id="validationBioBootstrap" name="judul" value="@if(isset($old_value['judul'])) {{$old_value['judul']}} @endif">
                                        </div>
                                        <div class="col-md-1 mt-2">
                                            <button type="submit" class="btn btn-primary" id="validationBioBootstrap">Cari</button>
                                        </div>
                                    </div>
                                    <hr>
                                </form>
                            </div>
                        </div>
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Artikel</h4>
                                <a href="{{ route('tambahartikel') }}" class="btn btn-primary">Tambah Artikel</a>
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori Artikel</th>
                                            <th>Judul Artikel</th>
                                            <th>Tanggal Posting</th>
                                            <!-- <th>Status</th> -->
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no=1;
                                            @endphp --}}

                                            @foreach ($artikels as $key => $artikel)
                                            <td>{{ $artikels->firstItem() + $key }}</td>
                                            <td>{{ $artikel->nama_kategori }}</td>
                                            <td>{{ $artikel->judul }}</td>
                                            <td>{{ $artikel->updated_at}}</td>
                                            
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('edit-artikel/'.$artikel->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" onclick="deleteF('{{ url('delete-artikel/'.$artikel->id) }}')">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $artikels->firstItem() }} to {{ $artikels->lastItem() }} of {{ $artikels->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $artikels->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function deleteF(url){
        if(confirm('Apakah anda yakin?'))
        {
            window.location.href = url;
        }
    }
</script>
@endsection