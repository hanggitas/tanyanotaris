@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        @if(session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('success') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Error</h4>
                            <div class="alert-body">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                        @endif
                        @if(session()->get('status'))
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Success</h4>
                            <div class="alert-body">
                                {{ session()->get('status') }}
                            </div>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Pencapaian</h4>
                                {{-- <a href="{{ route('tambahpencapaian') }}" class="btn btn-primary">Tambah Pencapaian</a> --}}
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Klien Senang</th>
                                            <th>Kasus</th>
                                            <th>Kasus Menang</th>
                                            <th>Penghargaan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no=1;
                                            @endphp --}}

                                            @foreach ($capai as $key => $c)
                                            <td>{{ $capai->firstItem() + $key }}</td>
                                            <td>{{ $c->klien_senang }}</td>
                                            <td>{{ $c->kasus }}</td>
                                            <td>{{ $c->kasus_menang }}</td>
                                            <td>{{ $c->penghargaan }}</td>
                                            <td>
                                                <div>
                                                        <a class="btn btn-info" href="{{ url('edit-pencapaian/'.$c->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $capai->firstItem() }} to {{ $capai->lastItem() }} of {{ $capai->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $capai->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection