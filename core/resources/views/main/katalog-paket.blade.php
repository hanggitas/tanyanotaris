@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('main.content')
@extends('main.footer')
@section('content')
  <!-- BEGIN: Content-->
<style>
    .scrollable{
        overflow-y: auto;
        max-height: 150px;
        /*height: 100px;*/
    }
</style>
    <!-- .scrollable::-webkit-scrollbar{
        display: none;
    } -->
<div class="app-content content">
    <div class="row">
        <section id="ecommerce-searchbar" class="ecommerce-searchbar">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <div class="input-group input-group-merge text-center">
                        <input type="text" class="form-control search-product" id="shop-search" placeholder="Cari Paket" aria-label="Search..." aria-describedby="shop-search" />
                        <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </section>
        <div class="col-lg-12 text-center">
            <h1 class="mt-3">Katalog Paket</h1>
            <p class="mt-1 mb-1 pb-75">
                Pilih paket yang sesuai dengan kebutuhan anda.
            </p>
        </div>
        <div class="row justify-content-center mt-5">
            @foreach($services as $service)
            <div class="col-12 col-md-3">
                <div class="card basic-pricing text-center">
                    <div class="card-body">
                        <img src="{{ asset('app-assets/images/illustration/Pot1.svg') }}" class="mb-2 mt-5" alt="svg img" />
                        <h3>{{ $service->jenis_paket }}</h3>
                        <!-- <p class="card-text">A simple start for everyone</p> -->
                        <div class="annual-plan">
                            <div class="plan-price mt-2">
                                <sup class="font-medium-1 fw-bold text-primary">Rp</sup>
                                <span class="pricing-basic-value fw-bolder text-primary mb-1">{{ number_format($service->harga, 0, '', '.') }}</span>
                            </div>
                            <small class="annual-pricing d-none text-muted"></small>
                        </div>
                        <br/>
                        <div class="scrollable overflow-auto">
                            <ul class="list-group list-group-circle text-start">
                                @php
                                    $deskripsi = explode(',', $service->detail_paket);
                                @endphp
                                @foreach($deskripsi as $desc)
                                    <li class="list-group-item">{{ $desc }}</li>
                                @endforeach                
                            </ul>
                        </div>         
                        <a href="{{url('/katalog/detail-katalog/detail-paket/checkout/'.$service->id.'')}}" class="btn w-100 btn-primary mt-2">Beli Paket</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <!-- pricing free trial -->
        <div class="pricing-free-trial mt-2">
            <div class="row">
                <div class="col-12 col-lg-10 col-lg-offset-3 mx-auto">
                    <div class="pricing-trial-content d-flex justify-content-between">
                        <!-- <div class="text-center text-md-start mt-3">
                            <h3 class="text-primary">Masih belum menemukan paket?</h3>
                            <h5>Silahkan hubungi kami untuk mengajukan paket yang sesuai dengan kebutuhan anda!</h5>
                                                
                        <div class="modal-size-default d-inline-block"> -->
                        <!-- Button trigger modal -->
                            <!-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#defaultSize">
                                Buat Paket Custom
                            </button> -->
                            <!-- Modal -->
                                <div class="modal fade text-start" id="defaultSize" tabindex="-1" aria-labelledby="myModalLabel18" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel18">Paket Custom</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <form action="{{ route('bayarCustom') }}" method="post">
                                                @csrf
                                                <div class="modal-body">
                                                    <input type="hidden" name="tipe_order" value="custom" />
                                                    <input type="hidden" name="sisa_bayar" value="0" />
                                                    <input type="hidden" name="total_dibayar" value="0" />
                                                    <input type="hidden" name="total_harga" value="0" />
                                                    <input type="hidden" name="status_pembayaran" value="Menunggu" />
                                                    <input type="hidden" name="id" value="{{ $services[0]['id_kat'] }}" />
                                                    <label class="mb-1">Detail Paket yang diinginkan:</label>
                                                    <textarea rows="5" class="form-control" name="paket_custom" placeholder="Tuliskan detail paket yang sesuai dengan kebutuhan anda..." autofocus></textarea>
                                                </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Ajukan Paket</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                            <!-- image -->
                                <!-- <img src="{{ asset('app-assets/images/illustration/pricing-Illustration.svg') }}" class="pricing-trial-img img-fluid" alt="svg img" /> -->
                    </div>
                </div>
            </div>
        </div>
        <!--/ pricing free trial -->
    </div>
</div>
<!-- END: Content-->
@endsection