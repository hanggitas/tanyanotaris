<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\SubkategoriController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Models\LogVisitor;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ip', function (Request $request) {
    $bot_regex = '/BotLink|bingbot|AhrefsBot|ahoy|AlkalineBOT|anthill|appie|arale|araneo|AraybOt|ariadne|arks|ATN_Worldwide|Atomz|bbot|Bjaaland|Ukonline|borg\-bot\/0\.9|boxseabot|bspider|calif|christcrawler|CMC\/0\.01|combine|confuzzledbot|CoolBot|cosmos|Internet Cruiser Robot|cusco|cyberspyder|cydralspider|desertrealm, desert realm|digger|DIIbot|grabber|downloadexpress|DragonBot|dwcp|ecollector|ebiness|elfinbot|esculapio|esther|fastcrawler|FDSE|FELIX IDE|ESI|fido|H�m�h�kki|KIT\-Fireball|fouineur|Freecrawl|gammaSpider|gazz|gcreep|golem|googlebot|griffon|Gromit|gulliver|gulper|hambot|havIndex|hotwired|htdig|iajabot|INGRID\/0\.1|Informant|InfoSpiders|inspectorwww|irobot|Iron33|JBot|jcrawler|Teoma|Jeeves|jobo|image\.kapsi\.net|KDD\-Explorer|ko_yappo_robot|label\-grabber|larbin|legs|Linkidator|linkwalker|Lockon|logo_gif_crawler|marvin|mattie|mediafox|MerzScope|NEC\-MeshExplorer|MindCrawler|udmsearch|moget|Motor|msnbot|muncher|muninn|MuscatFerret|MwdSearch|sharp\-info\-agent|WebMechanic|NetScoop|newscan\-online|ObjectsSearch|Occam|Orbsearch\/1\.0|packrat|pageboy|ParaSite|patric|pegasus|perlcrawler|phpdig|piltdownman|Pimptrain|pjspider|PlumtreeWebAccessor|PortalBSpider|psbot|Getterrobo\-Plus|Raven|RHCS|RixBot|roadrunner|Robbie|robi|RoboCrawl|robofox|Scooter|Search\-AU|searchprocess|Senrigan|Shagseeker|sift|SimBot|Site Valet|skymob|SLCrawler\/2\.0|slurp|ESI|snooper|solbot|speedy|spider_monkey|SpiderBot\/1\.0|spiderline|nil|suke|http:\/\/www\.sygol\.com|tach_bw|TechBOT|templeton|titin|topiclink|UdmSearch|urlck|Valkyrie libwww\-perl|verticrawl|Victoria|void\-bot|Voyager|VWbot_K|crawlpaper|wapspider|WebBandit\/1\.0|webcatcher|T\-H\-U\-N\-D\-E\-R\-S\-T\-O\-N\-E|WebMoose|webquest|webreaper|webs|webspider|WebWalker|wget|winona|whowhere|wlm|WOLP|WWWC|none|XGET|Nederland\.zoek|AISearchBot|woriobot|NetSeer|Nutch|YandexBot|YandexMobileBot|SemrushBot|FatBot|MJ12bot|DotBot|AddThis|baiduspider|SeznamBot|mod_pagespeed|CCBot|openstat.ru\/Bot|m2e/i';
    $userAgent = empty($_SERVER['HTTP_USER_AGENT']) ? FALSE : $_SERVER['HTTP_USER_AGENT'];
    $isBot = !$userAgent || preg_match($bot_regex, $userAgent);
    if($isBot == true)
    {
        return response()->json(['success' => false, 'message' => 'Bot terdeteksi'], 200);
    }

    $check = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
    $cek_data = LogVisitor::where('ip', $check->ip)->orderBy('id','DESC')->first();
    if($cek_data)
    {
        $waktu_awal = strtotime($cek_data->created_at);
        $waktu_akhir = strtotime(date('Y-m-d H:i:s')); // bisa juga waktu sekarang now()

        //menghitung selisih dengan hasil detik
        $menit = ($waktu_akhir - $waktu_awal) / 60;
    
        if($menit > 5)
        {
            $create_data = LogVisitor::create([
                'kota' => $check->city,
                'negara' => $check->country,
                'ip' => $check->ip,
            ]);
            return response()->json(['success' => true, 'message' => 'Visitor baru berhasil disimpan'], 200);
        }
        else
        {
            return response()->json(['success' => true, 'message' => 'Visitor lama terdeteksi'], 200);
        }
    }
    else
    {
        $create_data = LogVisitor::create([
            'kota' => $check->city,
            'negara' => $check->country,
            'ip' => $check->ip,
         ]);
         return response()->json(['success' => true, 'message' => 'Visitor baru berhasil disimpan'], 200);
    }
})->name('getIP');


Route::post('/checkout-layanan/{id}', [App\Http\Controllers\LandingPage\IndexController::class, 'checkout'])->name('checkoutLayanan');
Route::post('/order', [App\Http\Controllers\LandingPage\IndexController::class, 'order']);
Route::get('/layanan-kami/{id}', [App\Http\Controllers\LandingPage\IndexController::class, 'layananKategori']);
Route::get('/layanan-kami', [App\Http\Controllers\LandingPage\IndexController::class, 'layanan'])->name('layanan');
Route::get('/layanan-kami/detail/{id}', [App\Http\Controllers\LandingPage\IndexController::class, 'detail'])->name('detail');
Route::get('/faq-landing', [App\Http\Controllers\LandingPage\IndexController::class, 'faq'])->name('faq-landing');
Route::get('/mengapa-kami', [App\Http\Controllers\LandingPage\HomeController::class, 'mengapa'])->name('mengapa');
Route::get('/artikel-kami', [App\Http\Controllers\LandingPage\IndexController::class, 'artikelkami'])->name('artikelKami');
Route::get('/artikel-kategori/{id}', [App\Http\Controllers\LandingPage\IndexController::class, 'kategoriartikel'])->name('kategoriartikel');
Route::get('/artikel-detail/{id}', [App\Http\Controllers\LandingPage\IndexController::class, 'detailartikel'])->name('detailartikel');
Route::get('/', [App\Http\Controllers\LandingPage\HomeController::class, 'home'])->name('beranda');

Route::get('/masuk', [App\Http\Controllers\Auth\LoginController::class, 'masuk'])->name('masuk')->middleware('guest');


Route::get('/email/verify', function () {
    return view('auth.verify');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/login');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.resend');

Auth::routes();

Route::group(['middleware' => ['verified']], function () {
    /**
     * Dashboard Routes
     */
    Route::get('/ubah-profil', [App\Http\Controllers\ClientController::class, 'ubahProfil'])->name('ubahProfil');
    Route::post('/edit-profil', [App\Http\Controllers\ClientController::class, 'editProfil'])->name('editProfil');
    Route::get('/pengaturan-profil', [App\Http\Controllers\ClientController::class, 'pengaturanProfil'])->name('pengaturanProfil');
    Route::get('/progres-proyek', [App\Http\Controllers\ClientController::class, 'trackingProgres'])->name('trackingProgres');
    Route::get('/feedback', [App\Http\Controllers\ClientController::class, 'feedback'])->name('feedback');
    Route::post('/post-feedback', [App\Http\Controllers\ClientController::class, 'postfeedback'])->name('post-feedback');
    Route::get('/proyek-saya', [App\Http\Controllers\ClientController::class, 'projekSaya'])->name('projek');
    Route::get('/invoice/{id}', [App\Http\Controllers\ClientController::class, 'invoice']);
    Route::get('/download-pdf/{id}', [App\Http\Controllers\ClientController::class, 'downloadPDF']);
    Route::get('/transaksi', [App\Http\Controllers\ClientController::class, 'transaksi'])->name('transaksi');
    Route::post('/upload-bukti-pembayaran', [App\Http\Controllers\ClientController::class, 'postBuktiBayar'])->name('upload-bukti-bayar');
    Route::post('/upload-bukti-pembayaran-dp', [App\Http\Controllers\ClientController::class, 'postBuktiBayarDP'])->name('upload-bukti-bayar-dp');
    Route::get('/katalog', [App\Http\Controllers\DashboardController::class, 'katalog'])->name('katalog');

    Route::get('/detail-paket', [App\Http\Controllers\ClientController::class, 'detailpaket'])->name('detailpaket');
    Route::get('/katalog', [App\Http\Controllers\ClientController::class, 'index']);
    Route::get('/katalog/detail-katalog/{id}', [App\Http\Controllers\ClientController::class, 'detailkatalog']);
    Route::get('/katalog/detail-katalog/detail-paket/{id}', [App\Http\Controllers\ClientController::class, 'detailpaket']);
    Route::get('/katalog/detail-katalog/detail-paket/checkout/{id}', [App\Http\Controllers\ClientController::class, 'checkout']);
    Route::post('/bayar', [App\Http\Controllers\ClientController::class, 'bayar'])->name('bayar');
    Route::post('/bayar-custom', [App\Http\Controllers\ClientController::class, 'bayarCustom'])->name('bayarCustom');



    Route::get('/katalog', [App\Http\Controllers\ClientController::class, 'index']);
    Route::get('/detail-item/{id}', [App\Http\Controllers\ClientController::class, 'getDetail']);

    Route::get('/dashboard-admin', [App\Http\Controllers\DashboardController::class, 'dashboard'])->name('dashboard');
    Route::post('/post-targer', [App\Http\Controllers\DashboardController::class, 'posttarget'])->name('target');

    Route::get('/akun', [App\Http\Controllers\AkunController::class, 'akun'])->name('akun');
    Route::get('/edit', [UpdateProfileInformationController::class, 'edit'])->name('edit.akun');
    // Route::put('/update', [UpdateProfileInformationController::class, 'update'])->name('update.akun');
    // Route::post('/updateakun', [App\Http\Controllers\AkunController::class, 'update'])->name('updateakun');
    Route::get('/password', [App\Http\Controllers\DashboardController::class, 'password'])->name('password');

    Route::get('/portofolio', [App\Http\Controllers\PortofolioController::class, 'portofolio'])->name('portofolio');
    Route::get('/tambah-data-portofolio', [App\Http\Controllers\PortofolioController::class, 'tambah_data_portofolio'])->name('tambah_data_portofolio');
    Route::get('/edit-portofolio/{id}', [App\Http\Controllers\PortofolioController::class, 'edit'])->name('edit');
    Route::put('/update-portofolio/{id}', [App\Http\Controllers\PortofolioController::class, 'update'])->name('update');
    Route::get('/delete-portofolio/{id}', [App\Http\Controllers\PortofolioController::class, 'delete'])->name('delete');

    Route::post('/postservis', [App\Http\Controllers\PortofolioController::class, 'postportofolio'])->name('data_portofolio');

    Route::get('/servis', [App\Http\Controllers\ServicesController::class, 'servis'])->name('servis');
    Route::get('/tambah-data-servis', [App\Http\Controllers\ServicesController::class, 'tambah_data_service'])->name('tambah_data_service');
    Route::post('/postservis', [App\Http\Controllers\ServicesController::class, 'postservice'])->name('data_service');
    Route::get('/servis/{id}/edit', [App\Http\Controllers\TaskController::class, 'edit'])->name('edit');
    Route::post('/servis/{id}/update', [App\Http\Controllers\TaskController::class, 'update'])->name('update');
    Route::get('/servis/{id}/delete', [App\Http\Controllers\TaskController::class, 'delete'])->name('delete');

    Route::post('/postportofolio', [App\Http\Controllers\PortofolioController::class, 'postportofolio'])->name('data_portofolio');

    Route::get('/servis', [App\Http\Controllers\ServicesController::class, 'servis'])->name('servis');
    Route::get('/tambah-data-service', [App\Http\Controllers\ServicesController::class, 'tambah_data_service'])->name('tambah_data_service');
    Route::post('/postservice', [App\Http\Controllers\ServicesController::class, 'postservice'])->name('data_service');
    Route::get('/edit-service/{id}', [App\Http\Controllers\ServicesController::class, 'edit']);
    Route::put('/update-service/{id}', [App\Http\Controllers\ServicesController::class, 'update']);
    Route::get('/delete-service/{id}', [App\Http\Controllers\ServicesController::class, 'delete']);

    Route::get('/kategori', [App\Http\Controllers\KategoriController::class, 'kategori'])->name('kategori');
    Route::get('/tambah-data-kategori', [App\Http\Controllers\KategoriController::class, 'tambah_data_kategori'])->name('tambah_data_kategori');
    Route::post('/postkategori', [App\Http\Controllers\KategoriController::class, 'postkategori'])->name('data_kategori');
    Route::get('/kategori/{id}/edit', [App\Http\Controllers\KategoriController::class, 'edit'])->name('edit');
    Route::post('/kategori/{id}/update', [App\Http\Controllers\KategoriController::class, 'update'])->name('update');
    Route::get('/kategori/{id}/delete', [App\Http\Controllers\KategoriController::class, 'delete'])->name('delete');
    Route::get('/form-kategori', [App\Http\Controllers\KategoriController::class, 'form-kategori'])->name('form-kategori');
    Route::get('/edit-kategori/{id}', [App\Http\Controllers\KategoriController::class, 'edit']);
    Route::put('/update-kategori/{id}', [App\Http\Controllers\KategoriController::class, 'update']);
    Route::get('/delete-kategori/{id}', [App\Http\Controllers\KategoriController::class, 'delete']);

    Route::get('/subkategori', [App\Http\Controllers\SubkategoriController::class, 'subkategori'])->name('subkategori');

    Route::get('/tambah-data-subkategori', [App\Http\Controllers\SubkategoriController::class, 'tambah_data_subkategori'])->name('tambah_data_subkategori');


    Route::post('/postsubkategori', [App\Http\Controllers\SubkategoriController::class, 'postsubkategori'])->name('data_subkategori');
    Route::get('/edit-subkategori/{id}', [App\Http\Controllers\SubkategoriController::class, 'edit']);
    Route::put('/update-subkategori/{id}', [App\Http\Controllers\SubkategoriController::class, 'update']);
    Route::get('/delete-subkategori/{id}', [App\Http\Controllers\SubkategoriController::class, 'delete']);

    Route::get('/order', [App\Http\Controllers\OrderController::class, 'order'])->name('order');
    Route::get('/formorder', [App\Http\Controllers\OrderController::class, 'formorder'])->name('formorder');
    Route::post('/postorder', [App\Http\Controllers\OrderController::class, 'postorder'])->name('data_order');
    Route::get('/edit-order/{id}', [App\Http\Controllers\OrderController::class, 'edit']);
    Route::post('/order/{id}', [App\Http\Controllers\OrderController::class, 'updateorder'])->name('updateorder');
    Route::get('/delete-order/{id}', [App\Http\Controllers\OrderController::class, 'delete']);
    Route::get('/custom-order/{id}', [App\Http\Controllers\OrderController::class, 'custom']);
    Route::post('/post-custom', [App\Http\Controllers\OrderController::class, 'postcustom'])->name('custom_order');

    Route::get('/detail', [App\Http\Controllers\DetailController::class, 'detail'])->name('detail');
    Route::get('/formdetail', [App\Http\Controllers\DetailController::class, 'formdetail'])->name('formdetail');
    Route::post('/postdetail', [App\Http\Controllers\DetailController::class, 'postdetail'])->name('data_detail');
    Route::get('/delete-detail/{id}', [App\Http\Controllers\DetailController::class, 'delete']);

    Route::get('/kontak', [App\Http\Controllers\ContactController::class, 'kontak'])->name('kontak');
    Route::get('/formkontak', [App\Http\Controllers\ContactController::class, 'formkontak'])->name('formkontak');
    Route::post('/postkontak', [App\Http\Controllers\ContactController::class, 'postkontak'])->name('data_kontak');
    Route::get('/kontak/{id}/edit', [App\Http\Controllers\ContactController::class, 'editkontak'])->name('edit_kontak');
    Route::post('/kontak/{id}/update', [App\Http\Controllers\ContactController::class, 'updatekontak'])->name('update_kontak');
    Route::get('/kontak/{id}/delete', [App\Http\Controllers\ContactController::class, 'deletekontak'])->name('delete_kontak');

    Route::get('/progres', [App\Http\Controllers\ProgresController::class, 'progres'])->name('progres');
    Route::post('/postprogres', [App\Http\Controllers\ProgresController::class, 'postprogres'])->name('data_progres');
    Route::get('/edit-progres/{id}', [App\Http\Controllers\ProgresController::class, 'edit']);
    Route::put('/update-progres/{id}', [App\Http\Controllers\ProgresController::class, 'update']);
    Route::get('/delete-progres/{id}', [App\Http\Controllers\ProgresController::class, 'delete']);

    Route::get('/task', [App\Http\Controllers\TaskController::class, 'task'])->name('task');
    Route::get('/task/{id}/edit', [App\Http\Controllers\TaskController::class, 'edittask'])->name('edittask');
    Route::post('/task/{id}/update', [App\Http\Controllers\TaskController::class, 'updatetask'])->name('updatetask');
    Route::get('/task/{id}/delete', [App\Http\Controllers\TaskController::class, 'deletetask'])->name('deletetask');
    Route::get('/formtask', [App\Http\Controllers\TaskController::class, 'formtask'])->name('formtask');
    Route::post('/posttask', [App\Http\Controllers\TaskController::class, 'posttask'])->name('data_task');

    Route::get('/detailtask', [App\Http\Controllers\DetailTaskController::class, 'detailtask'])->name('detailtask');
    Route::get('/detailtask/{id}/edit', [App\Http\Controllers\DetailTaskController::class, 'editdetailtask'])->name('editdetailtask');
    Route::post('/detailtask/{id}/update', [App\Http\Controllers\DetailTaskController::class, 'updatedetailtask'])->name('updatedetailtask');
    Route::get('/detailtask/{id}/delete', [App\Http\Controllers\DetailTaskController::class, 'deletedetailtask'])->name('deletedetailtask');
    Route::get('/formdetailtask', [App\Http\Controllers\DetailTaskController::class, 'formdetailtask'])->name('formdetailtask');
    Route::post('/postdetailtask', [App\Http\Controllers\DetailTaskController::class, 'postdetailtask'])->name('data_detail_task');

    Route::get('/metodepembayaran', [App\Http\Controllers\MetodePembayaranController::class, 'metodepembayaran'])->name('metodepembayaran');
    Route::get('/metodepembayaran/{id}/edit', [App\Http\Controllers\MetodePembayaranController::class, 'editmetodepembayaran'])->name('editmetodepembayaran');
    Route::post('/metodepembayaran/{id}/update', [App\Http\Controllers\MetodePembayaranController::class, 'updatemetodepembayaran'])->name('updatemetodepembayaran');
    Route::get('/metodepembayaran/{id}/delete', [App\Http\Controllers\MetodePembayaranController::class, 'deletemetodepembayaran'])->name('deletemetodepembayaran');
    Route::get('/formmetodepembayaran', [App\Http\Controllers\MetodePembayaranController::class, 'formmetodepembayaran'])->name('formmetodepembayaran');
    Route::post('/postmetodepembayaran', [App\Http\Controllers\MetodePembayaranController::class, 'postmetodepembayaran'])->name('data_metode_pembayaran');

    Route::get('/preview', [App\Http\Controllers\PreviewController::class, 'preview'])->name('preview');
    Route::get('/preview/{id}/edit', [App\Http\Controllers\PreviewController::class, 'editpreview'])->name('editpreview');
    Route::post('/preview/{id}/update', [App\Http\Controllers\PreviewController::class, 'updatepreview'])->name('updatepreview');
    Route::get('/preview/{id}/delete', [App\Http\Controllers\PreviewController::class, 'deletepreview'])->name('deletepreview');
    Route::get('/formpreview', [App\Http\Controllers\PreviewController::class, 'formpreview'])->name('formpreview');
    Route::post('/postpreview', [App\Http\Controllers\PreviewController::class, 'postpreview'])->name('data_preview');

    Route::get('/servicesubkategori', [App\Http\Controllers\ServiceSubkategoriController::class, 'service_subkategori'])->name('servicesubkategori');
    Route::get('/form-Service-Subkategori', [App\Http\Controllers\ServiceSubkategoriController::class, 'formServiceSubkategori'])->name('form_Service_Subkategori');
    Route::post('/postservicesubbkategori', [App\Http\Controllers\ServiceSubkategoriController::class, 'postservicesubkategori'])->name('data_service_subkategori');
    Route::get('/servicesubkategori/{id}/edit', [App\Http\Controllers\ServiceSubkategoriController::class, 'editservicesubkategori'])->name('edit_servicesubkategori');
    Route::post('/servicesubkategori/{id}/update', [App\Http\Controllers\ServiceSubkategoriController::class, 'updateservicesubkategori'])->name('update_servicesubkategori');
    Route::get('/servicesubkategori/{id}/delete', [App\Http\Controllers\ServiceSubkategoriController::class, 'deleteservicesubkategori'])->name('deletesubkategori');

    Route::get('/pekerjaan', [App\Http\Controllers\PekerjaanController::class, 'pekerjaan'])->name('pekerjaan');
    Route::get('/form-pekerjaan', [App\Http\Controllers\PekerjaanController::class, 'formpekerjaan'])->name('form_pekerjaan');
    Route::post('/postpekerjaan', [App\Http\Controllers\PekerjaanController::class, 'postpekerjaan'])->name('data_pekerjaan');
    Route::get('/pekerjaan/{id}/edit', [App\Http\Controllers\PekerjaanController::class, 'edittpekerjaan'])->name('edit_pekerjaan');
    Route::post('/pekerjaan/{id}/update', [App\Http\Controllers\PekerjaanController::class, 'updatepekerjaan'])->name('update_pekerjaan');
    Route::get('/pekerjaan/{id}/delete', [App\Http\Controllers\PekerjaanController::class, 'hapuspekerjaan'])->name('deletepekerjaan');
    Route::get('/open/lampiran/{nama_file}', [App\Http\Controllers\PekerjaanController::class, 'open_lampiran'])->name('open_lampiran');

    Route::get('/user', [App\Http\Controllers\UserController::class, 'user'])->name('user');
    Route::post('/postuser', [App\Http\Controllers\UserController::class, 'postuser'])->name('data_user');
    Route::get('/edit-user/{id}', [App\Http\Controllers\UserController::class, 'edit']);
    Route::put('/update-user/{id}', [App\Http\Controllers\UserController::class, 'update']);
    Route::get('/delete-user/{id}', [App\Http\Controllers\UserController::class, 'delete']);

    Route::get('/checkout', [App\Http\Controllers\ClientController::class, 'checkout'])->name('checkout');
    Route::post('/bayar', [App\Http\Controllers\ClientController::class, 'bayar'])->name('bayar');

    Route::get('/banner', [App\Http\Controllers\LandingPage\HomeController::class, 'banner'])->name('banner');
    Route::get('/tambah-banner', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_banner'])->name('tambahbanner');
    Route::post('/postbanner', [App\Http\Controllers\LandingPage\HomeController::class, 'postbanner'])->name('data_banner');
    Route::post('/update-data-banner/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'updatebanner']);
    Route::get('/edit-banner/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'edit_banner']);
    Route::get('/delete-banner/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete']);

    Route::get('/image-banner', [App\Http\Controllers\LandingPage\HomeController::class, 'image_banner'])->name('image-banner');
    Route::get('/tambah-image-banner', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_image_banner'])->name('tambahimagebanner');
    Route::post('/postimagebanner', [App\Http\Controllers\LandingPage\HomeController::class, 'postimagebanner'])->name('data_image_banner');
    Route::get('/delete-image-banner/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete_image_banner']);

    Route::get('/artikel', [App\Http\Controllers\LandingPage\HomeController::class, 'artikel'])->name('artikel');
    Route::get('/tambah-artikel', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_artikel'])->name('tambahartikel');
    Route::post('/postartikel', [App\Http\Controllers\LandingPage\HomeController::class, 'postartikel'])->name('data_artikel');
    Route::post('/updateartikel/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'updateartikel'])->name('update_artikel');
    Route::get('/edit-artikel/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'edit_artikel']);
    Route::get('/delete-artikel/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete_artikel']);

    Route::get('/kategori-artikel', [App\Http\Controllers\LandingPage\KategoriArtikelController::class, 'getList'])->name('kategori-artikel');
    Route::get('/tambah-kategori-artikel', [App\Http\Controllers\LandingPage\KategoriArtikelController::class, 'formAdd'])->name('tambahkategori-artikel');
    Route::post('/postkategori-artikel', [App\Http\Controllers\LandingPage\KategoriArtikelController::class, 'post'])->name('data_kategori-artikel');
    Route::post('/update-kategori-artikel/{id}', [App\Http\Controllers\LandingPage\KategoriArtikelController::class, 'update'])->name('update_kategori-artikel');
    Route::get('/edit-kategori-artikel/{id}', [App\Http\Controllers\LandingPage\KategoriArtikelController::class, 'getDetail']);
    Route::get('/delete-kategori-artikel/{id}', [App\Http\Controllers\LandingPage\KategoriArtikelController::class, 'delete']);

    Route::get('/klien-kami', [App\Http\Controllers\LandingPage\KlienKamiController::class, 'getList'])->name('klien-kami');
    Route::get('/tambah-klien-kami', [App\Http\Controllers\LandingPage\KlienKamiController::class, 'formAdd'])->name('tambah-klien-kami');
    Route::post('/post-klien-kami', [App\Http\Controllers\LandingPage\KlienKamiController::class, 'post'])->name('data-klien-kami');
    Route::post('/update-klien-kami/{id}', [App\Http\Controllers\LandingPage\KlienKamiController::class, 'update'])->name('update-klien-kami');
    Route::get('/edit-klien-kami/{id}', [App\Http\Controllers\LandingPage\KlienKamiController::class, 'getDetail']);
    Route::get('/delete-klien-kami/{id}', [App\Http\Controllers\LandingPage\KlienKamiController::class, 'delete']);
    
    Route::get('/faq', [App\Http\Controllers\LandingPage\HomeController::class, 'faq'])->name('faq');
    Route::get('/tambah-faq', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_faq'])->name('tambah_data_faq');
    Route::get('/edit-faq/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'edit_faq']);
    Route::post('/postfaq', [App\Http\Controllers\LandingPage\HomeController::class, 'postfaq'])->name('data_faq');
    Route::get('/delete-faq/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete_faq']);
    Route::post('/update-faq/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'update_faq']);

    Route::get('/klien', [App\Http\Controllers\LandingPage\HomeController::class, 'klien'])->name('klien');
    Route::get('/tambah-klien', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_klien'])->name('tambahklien');
    Route::post('/postklien', [App\Http\Controllers\LandingPage\HomeController::class, 'postklien'])->name('data_klien');
    Route::get('/delete-klien/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete']);

    Route::get('/testimoni', [App\Http\Controllers\LandingPage\HomeController::class, 'testimoni'])->name('testimoni');
    Route::get('/tambah-testimoni', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_testimoni'])->name('tambahtestimoni');
    Route::post('/posttestimoni', [App\Http\Controllers\LandingPage\HomeController::class, 'posttestimoni'])->name('data_testimoni');
    Route::get('/delete-testimoni/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete_testi']);
    Route::get('/edit-testimoni/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'edit_testi']);
    Route::put('/update-testimoni/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'update_testi']);

    Route::get('/detailklien', [App\Http\Controllers\DashboardController::class, 'detailklien'])->name('detailklien');

    Route::get('/pencapaian', [App\Http\Controllers\LandingPage\HomeController::class, 'pencapaian'])->name('pencapaian');
    Route::get('/tambah-pencapaian', [App\Http\Controllers\LandingPage\HomeController::class, 'tambah_pencapaian'])->name('tambahpencapaian');
    Route::post('/postpencapaian', [App\Http\Controllers\LandingPage\HomeController::class, 'postpencapaian'])->name('data_pencapaian');
    Route::get('/delete-pencapaian/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'delete_pencapaian']);
    Route::get('/edit-pencapaian/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'edit_pencapaian']);
    Route::put('/update-pencapaian/{id}', [App\Http\Controllers\LandingPage\HomeController::class, 'update_pencapaian']);
});
