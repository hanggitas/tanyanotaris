<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogVerifikasi extends Model
{
    use HasFactory;
    protected $table = 'log_verifikasi';
    protected $fillable = [
        'log',
        'created_at',
        'updated_at'
    ];
}
