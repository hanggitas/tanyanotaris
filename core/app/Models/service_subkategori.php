<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class service_subkategori extends Model
{
    use HasFactory;
    protected $table = 'service_subkategori';
    // protected $fillable = [
    //     'jenis_paket',
    //     'detail_paket',
    //     'harga',
    //         'created_at',
    //         'updated_at',
    //         'id_sub_kategori',
    // ];
    protected $fillable = [
        'id_subkategori',
        'id_services',
        'id_kategori',
        'created_at',
        'updated_at'
    ];

    public function select_kategori()
    {
        return $this->hasMany(portofolio::class);
    }
}
