<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{
    use HasFactory;
    protected $table = 'landing_page';
    protected $fillable = [
        'title',
        'nama',
        'jabatan',
        'caption',
        'no_telp',
        'hari',
        'jam',
        'img',
        'link_facebook',
        'link_twitter',
        'link_instagram',
        'section',
        'created_at',
        'updated_at',
    ];
}
