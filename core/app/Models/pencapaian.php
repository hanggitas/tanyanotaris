<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pencapaian extends Model
{
    use HasFactory;
    protected $table = 'pencapaian';
    protected $fillable = [
        'klien_senang',
        'kasus',
        'kasus_menang',
        'penghargaan',
        'created_at',
        'updated_at'
    ];
}
