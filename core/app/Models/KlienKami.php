<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KlienKami extends Model
{
    use HasFactory;
    protected $table = 'klien_kami';
    protected $fillable = [
        'nama',
        'logo',
        'created_at',
        'updated_at',
    ];
}
