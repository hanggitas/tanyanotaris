<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class portofolio extends Model
{
    use HasFactory;
    protected $table = 'portofolio';
    protected $fillable = [
        'id_users',
        'id_kategori',
        'id_preview',
        'projek',
        'created_at',
        'updated_at',
    ];

    public function kategori()
    {
        return $this->belongsTo(kategori::class);
    }
}
