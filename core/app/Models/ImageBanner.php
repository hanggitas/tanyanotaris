<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageBanner extends Model
{
    use HasFactory;
    protected $table = 'image_banner';
    protected $fillable = [
        'nama',
        'img',
        'created_at',
        'updated_at'
    ];
}
