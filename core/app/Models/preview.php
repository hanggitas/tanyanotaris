<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class preview extends Model
{
    use HasFactory;
    protected $table = 'preview';
    protected $fillable = [
        'id_order',
        'link_finish_project',
        'created_at',
        'updated_at',
    ];
}
