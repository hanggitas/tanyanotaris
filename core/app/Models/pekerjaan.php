<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pekerjaan extends Model
{
    use HasFactory;
    protected $table = 'pekerjaan';
    protected $fillable = [
        'id_order',
        'pre_produksi',
        'produksi',
        'post_produksi',
        'link_project',
        'size_project',
        'diunggah',
        'selesai',
        'created_at',
        'updated_at',
    ];
}
