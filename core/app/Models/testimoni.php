<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class testimoni extends Model
{
    use HasFactory;
    protected $table = 'testimoni';
    protected $fillable = [
        'id_users',
        'review',
        'created_at',
        'updated_at'
    ];
}
