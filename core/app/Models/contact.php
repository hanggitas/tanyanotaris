<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    use HasFactory;
    protected $table = 'contact';
    protected $fillable = [
        'email',
        'nomor_telepon',
        'nomor_konsultasi',
        'alamat',
        'logo',
        'instagram',
        'twitter',
        'linkedin',
        'facebook',
        'created_at',
        'updated_at'
    ];
}
