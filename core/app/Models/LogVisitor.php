<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogVisitor extends Model
{
    use HasFactory;
    protected $table = 'log_visitor';
    protected $fillable = [
        'kota',
        'negara',
        'ip',
        'created_at',
        'updated_at'
    ];
}
