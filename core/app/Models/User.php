<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class User extends Authenticatable implements MustVerifyEmail
{

    use HasFactory;
    use Notifiable;
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
        'foto',
        'logo',
        'email_verified_at',
        'no_hp_user',
        'alamat_user',
        'remember_token',
        'created_at',
        'updated_at',
    ];
}


// class user extends Model
// {

// }
