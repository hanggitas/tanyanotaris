<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;
    protected $table = 'order';
    protected $fillable = [
        'id_users',
        'id_metode_pembayaran',
        'id_service_subkategori',
        'bukti_bayar',
        'status_pembayaran',
        'date_time',
        'total_harga',
        'created_at',
        'updated_at'
    ];
}
