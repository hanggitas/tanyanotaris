<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class banner extends Model
{
    use HasFactory;
    protected $table = 'banner';
    protected $fillable = [
        'nama_banner',
        'gambar',
        'judul_caption',
        'image_banner_id',
        'btn_caption',
        'caption',
        'status',
        'url',
        'created_at',
        'updated_at'
    ];
}
