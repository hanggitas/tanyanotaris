<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\portofolio;
use App\Models\User;
use App\Models\service_subkategori;
use App\Models\users;
use App\Models\kategori;
use App\Models\preview;
use Illuminate\Http\Request;
use Auth;

class PortofolioController extends Controller
{
    
    public function portofolio()
    {
        if(Auth::user()->role == 'admin')
        {
            $left_join = portofolio::select('portofolio.*', 'users.name')
            ->leftjoin('users', 'portofolio.id_users', '=', 'users.id')
            ->orderBy('portofolio.id','DESC')
            ->paginate(5);
 
            return view('main.portofolio', compact('left_join'));
        }
    }

    public function tambah_data_portofolio()
    {
        if(Auth::user()->role == 'admin')
        {
            $pilihan_kategori = kategori::all();
            $pilihan_user = User::all();

            return view('input_form.tambah_data_portofolio', compact('pilihan_kategori', 'pilihan_user'));
        }
    }

    public function postportofolio(Request $request)
    {
        if(Auth::user()->role == 'admin')
        {
            $this->validate($request, [
                'id_users' => 'required',
                'projek' => 'required'
            ]);

            $data = $request->all();
            $post = [
                'id_users' => $data['id_users'],
                'projek' => $data['projek']
            ];
            $buat = portofolio::create($post);
            if ($request->hasFile('projek')) {
                $request->file('projek')->move('public/app-assets/images/docs/', $request->file('projek')->getClientOriginalName());
                $buat->projek = $request->file('projek')->getClientOriginalName();
                $buat->save();
            }

            return redirect('/portofolio')->with([ 'success' => 'Data Berhasil Ditambahkan!' ]);
        }
    }

    public function edit(Request $request, $id)
    {
        $portofolio = portofolio::find($id);
        $pilihan_kategori = kategori::all();
        $pilihan_user = User::all();
        // $pilihan_link = preview::all();

        //dd($pilihan_user);
        return view('input_form.ubah_data_portofolio', compact('portofolio', 'pilihan_kategori', 'pilihan_user'));
    }

    public function update(Request $request, $id)
    {


        $portofolio = portofolio::find($request->route('id'));
        //dd($portofolio);
        $portofolio->id_users = $request->input('id_users');
        $portofolio->id_kategori = $request->input('id_kategori');
        // $portofolio->id_preview = $request->input('id_preview');
        if ($request->hasFile('projek')) {
            $file = $request->file('projek');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/docs/', $filename);
            $portofolio->projek = $filename;
        }
        $portofolio->update();
        //dd($portofolio);

        // if ($portofolio) {
        //     //redirect dengan pesan sukses
        //     return redirect()->route('portofolio')->with(['success' => 'Data Berhasil Diupdate!']);
        // } else {
        //     //redirect dengan pesan error
        //     return redirect()->route('portofolio')->with(['error' => 'Data Gagal Diupdate!']);
        // }

        return redirect('/portofolio');
    }

    public function delete($id)
    {
        $portofolio = portofolio::find($id);
        $portofolio->delete();
        return redirect()->route('portofolio')->with('status', 'Data Berhasil Dihapus!');
    }
}
