<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\order;
use App\Models\kategori;
use App\Models\metode_pembayaran;
use App\Models\service_subkategori;
use App\Models\services;
use App\Models\subkategori;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function filterBy($data, $request)
    {
        if($request->input('id'))
        {
            $data->where('order.id','LIKE','%'.$request->input('id').'%');
        }
        if($request->input('email'))
        {
            $data->where('users.email','LIKE','%'.$request->input('email').'%');
        }
        if($request->input('jenis_paket'))
        {
            $data->where('services.jenis_paket','LIKE','%'.$request->input('jenis_paket').'%');
        }
        if($request->input('status_pembayaran'))
        {
            $data->where('order.status_pembayaran','LIKE','%'.$request->input('status_pembayaran').'%');
        }

        return $data;
    }

    public function order(Request $request)
    {

        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $pilihan_kategori = kategori::all();

        $pilihan_subkategori = subkategori::all();

        $orders = order::select('order.*', 'metode_pembayaran.metode_pembayaran', 'users.email', 'services.jenis_paket')
            ->leftjoin('metode_pembayaran', 'order.id_metode_pembayaran', '=', 'metode_pembayaran.id')
            ->leftjoin('users', 'order.id_users', '=', 'users.id')
            ->leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
            ->leftJoin('services','service_subkategori.id_services','=','services.id')
            ->orderBy('order.id', 'DESC');
        $orders = $this->filterBy($orders, $request)->paginate(10);
        $old_value = $request->all();
        return view('main.order', compact('orders', 'pilihan_kategori', 'pilihan_subkategori','old_value'));
    }

    public function formorder()
    {
        $pilihan_kategori = kategori::all();
        return view('input_form.tambah_data_order', compact('pilihan_kategori'));
    }

    public function postorder(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data = $request->all();
        //dd($data);
        $post = [
            'id_users' => $data['id_users'],
            'id_metode_pembayaran' => $data['id_metode_pembayaran'],
            'id_service_subkategori' => $data['id_service_subkategori'],
            'bukti_bayar_dp' => $data['bukti_bayar_dp'],
            'bukti_bayar_lunas' => $data['bukti_bayar_lunas'],
            'status_pembayaran' => $data['status_pembayaran'],
            'date_time' => $data['date_time'],
            'total_harga' => $data['total_harga'],
            'total_dibayar' => $data['total_dibayar'],
            'sisa_bayar' => $data['sisa_bayar']
        ];

        order::create($post);
        return redirect()->back();
    }

    public function postcustom(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }

        $data = $request->all();
        //dd($data);
        $post = [
            'jenis_paket' => $data['jenis_paket'],
            'detail_paket' => $data['detail_paket'],
            'harga' => $data['harga']
        ];
        $postServis = services::create($post);

        $postKategori = [
            'id_services' => $postServis->id,
            'id_kategori' => $data['id_kategori'],
            'id_subkategori' => $data['id_subkategori']
        ];
        $postSersub = service_subkategori::create($postKategori);

        $apaaja = order::where('id', $request->input('id_order'))->update([
            'id_service_subkategori' => $postSersub->id,
            'total_harga' => $data['harga'],
            'total_bayar_dp' => $data['harga'] / 2
        ]);
        return redirect('/order');
    }

    public function edit(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $order = order::find($request->route('id'));
        //dd($order);
        $pilihan_medpem = metode_pembayaran::get();
        $pilihan_user = User::get();
        $orders = order::leftjoin('users', 'order.id_users', '=', 'users.id')
            ->leftjoin('metode_pembayaran', 'order.id_metode_pembayaran', '=', 'metode_pembayaran.id')
            ->get(['order.*', 'users.name', 'metode_pembayaran.metode_pembayaran']);
        return view('edit.edit_order', compact('order', 'orders', 'pilihan_medpem', 'pilihan_user'));
    }

    public function updateorder(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $update = order::find($request->route('id'));
        $update->status_pembayaran = $request->input('status_pembayaran');
        $update->update();
        //dd($update);
        return redirect('/order')->with('status', 'Data Berhasil Diupdate!');
        //return $request;
    }

    public function delete(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $order = order::find($request->route('id'));
        $order->delete();
        return redirect()->route('order')->with('status', 'Data Berhasil Dihapus!');
    }
}
