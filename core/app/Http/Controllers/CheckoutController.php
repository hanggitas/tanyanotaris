<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\kategori;
use App\Models\subkategori;
use App\Models\services;
use App\Models\ServiceSubkategori;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
     public function __construct()
     {
          $this->middleware('auth');
     }
     public function checkout(Request $request)
     {
          $kategori = kategori::get();
          foreach ($kategori as $item) {
               $item->subkategori = subkategori::where('id_kategori', $item->id)->get();
          }

          return view('main.checkout', compact('kategori'));
     }

     public function getDetail(Request $request)
     {
          $detail = ServiceSubkategori::where('id_subkategori', $request->route('id'))
               ->leftJoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
               ->leftJoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->leftJoin('kategori', 'subkategori.id_kategori', '=', 'kategori.id')
               ->orderBy('subkategori.id', 'ASC')
               ->get();

          $subkategori = Subkategori::where('subkategori.id', $request->route('id'))
               ->leftJoin('kategori', 'subkategori.id_kategori', '=', 'kategori.id')
               ->first();

          return view('main.detailklien', compact('detail', 'subkategori'));
     }
}
