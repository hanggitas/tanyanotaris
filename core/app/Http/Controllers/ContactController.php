<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\contact;
use Illuminate\Http\Request;
use Auth;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function kontak()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data_kontak = contact::paginate(5);
        return view('main.contact', compact('data_kontak'));
    }

    public function formkontak()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_contact');
    }

    public function postkontak(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'email' => 'required',
            'nomor_telepon' => 'required',
            'alamat' => 'required',
            'logo' => 'required',
            'instagram' => 'required',
            'youtube' => 'required',
            'linkedin' => 'required',
        ]);
        $data = $request->all();
        //dd($data);
        $post = [
            'email' => $data['email'],
            'nomor_telepon' => $data['nomor_telepon'],
            'alamat' => $data['alamat'],
            'logo' => $data['logo'],
            'instagram' => $data['instagram'],
            'youtube' => $data['youtube'],
            'linkedin' => $data['linkedin']
        ];
        //dd($data);
        $buat = contact::create($post);
        if ($request->hasFile('logo')) {
            $request->file('logo')->move('public/app-assets/images/logo/', $request->file('logo')->getClientOriginalName());
            $buat->logo = $request->file('logo')->getClientOriginalName();
            $buat->save();
        }
        return redirect('/kontak'); //->back();
    }

    public function editkontak(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data_kon = contact::find($request->route('id'));
        return view('edit.edit_kontak', compact('data_kon'));
    }

    public function updatekontak(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $update_data_kon = contact::find($request->route('id'));
        $update_data_kon->email = $request->input('email');
        $update_data_kon->nomor_telepon = $request->input('nomor_telepon');
        $update_data_kon->nomor_konsultasi = $request->input('nomor_konsultasi');
        $update_data_kon->alamat = $request->input('alamat');
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/logo/', $filename);
            $update_data_kon->logo = $filename;
        }
        $update_data_kon->facebook = $request->input('facebook');
        $update_data_kon->twitter = $request->input('twitter');
        $update_data_kon->instagram = $request->input('instagram');
        $update_data_kon->youtube = $request->input('youtube');
        $update_data_kon->linkedin = $request->input('linkedin');
        $update_data_kon->update();
        //dd($update_data_kon);
        return redirect('/kontak')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function deletekontak(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $deletekontak = contact::find($request->route('id'));
        $deletekontak->delete();

        return redirect('/kontak');
    }
}
