<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\preview;
use Illuminate\Http\Request;


class PreviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function preview()
    {
        // $data_preview = preview::all();
        $data_preview = preview::paginate(5);
        //dd($data_preview);
        return view('main.preview', ['data_preview' => $data_preview]);
    }

    public function formpreview()
    {
        return view('input_form.tambah_data_preview');
    }

    public function postpreview(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $post = [
            'id_order' => $data['id_order'],
            'link_finish_project' => $data['link_finish_project'],
        ];
        preview::create($post);
        return redirect('/preview'); //->back();
    }

    public function editpreview(Request $request)
    {
        $data_preview = preview::find($request->route('id'));
        //dd($request->route('id'));
        return view('edit.edit_preview', ['data_preview' => $data_preview]);
    }

    public function updatepreview(Request $request)
    {
        $updatepreview = preview::find($request->route('id'));
        //Cara 1
        //$updatepreview->id_order = $request->id_order;
        //$updatepreview->link_preview = $request->link_preview;
        //$updatepreview->link_finish_project = $request->link_finish_project;
        //dd($request);
        //cara 2
        $updatepreview->update($request->all());
        //$updatepreview->save();
        return redirect('/preview');
        //->update(['deskripsi' => $request->deskripsi,]);
        //task::where('id', $request->route('id'))->update(['deskripsi' => $request->deskripsi,]);
        //dd($request->route('id'));
        //dd($updatetask);
        //$data_task = task::find($request->route('id'));
        //$data_task->deskripsi = $request->deskripsi;
        //$data_task->save();
        //return redirect('task');

        //return $request;
    }

    public function deletepreview(Request $request)
    {
        $deletepreview = preview::find($request->route('id'));
        $deletepreview->delete();

        return redirect('/preview');
    }
}
