<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function filterBy($data, $request)
    {
        if($request->input('email'))
        {
            $data->where('email','LIKE','%'.$request->input('email').'%');
        }
        if($request->input('nama'))
        {
            $data->where('name','LIKE','%'.$request->input('nama').'%');
        }
        if($request->input('verifikasi_email') == 1)
        {
            $data->whereNotNull('email_verified_at');
        }
        return $data;
    }

    public function user(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data = user::orderBy('id','DESC');
        $userss = $this->filterBy($data, $request)->paginate(10);
        $old_value = $request->all();
        

        return view('main.user', compact('userss','old_value'));
    }

    public function postuser(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data = $request->all();
        $post = [
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => $data['password'],
            'foto' => $data['foto'],
            'no_hp_user' => $data['no_hp_user'],
            'alamat_user' => $data['alamat_user']
        ];
        user::create($post);
        return redirect()->route('user');
    }

    public function edit($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }

        $user = user::find($id);
        if($user->email_verified_at != null)
        {
            $user->status_user = 1;
        }
        else
        {
            $user->status_user = 0;
        }
        return view('input_form.ubah_data_user', compact('user'));
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }

        $users = user::find($id);
        $users->name = $request->input('name');
        if($request->input('password'))
        {
            $users->password = Hash::make($request->input('password'));
        }
        $users->no_hp_user = $request->input('no_hp_user');
        $users->alamat_user = $request->input('alamat_user');
        if($request->input('verifikasi_email') == 1)
        {
            $users->email_verified_at = date('Y-m-d');
        }
        else
        {
            $users->email_verified_at = null;
        }

        $users->update();

        if ($users) {
            //redirect dengan pesan sukses
            return redirect()->route('user')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('user')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $users = User::find($id);
        $users->delete();
        return redirect()->route('user')->with('status', 'Data Berhasil Dihapus!');
    }
}
