<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function faq()
    {
        $faqs = faq::orderBy('id','DESC')->paginate(10);
        return view('main.faq', compact('faqs'));
    }

    public function tambah_data_faq()
    {
        return view('input_form.tambah_data_faq');
    }

    public function postfaq(Request $request)
    {
        $this->validate($request, [
            'pertanyaan' => 'required',
            'jawaban' => 'required'
        ]);

        $data = $request->all();
        $post = [
            'pertanyaan' => $data['pertanyaan'],
            'jawaban' => $data['jawaban']
        ];
        faq::create($post);
        return redirect()->route('faq')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    public function edit($id)
    {
        $faq = faq::find($id);
        return view('input_form.ubah_data_faq', compact('faq'));
    }

    public function update(Request $request, $id)
    {
        $faq = faq::find($id);
        $faq->pertanyaan = $request->input('pertanyaan');
        $faq->jawaban = $request->input('jawaban');
        $faq->update();

        if ($faq) {
            //redirect dengan pesan sukses
            return redirect()->route('faq')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('faq')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        $faq = faq::find($id);
        $faq->delete();
        return redirect()->route('faq')->with('success', 'Data Berhasil Dihapus!');
    }
}
