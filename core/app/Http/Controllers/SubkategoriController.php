<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\kategori;
use App\Models\subkategori;
use Illuminate\Http\Request;

class SubkategoriController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function subkategori()
    {

        $left_join = subkategori::leftjoin('kategori', 'subkategori.id_kategori', '=', 'kategori.id')
            ->select(['subkategori.*', 'kategori.nama_kategori'])
            ->paginate(5);

        return view('main.subkategori', compact('left_join'));
    }

    public function tambah_data_subkategori()
    {
        $pilihan_kategori = kategori::all();
        return view('input_form.tambah_data_subkategori', compact('pilihan_kategori'));
    }
    public function postsubkategori(Request $request)
    {
        $this->validate($request, [
            'id_kategori' => 'required',
            'nama_subkategori' => 'required',
            'foto' => 'required'
        ]);
        // $data = $request->all();
        // $post = [
        //     'id_kategori' => $data['id_kategori'],
        //     'nama_subkategori' => $data['nama_subkategori'],
        //     'foto' => $data['foto']
        // ];
        // $buat = subkategori::create($post);
        // if ($request->hasFile('foto')) {
        //     $request->file('foto')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto')->getClientOriginalName());
        //     $buat->foto = $request->file('foto')->getClientOriginalName();
        //     $buat->save();
        // }
        $data = subkategori::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('subkategori');
    }

    public function edit($id)
    {
        $subkategori = subkategori::find($id);
        $pilihan_kategori = kategori::all();
        return view('input_form.ubah_data_subkategori', compact('subkategori', 'pilihan_kategori'));
    }

    public function update(Request $request, $id)
    {


        $subkategori = subkategori::find($id);
        $subkategori->id_kategori = $request->input('id_kategori');
        $subkategori->nama_subkategori = $request->input('nama_subkategori');
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/pages/eCommerce/', $filename);
            $subkategori->foto = $filename;
        }
        //$subkategori->foto = $request->input('foto');
        //dd($subkategori);
        $subkategori->update();
        //dd($subkategori);
        return redirect('/subkategori');

        if ($subkategori) {
            //redirect dengan pesan sukses
            return redirect()->route('subkategori')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('subkategori')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        $subkategori = subkategori::find($id);
        $subkategori->delete();
        return redirect()->route('subkategori')->with('status', 'Data Berhasil Dihapus!');
    }
}
