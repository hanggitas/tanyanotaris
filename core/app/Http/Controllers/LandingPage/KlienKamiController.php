<?php

namespace App\Http\Controllers\LandingPage;

use App\Http\Controllers\Controller;
use App\Models\KlienKami;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class KlienKamiController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function filterBy($data, $request)
    {
        if($request->input('nama'))
        {
            $data->where('nama','LIKE','%'.$request->input('nama').'%');
        }
        return $data;
    }

    public function getList(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $klien_kami = KlienKami::orderBy('id','DESC');
        $klien_kami = $this->filterBy($klien_kami, $request)->paginate(10);
        return view('main.klien-kami', compact('klien_kami'));
    }

    public function formAdd()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_klien_kami');
    }

    public function post(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
            'logo' => 'mimes:jpg,jpeg,png|max:10000',
         ]);
 
         if($validator->fails())
         {
             return redirect()->back()->with(['error' => $validator->messages()->first()]);
         }
 
         $data = KlienKami::create([
            'nama' => $request->input('nama')
        ]);
        if ($request->hasFile('logo')) {
            $request->file('logo')->move('public/app-assets/images/pages/eCommerce/', $request->file('logo')->getClientOriginalName());
            $data->logo = $request->file('logo')->getClientOriginalName();
            $data->save();
        }
        if($data != null)
        {
            return redirect()->route('klien-kami')->with(['success' => 'Data Berhasil Ditambahkan!']);
        }
        else
        {
            return redirect()->route('klien-kami')->with(['error' => 'Data Gagal Ditambahkan!']);
        }
    }

    public function getDetail($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $klien_kami = KlienKami::find($id);
        return view('input_form.ubah_data_klien_kami', compact('klien_kami'));
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }

        $klien_kami = KlienKami::find($id);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $extension = strtolower($extension);
            if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg')
            {
                $filename = time() . '.' . $extension;
                $file->move('public/app-assets/images/pages/eCommerce/', $filename);
                $klien_kami->logo = $filename;
                $klien_kami->save();
            }
        }
        $klien_kami->update([
            'nama' => $request->input('nama')
        ]);

        if ($klien_kami) {
            //redirect dengan pesan sukses
            return redirect()->route('klien-kami')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('klien-kami')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $faq = KlienKami::find($id);
        $faq->delete();
        return redirect()->route('klien-kami')->with('status', 'Data Berhasil Dihapus!');
    }
}
