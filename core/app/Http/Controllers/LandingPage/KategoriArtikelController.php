<?php

namespace App\Http\Controllers\LandingPage;

use App\Http\Controllers\Controller;
use App\Models\KategoriArtikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class KategoriArtikelController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getList()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori_artikel = KategoriArtikel::paginate(10);
        return view('main.kategori-artikel', compact('kategori_artikel'));
    }

    public function formAdd()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_kategori_artikel');
    }

    public function post(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
         ]);
 
         if($validator->fails())
         {
             return redirect()->back()->with(['error' => $validator->messages()->first()]);
         }
        $data = KategoriArtikel::create($request->all());
        if($data != null)
        {
            return redirect()->route('kategori-artikel')->with(['success' => 'Data Berhasil Ditambahkan!']);
        }
        else
        {
            return redirect()->route('kategori-artikel')->with(['error' => 'Data Gagal Ditambahkan!']);
        }
    }

    public function getDetail($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori_artikel = KategoriArtikel::find($id);
        return view('input_form.ubah_data_kategori_artikel', compact('kategori_artikel'));
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
         ]);
 
         if($validator->fails())
         {
             return redirect()->back()->with(['error' => $validator->messages()->first()]);
         }

        $faq = KategoriArtikel::find($id);
        $faq->nama = $request->input('nama');
        $faq->update();
        if ($faq) {
            //redirect dengan pesan sukses
            return redirect()->route('kategori-artikel')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('kategori-artikel')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $faq = KategoriArtikel::find($id);
        $faq->delete();
        return redirect()->route('kategori-artikel')->with('status', 'Data Berhasil Dihapus!');
    }
}
