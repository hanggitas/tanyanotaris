<?php

namespace App\Http\Controllers\LandingPage;

use App\Http\Controllers\Controller;
use App\Models\contact;
use App\Models\banner;
use App\Models\faq;
use App\Models\testimoni;
use App\Models\artikel;
use App\Models\kategori;
use App\Models\KategoriArtikel;
use App\Models\services;
use App\Models\pencapaian;
use App\Models\User;
use App\Models\LogAktifitas;
use Illuminate\Http\Request;
use App\Models\ImageBanner;
use App\Models\KlienKami;
use App\Models\LogVisitor;
use Auth;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        $slide = banner::whereNotNull('banner.id')
        ->select('banner.*','image_banner.img as gambar')
        ->leftJoin('image_banner','banner.image_banner_id','=','image_banner.id')
        ->get();
        $testi = testimoni::leftjoin('users', 'testimoni.id_users', '=', 'users.id')
            ->select('testimoni.*', 'users.name')->get();
        $new = artikel::orderBy('id', 'DESC')->take(3)->get();
        $capai = pencapaian::all();
        $kontak = contact::all();
        $logo = KlienKami::select('logo as foto')->get();
        $penawaran = kategori::take(3)->orderBy('id','DESC')->get();
        $visitor = LogVisitor::get();

        return view('landing-page.home', [
            'slide' => $slide,
            'testi' => $testi,
            'new' => $new,
            'penawaran' => $penawaran,
            'capai' => $capai,
            'kontak' => $kontak,
            'logo' => $logo,
            'visitor' => count($visitor),
        ]);
    }

    public function mengapa()
    {
        $kontak = contact::all();
        return view('landing-page.mengapa', compact('kontak'));
    }

    public function banner(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $slide = banner::select('banner.*','image_banner.img as gambar')
        ->leftJoin('image_banner','banner.image_banner_id','=','image_banner.id')->orderBy('id','DESC');
        $slide = $this->filterBy($slide,$request)->paginate(10);
        $old_value = $request->all();
        return view('main.banner', compact('slide','old_value'));
    }

    public function image_banner(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $image_banner = ImageBanner::orderBy('id','DESC');
        $image_banner = $this->filterBy($image_banner,$request)->paginate(10);
        $old_value = $request->all();
        return view('main.image-banner', compact('image_banner','old_value'));
    }

    public function tambah_banner()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $image_banner = ImageBanner::get();
        return view('input_form.tambah_data_banner', compact('image_banner'));
    }

    public function tambah_image_banner()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_image_banner');
    }

    public function faq()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $faqs = faq::orderBy('id','DESC')->paginate(10);
        return view('main.faq', compact('faqs'));
    }

    public function tambah_faq(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_faq');
    }

    public function edit_faq($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $faqs = faq::find($id);
        return view('input_form.ubah_data_faq', compact('faqs'));
    }

    public function postfaq(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'pertanyaan' => 'required',
            'jawaban' => 'required'
        ]);
        faq::create($request->all());
        return redirect()->route('faq')->with('success', 'Data berhasil disimpan');
    }

    public function delete_faq(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $faqs = faq::find($request->route('id'));
        $faqs->delete();

        return redirect('/faq')->with('success', 'Data berhasil dihapus');
    }

    public function postbanner(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'image_banner_id' => 'required'
        ]);

        $data = banner::create($request->all());
        return redirect()->route('banner')->with('success', 'Data berhasil disimpan');
    }
    public function postimagebanner(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'nama' => 'required',
            'img' => 'required | mimes:png,jpg,jpeg,gif,svg,bmp'
        ]);

        $data = ImageBanner::create($request->all());
        if ($request->hasFile('img')) {
            $request->file('img')->move('public/aset/images/', $request->file('img')->getClientOriginalName());
            $data->img = $request->file('img')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('image-banner')->with('success', 'Data berhasil disimpan');
    }


    public function delete(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $slide = banner::find($request->route('id'));
        $slide->delete();

        return redirect('/banner')->with('success', 'Data Berhasil Dihapus');
    }

    public function delete_image_banner(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $slide = ImageBanner::find($request->route('id'));
        $slide->delete();

        return redirect('/image-banner')->with('success', 'Data Berhasil Dihapus');
    }

    public function edit_banner($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $s = banner::find($id);
        $image_banner = ImageBanner::get();
        return view('input_form.ubah_data_banner', compact('s','image_banner'));
    }

    public function updatebanner(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $get_img_banner = ImageBanner::where('nama', 'LIKE', '%'.$request->input('nama_banner').'%')->first();
        if(!$get_img_banner)
        {
            return redirect()->route('banner')->with(['error' => 'Banner tidak ditemukan!']);
        }
        else
        {
            $s = banner::find($id);
            $s->id = $id;
            $s->image_banner_id = $get_img_banner->id;
            $s->status = $request->input('status');
            $s->judul_caption = $request->input('judul_caption');
            $s->caption = $request->input('caption');
            $s->btn_caption = $request->input('btn_caption');
            $s->url = $request->input('url');
            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('public/aset/images/', $filename);
                $s->gambar = $filename;
            }
            $s->update();
            if ($s) {
                //redirect dengan pesan sukses
                return redirect()->route('banner')->with(['success' => 'Data Berhasil Diupdate!']);
            } else {
                //redirect dengan pesan error
                return redirect()->route('banner')->with(['error' => 'Data Gagal Diupdate!']);
            }
        }
    }
    public function updateimagebanner(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $s = banner::find($id);
        $s->id = $id;
        $s->status = $request->input('status');
        $s->judul_caption = $request->input('judul_caption');
        $s->caption = $request->input('caption');
        $s->btn_caption = $request->input('btn_caption');
        $s->image_banner_id = $request->input('image_banner_id');
        //$subkategori->foto = $request->input('foto');
        //dd($subkategori);
        $s->update();

        if ($s) {
            //redirect dengan pesan sukses
            return redirect()->route('banner')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('banner')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function filterBy($data, $request)
    {
        if($request->input('id_kategori'))
        {
            $data->where('id_kategori','LIKE','%'.$request->input('id_kategori').'%');
        }
        if($request->input('judul'))
        {
            $data->where('judul','LIKE','%'.$request->input('judul').'%');
        }
        if($request->input('judul_caption'))
        {
            $data->where('judul_caption','LIKE','%'.$request->input('judul_caption').'%');
        }

        return $data;
    }

    public function artikel(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $artikels = artikel::select('artikel.*','kategori_artikel.nama as nama_kategori')
        ->leftJoin('kategori_artikel','artikel.id_kategori','=','kategori_artikel.id')
        ->orderBy('artikel.id', 'DESC');
        $artikels = $this->filterBy($artikels, $request)->paginate(10);
        $kategori_all = KategoriArtikel::select('id','nama as nama_kategori')->get();
        $old_value = $request->all();
        return view('main.artikel', compact('artikels','old_value', 'kategori_all'));

    }

    public function tambah_artikel(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori = KategoriArtikel::select('id','nama as nama_kategori')->get();
        return view('input_form.tambah_data_artikel',compact('kategori'));
    }

    public function edit_artikel($id)
    {
        try {
            //code...
            if(Auth::user()->role != 'admin')
            {
                return redirect()->route('dashboard');
            }
            $artikels = artikel::find($id);
            $kategori = KategoriArtikel::where('id',$artikels->id_kategori)->first();
    
            if(empty($kategori))
            {
                $kategori = [];
                $kategori['id_kategori'] = null;
                $kategori = json_decode(json_encode($kategori), FALSE);
            }
    
            $kategori_all = KategoriArtikel::select('id','nama as nama_kategori')->get();
            return view('input_form.ubah_data_artikel', compact('artikels','kategori','kategori_all'));
        } catch (Exception $e) {
            //throw $th; $e->getMessage();
            $create_log = LogAktifitas::create([
                'log' => $e->getMessage(),
            ]);
            return redirect()->route('artikel')->with('error', $e->getMessage());
        }
       
    }

    public function postartikel(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'id_kategori' => 'required',
            'judul' => 'required',
            'gambar' => 'required|mimes:png,jpg,jpeg,gif,svg,bmp|max:5000',
            'deskripsi' => 'required'
        ]);
        $temp_data = $request->all();
        $temp_data['deskripsi'] = str_replace("<ul>","",$temp_data['deskripsi']);
        $temp_data['deskripsi'] = str_replace("</ul>","",$temp_data['deskripsi']);
        $data = artikel::create($temp_data);
        if ($request->hasFile('gambar')) {
            $request->file('gambar')->move('public/aset/images/blog', $request->file('gambar')->getClientOriginalName());
            $data->gambar = $request->file('gambar')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('artikel')->with('success', 'Data berhasil disimpan');
    }

    public function updateartikel(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $artikels = artikel::find($id);
        $artikels->id_kategori = $request->input('id_kategori');
        $artikels->judul = $request->input('judul');
        $temp = str_replace("?","",$request->input('deskripsi'));
        $artikels->deskripsi = $temp;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/aset/images/blog', $filename);
            $artikels->gambar = $filename;
        }
        //$subkategori->foto = $request->input('foto');
        //dd($subkategori);
        $artikels->update();

        if ($artikels) {
            //redirect dengan pesan sukses
            return redirect()->route('artikel')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('artikel')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete_artikel(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $artikels = artikel::find($request->route('id'));
        $artikels->delete();

        return redirect('/artikel')->with(['success' => 'Data Berhasil Dihapus!']);
    }

    public function update_faq(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $faqs = faq::find($id);
        $faqs->pertanyaan = $request->input('pertanyaan');
        $faqs->jawaban = $request->input('jawaban');

        //$subkategori->foto = $request->input('foto');
        //dd($subkategori);
        $faqs->update();

        if ($faqs) {
            //redirect dengan pesan sukses
            return redirect()->route('faq')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('faq')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    // public function postfaq(Request $request)
    // {
    //     $this->validate($request, [
    //         'nama_artikel' => 'required',
    //         'gambar_artikel' => 'required | mimes:png,jpg,jpeg,gif,svg,bmp',
    //         'deskripsi' => 'required'
    //     ]);
    //     $data = artikel::create($request->all());
    //     if ($request->hasFile('gambar_artikel')) {
    //         $request->file('gambar_artikel')->move('public/aset/images/artikel', $request->file('gambar')->getClientOriginalName());
    //         $data->gambar = $request->file('gambar_artikel')->getClientOriginalName();
    //         $data->save();
    //     }
    //     return redirect()->route('artikel')->with('success', 'Data berhasil disimpan');
    // }

    // public function tambah_klien(Request $request)
    // {
    //     return view('input_form.tambah_data_artikel');
    // }

    // public function posttestimoni(Request $request)
    // {
    //     $this->validate($request, [
    //         'nama_klien' => 'required',
    //         'gambar_klien' => 'required | mimes:png,jpg,jpeg,gif,svg,bmp',
    //     ]);
    //     $data = artikel::create($request->all());
    //     if ($request->hasFile('gambar_klien')) {
    //         $request->file('gambar_klien')->move('public/aset/images/klien', $request->file('gambar')->getClientOriginalName());
    //         $data->gambar = $request->file('gambar_klien')->getClientOriginalName();
    //         $data->save();
    //     }
    //     return redirect()->route('artikel')->with('success', 'Data berhasil disimpan');
    // }


    public function testimoni()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $testi = testimoni::leftjoin('users', 'testimoni.id_users', '=', 'users.id')
            ->select('testimoni.*', 'users.name')->orderBy('testimoni.id','DESC')->paginate(10);
        return view('main.testimoni', compact('testi'));
    }

    public function tambah_testimoni(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $pilihan_klien = User::all();
        return view('input_form.tambah_data_testimoni', compact('pilihan_klien'));
    }

    public function posttestimoni(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'review' => 'required',
            'id_users' => 'required'
        ]);

        testimoni::create($request->all());
        return redirect('testimoni')->with('success', 'Data berhasil disimpan');
    }

    public function edit_testi($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $testi = testimoni::find($id);
        $pilihan_klien = User::all();
        return view('input_form.ubah_data_testimoni', compact('testi', 'pilihan_klien'));
    }

    public function update_testi(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $testi = testimoni::find($id);
        $testi->id_users = $request->input('id_users');
        $testi->review = $request->input('review');

        //$subkategori->foto = $request->input('foto');
        //dd($subkategori);
        $testi->update();

        if ($testi) {
            //redirect dengan pesan sukses
            return redirect()->route('testimoni')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('testimoni')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }


    public function delete_testi(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $testi = testimoni::find($request->route('id'));
        $testi->delete();

        return redirect('/testimoni')->with(['success' => 'Data Berhasil Dihapus!']);
    }

    public function pencapaian()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $capai = pencapaian::paginate(10);
        return view('main.pencapaian', compact('capai'));
    }

    public function tambah_pencapaian(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_pencapaian');
    }

    public function postpencapaian(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'klien_senang' => 'required',
            'kasus' => 'required',
            'kasus_menang' => 'required',
            'penghargaan' => 'required',
        ]);

        pencapaian::create($request->all());
        return redirect('pencapaian')->with('success', 'Data berhasil disimpan');
    }

    public function edit_pencapaian($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $capai = pencapaian::find($id);
        return view('input_form.ubah_data_pencapaian', compact('capai'));
    }

    public function update_pencapaian(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $capai = pencapaian::find($id);
        $capai->klien_senang = $request->input('klien_senang');
        $capai->kasus = $request->input('kasus');
        $capai->kasus_menang = $request->input('kasus_menang');
        $capai->penghargaan = $request->input('penghargaan');

        $capai->update();

        if ($capai) {
            //redirect dengan pesan sukses
            return redirect()->route('pencapaian')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('pencapaian')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }


    public function delete_pencapaian(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $capai = pencapaian::find($request->route('id'));
        $capai->delete();

        return redirect('/pencapaian')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
