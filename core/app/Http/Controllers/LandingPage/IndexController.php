<?php

namespace App\Http\Controllers\LandingPage;

use App\Http\Controllers\Controller;
use App\Models\contact;
use App\Models\faq;
use App\Models\kategori;
use App\Models\KategoriArtikel;
use App\Models\service_subkategori;
use App\Models\services;
use App\Models\order;
use Illuminate\Http\Request;
use App\Models\artikel;
use App\Models\metode_pembayaran;
use App\Models\User;
use App\Models\pekerjaan;
use Illuminate\Support\Facades\Redirect;
use Auth;


class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function layanan()
    {
        $kontak = contact::all();
        $kategori = kategori::get();
        $services = kategori::leftjoin('service_subkategori', 'service_subkategori.id_kategori', '=', 'kategori.id')
            ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
            ->select('services.*')
            ->paginate(4);

        return view('landing-page.layanan-kami', compact('kategori', 'services', 'kontak'));
    }

    public function layananKategori(Request $request)
    {
        $kontak = contact::all();
        $kategori = kategori::get();
        $services = kategori::leftjoin('service_subkategori', 'service_subkategori.id_kategori', '=', 'kategori.id')
            ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
            ->where('service_subkategori.id_kategori', '=', $request->route('id'))
            ->select('services.*')
            ->paginate(4);

        return view('landing-page.layanan-kami', compact('services', 'kategori', 'kontak'));
    }

    public function detail(Request $request)
    {
        $kontak = contact::all();
        $detail = services::leftjoin('service_subkategori', 'services.id', 'service_subkategori.id_services')
            ->where('services.id', '=', $request->route('id'))
            ->select('services.*', 'services.id as id_services', 'service_subkategori.id as id_ss')
            ->get();
        return view('landing-page.detail-layanan', compact('detail', 'kontak'));
    }

    public function checkout(Request $request){
        $kontak = contact::all();
        $services = services::select('services.*', 'service_subkategori.id as id_service_sub')
        ->leftjoin('service_subkategori', 'services.id', '=', 'service_subkategori.id_services')
        ->where('services.id', '=', $request->route('id'))
        ->first();
        $metode = metode_pembayaran::all();

        return view('landing-page.checkout-layanan', compact('kontak', 'services', 'metode'));
    }

    public function order(Request $request)
    {
        if(Auth::user())
        {
            if(Auth::user()->alamat_user == null || Auth::user()->no_hp_user == null)
            {
                return redirect()->route('ubahProfil')->with(['notif' => 'sebelum bertransaksi silahkan lengkapi alamat dan nomor hp']);
            }
        }
        $data = $request->all();
        $post = [
            'id_service_subkategori' => $data['id_service_sub'],
            'id_users' => Auth::user()->id,
            'date_time' => date('Y-m-d'),
            'total_harga' => $data['total_harga'],
            'id_metode_pembayaran' => $data['metode_pembayaran'],
            'status_pembayaran' => $data['status_pembayaran'],
            'bukti_bayar' => NULL
        ];

        $order = order::create($post);
        if ($order) {
            $create = [
                 'id_order' => $order->id,
                 'pre_produksi' => 0,
                 'produksi' => 0,
                 'post_produksi' => 0,
                 'selesai' => 0,
                 'link_project' => NULL,
                 'size_project' => NULL,
                 'diunggah' => NULL
            ];

            pekerjaan::create($create);
       }
        return redirect()->route('transaksi');
    }

    public function faq()
    {
        $kontak = contact::all();
        $faqs = faq::get();

        return view('landing-page.faq', compact('faqs', 'kontak'));
    }


    public function artikelkami(Request $request)
    {
        $search = '';
        $kontak = contact::all();
        $kategori = KategoriArtikel::select('id','nama as nama_kategori')->orderBy('id', 'DESC')->get();
        $artikel = artikel::orderBy('id', 'DESC');
        if($request->input('search'))
        {
            $search = $request->input('search');
            $filter = $artikel->where('judul','like','%'.$request->input('search').'%')->orWhere('deskripsi','like','%'.$request->input('search').'%');
            $berita = $filter->paginate(5);
        }
        else
        {
            $berita = $artikel->paginate(5);
        }
        $new = artikel::take(3)->orderBy('id', 'DESC')->get();
        return view('landing-page.artikel', compact('kategori', 'berita', 'new', 'kontak', 'search'));
    }

    public function kategoriartikel(Request $request)
    {
        $search = $request->input('search') != null ? $request->input('search') : '';
        $kontak = contact::all();
        $kategori = KategoriArtikel::select('id','nama as nama_kategori')->find($request->route('id'));
        $kategoriall = KategoriArtikel::select('kategori_artikel.id','nama as nama_kategori')->leftjoin('artikel', 'artikel.id_kategori', '=', 'kategori_artikel.id')
            ->where('artikel.id_kategori', '=', $request->route('id'))
            ->orderBy('id','DESC')
            ->get();
        $kategori = KategoriArtikel::select('id','nama as nama_kategori')->orderBy('id','DESC')->get();
        $berita = artikel::leftjoin('kategori', 'artikel.id_kategori', '=', 'kategori.id')
            ->select('artikel.*')->where('artikel.id_kategori', '=', $request->route('id'))->orderBy('id','DESC')->paginate(5);
        $new = artikel::take(3)->orderBy('id', 'DESC')->orderBy('id','DESC')->get();

        return view('landing-page.artikel', compact('kategori', 'berita', 'kategoriall', 'new', 'kontak','search'));
    }

    public function detailartikel(Request $request)
    {
        $search = $request->input('search') != null ? $request->input('search') : '';
        $kontak = contact::all();
        $kategori = KategoriArtikel::select('id','nama as nama_kategori')->orderBy('id','DESC')->get();
        $berita = artikel::find($request->route('id'));
        $berita = artikel::leftjoin('kategori', 'artikel.id_kategori', '=', 'kategori.id')
            ->select('artikel.*')->where('artikel.id', '=', $request->route('id'))->orderBy('id','DESC')->get();
        // $berita = artikel::get($request->route('id'));
        //dd($berita);
        $new = artikel::take(3)->orderBy('id', 'DESC')->orderBy('id','DESC')->get();
        return view('landing-page.detailartikel', ['berita' => $berita, 'kategori' => $kategori, 'new' => $new, 'kontak' => $kontak, 'search' => $search]);
    }
}
