<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function task()
    {
        $data_task = task::get();
        //return view('main.task', compact('task'));
        //$data_task = DB::table('task')->get();
        return view('main.task', ['data_task' => $data_task]);
    }

    public function formtask()
    {
        return view('input_form.tambah_data_task');
    }

    public function posttask(Request $request)
    {
        $data = $request->all();
        $post = [
            'deskripsi' => $data['deskripsi']
        ];

        task::create($post);
        return redirect('/task'); //->back();
        //dd($data);
    }

    public function edittask(Request $request)
    {
        $data_task = task::find($request->route('id'));
        //dd($request->route('id'));
        return view('edit.edit_task', ['data_task' => $data_task]);
    }

    public function updatetask(Request $request)
    {
        $updatetask = task::find($request->route('id'));
        //Cara 1
        //$updatetask->deskripsi = $request->deskripsi;
        //$updatetask->save();
        //Cara 2
        $updatetask->update($request->all());
        return redirect('/task');

        //return $request;
    }

    public function deletetask(Request $request)
    {
        $deletetask = task::find($request->route('id'));
        $deletetask->delete();

        return redirect('/task');
    }
}
