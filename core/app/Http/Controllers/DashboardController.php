<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Instagram;
use App\Models\Reservasi;
use App\Models\LandingPage;
use App\Models\WebSetting;
use App\Models\CaptionDetail;
use App\Models\User;
use App\Models\order;
use App\Models\goal;
use App\Models\counter;
use App\Models\kategori;
use App\Models\faq;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //public function __construct()
    //{
    //    $this->middleware('auth');
    //}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function akun()
    {
        return view('main.akun');
    }

    public function dashboard()
    {
        $user = User::all()->count();
        $order = order::all()->count();
        $orders = order::all()->sum('total_harga');
        // $target = goal::get('target');


        // $pengunjung = counter::all()->count();
        // $pengunjung = counter::increment('visit');
        if (Auth::user()->role == 'admin') {
            return view('main.index', compact('user', 'order',  'orders'));
        } else {
            return redirect()->route('projek');
        }
    }

    public function posttarget(Request $request)
    {
        $data = $request->all();
        //dd($data);
        goal::create($data);

        return redirect('/');
    }

    public function katalog()
    {
        return view('main.katalog');
    }

    public function katalogsub()
    {
        return view('main.katalog-sub');
    }


    public function password()
    {
        return view('main.password');
    }

    public function order()
    {
        return view('main.order');
    }

    public function kontak()
    {
        return view('main.contact');
    }

    public function kategori()
    {
        return view('main.kategori');
    }

    public function subkategori()
    {
        return view('main.subkategori');
    }

    public function task()
    {
        $data_task = DB::table('task')->get();
        return view('main.task', ['data_task' => $data_task]);
    }

    public function detailtask()
    {
        $data_detail_task = DB::table('detail_task')->get();
        return view('main.detailtask', ['data_detail_task' => $data_detail_task]);
    }

    public function metodepembayaran()
    {
        $data_metode_pembayaran = DB::table('metode_pembayaran')->get();
        return view('main.metodepembayaran', ['data_metode_pembayaran' => $data_metode_pembayaran]);
    }

    public function preview()
    {
        $data_preview = DB::table('preview')->get();
        return view('main.preview', ['data_preview' => $data_preview]);
    }

    public function detail()
    {
        return view('main.detail');
    }


    public function user()
    {
        return view('main.user');
    }

    public function progres()
    {
        return view('main.progres');
    }

    public function checkout()
    {
        return view('main.checkout');
    }

    public function detailklien()
    {
        return view('main.detailklien');
    }
}
