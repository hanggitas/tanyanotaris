<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\pekerjaan;
use App\Models\order;
use App\Models\goal;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Response;
use Auth;

class PekerjaanController extends Controller
{
    
    public function filterBy($data, $request)
    {
        if($request->input('id'))
        {
            $data->where('order.id','LIKE','%'.$request->input('id').'%');
        }
        if($request->input('email'))
        {
            $data->where('users.email','LIKE','%'.$request->input('email').'%');
        }
        if($request->input('jenis_paket'))
        {
            $data->where('services.jenis_paket','LIKE','%'.$request->input('jenis_paket').'%');
        }

        return $data;
    }

    public function pekerjaan(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        //$data_pekerjaan = pekerjaan::all();
        //dd($data_pekerjaan);

        $data_pekerjaan = pekerjaan::whereNotNull('pekerjaan.id')
            ->leftJoin('order','pekerjaan.id_order','order.id')
            ->leftJoin('service_subkategori','order.id_service_subkategori','service_subkategori.id')
            ->leftJoin('services','service_subkategori.id_services','services.id')
            ->leftjoin('users', 'order.id_users', '=', 'users.id')
            ->select(['pekerjaan.*', 'users.email','services.jenis_paket'])
            ->orderBy('pekerjaan.id','DESC');
        $data_pekerjaan = $this->filterBy($data_pekerjaan, $request)->paginate(10);
        $old_value = $request->all();

        foreach ($data_pekerjaan as $key) {
            if($key->pre_produksi != 0 && $key->produksi == 0 && $key->post_produksi == 0)
            {
                $key->status_pekerjaan = 'Mulai';
            }
            elseif($key->pre_produksi != 0 && $key->produksi != 0 && $key->post_produksi == 0)
            {
                $key->status_pekerjaan = 'Dalam Proses';
            }
            elseif($key->pre_produksi != 0 && $key->produksi != 0 && $key->post_produksi != 0)
            {
                $key->status_pekerjaan = 'Pekerjaan Selesai';
            }
            else
            {
                $key->status_pekerjaan = 'Verifikasi Pembayaran';
            }
        }
            
        return view('main.pekerjaan', compact('data_pekerjaan','old_value'));
    }

    public function formpekerjaan()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $pilihan_order = order::all();
        return view('input_form.tambah_data_pekerjaan', compact('pilihan_order'));
    }

    public function postpekerjaan(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data = $request->all();
        $post = [
            'id_users' => $data['id_users'],
            'pre_produksi' => 0,
            'produksi' => $data['produksi'],
            'post_produksi' => $data['post_produksi'],
            'selesai' => $data['selesai'],
            'link_project' => $data['link_project'],
            'size_project' => $data['size_project'],
            'diunggah' => $data['diunggah'],
        ];
        pekerjaan::create($post);
        return redirect('/pekerjaan');
    }

    public function edittpekerjaan(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data_pekerjaan = pekerjaan::where('pekerjaan.id', $id)
        ->leftJoin('order','pekerjaan.id_order','order.id')
        ->leftJoin('service_subkategori','order.id_service_subkategori','service_subkategori.id')
        ->leftJoin('services','service_subkategori.id_services','services.id')
        ->leftjoin('users', 'order.id_users', '=', 'users.id')
        ->select(['pekerjaan.*', 'users.name','services.jenis_paket'])
        ->first();

       
            if($data_pekerjaan->pre_produksi != 0 && $data_pekerjaan->produksi == 0 && $data_pekerjaan->post_produksi == 0)
            {
                $data_pekerjaan->status_pekerjaan = 'Mulai';
            }
            elseif($data_pekerjaan->pre_produksi != 0 && $data_pekerjaan->produksi != 0 && $data_pekerjaan->post_produksi == 0)
            {
                $data_pekerjaan->status_pekerjaan = 'Dalam Proses';
            }
            elseif($data_pekerjaan->pre_produksi != 0 && $data_pekerjaan->produksi != 0 && $data_pekerjaan->post_produksi != 0)
            {
                $data_pekerjaan->status_pekerjaan = 'Pekerjaan Selesai';
            }
        
        // $data_pekerjaan = pekerjaan::find($id);
        // //dd($data_pekerjaan);
        // // $pilihan_user = User::get();
        // $user = pekerjaan::leftjoin('users', 'pekerjaan.id', '=', 'users.id')
        //     ->select(['pekerjaan.*', 'users.name']);
        return view('edit.edit_pekerjaan', compact('data_pekerjaan'));
        //['data_pekerjaan' => $data_pekerjaan, 'pilihan_user' => $pilihan_user]
    }

    public function updatepekerjaan(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data = [];
        $updatepekerjaan = pekerjaan::find($request->route('id'));
        if($request->input('status_pekerjaan') == "Mulai")
        {
            $data = [
                'pre_produksi' => 1,
                'produksi' => 0,
                'post_produksi' => 0,
            ];
        }
        if($request->input('status_pekerjaan') == "Dalam Proses")
        {
            $data = [
                'pre_produksi' => 1,
                'produksi' => 1,
                'post_produksi' => 0,
            ];
        }
        if($request->input('status_pekerjaan') == "Pekerjaan Selesai")
        {
            $data = [
                'pre_produksi' => 1,
                'produksi' => 1,
                'post_produksi' => 1,
            ];
        }

        $validator = Validator::make(
            $request->all(),
            [
                'lampiran_hasil' => 'mimes:jpg,jpeg,png,pdf|max:10000',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($request->hasFile('lampiran_hasil')) {
            $request->file('lampiran_hasil')->move('public/app-assets/lampiran/', $request->file('lampiran_hasil')->getClientOriginalName());
            $data['link_project'] = $request->file('lampiran_hasil')->getClientOriginalName();
        }

        $updatepekerjaan->update($data);
        return redirect('/pekerjaan')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function open_lampiran(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        if($request->route('nama_file'))
        {
            $path_to_file = storage_path('../../public/app-assets/lampiran/'.$request->route('nama_file'));
            return response()->file($path_to_file);
        }
        else
        {
            return redirect()->back();
        }
        
    }

    public function hapuspekerjaan(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $deletepekerjaan = pekerjaan::find($request->route('id'));
        $deletepekerjaan->delete();
        return redirect('/pekerjaan')->with(['success' => 'Data Berhasil Dihapus!']);
    }

    public function target()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        //$data_pekerjaan = pekerjaan::all();
        //dd($data_pekerjaan);

        $data_target = goal::get();
        return view('main.target', compact('data_target'));
    }

    public function posttarget(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data = $request->all();
        //dd($data);
        goal::create($data);
        return redirect('/target');
    }

    public function edittarget(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $data_target = goal::find($id);
        //dd($data_pekerjaan);

        return view('edit.edit_target', compact('data_target'));
        //['data_pekerjaan' => $data_pekerjaan, 'pilihan_user' => $pilihan_user]
    }

    public function updatetarget(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $updatetarget = goal::find($request->route('id'));
        $updatetarget->update($request->all());
        return redirect('/target');
    }

    public function hapustarget(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $deletetarget = goal::find($request->route('id'));
        $deletetarget->delete();
        return redirect('/target');
    }
}
