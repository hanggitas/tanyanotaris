<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\metode_pembayaran;
use Illuminate\Http\Request;

class MetodePembayaranController extends Controller
{
    
    public function metodepembayaran()
    {
        $data_metode_pembayaran = metode_pembayaran::paginate(5);
        return view('main.metodepembayaran', ['data_metode_pembayaran' => $data_metode_pembayaran]);
    }

    public function formmetodepembayaran()
    {
        return view('input_form.tambah_data_metode_pembayaran');
    }

    public function postmetodepembayaran(Request $request)
    {
        $this->validate($request, [
            'nama_pemilik_rekening' => 'required',
            'metode_pembayaran' => 'required',
            'nama_provider' => 'required',
            'foto_provider' => 'required',
            'no_rekening' => 'required'
        ]);
        // $data = $request->all();
        // //dd($data);
        // $post = [
        //     'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
        //     'metode_pembayaran' => $data['metode_pembayaran'],
        //     'nama_provider' => $data['nama_provider'],
        //     'foto_provider' => $data['foto_provider'],
        //     'no_rekening' => $data['no_rekening']
        // ];
        // $buat = metode_pembayaran::create($post);
        // if ($request->hasFile('foto_provider')) {
        //     $request->file('foto_provider')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto_provider')->getClientOriginalName());
        //     $buat->foto_provider = $request->file('foto_provider')->getClientOriginalName();
        //     $buat->save();
        // }
        //dd($buat);
        $data = metode_pembayaran::create($request->all());
        if ($request->hasFile('foto_provider')) {
            $request->file('foto_provider')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto_provider')->getClientOriginalName());
            $data->foto_provider = $request->file('foto_provider')->getClientOriginalName();
            $data->save();
        }
        return redirect('/metodepembayaran'); //->back();
    }

    public function editmetodepembayaran(Request $request)
    {
        $data_metode_pembayaran = metode_pembayaran::find($request->route('id'));
        //dd($request->route('id'));
        return view('edit.edit_metodepembayaran', ['dmp' => $data_metode_pembayaran]);
    }

    public function updatemetodepembayaran(Request $request)
    {
        $updatemetodepembayaran = metode_pembayaran::find($request->route('id'));
        $updatemetodepembayaran->nama_pemilik_rekening = $request->input('nama_pemilik_rekening');
        $updatemetodepembayaran->metode_pembayaran = $request->input('metode_pembayaran');
        $updatemetodepembayaran->nama_provider = $request->input('nama_provider');
        $updatemetodepembayaran->no_rekening = $request->input('no_rekening');
        if ($request->hasFile('foto_provider')) {
            $file = $request->file('foto_provider');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/pages/eCommerce/', $filename);
            $updatemetodepembayaran->foto_provider = $filename;
        }
        //dd($updatemetodepembayaran);
        $updatemetodepembayaran->update();
        return redirect('/metodepembayaran');

        //return $request;
    }

    public function deletemetodepembayaran(Request $request)
    {
        $deletemetodepembayaran = metode_pembayaran::find($request->route('id'));
        $deletemetodepembayaran->delete();

        return redirect('/metodepembayaran');
    }
}
