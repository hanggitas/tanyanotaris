<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\services;
use App\Models\service_subkategori;
use App\Models\kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class ServicesController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function filterBy($data, $request)
    {
        if($request->input('jenis_paket'))
        {
            $data->where('jenis_paket','LIKE','%'.$request->input('jenis_paket').'%');
        }
        if($request->input('id_kategori'))
        {
            $data->where('id_kategori',$request->input('id_kategori'));
        }
        return $data;
    }

    public function servis(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $services = services::select('services.*','service_subkategori.id_kategori','service_subkategori.id_services','kategori.nama_kategori')
        ->leftJoin('service_subkategori','services.id','=','service_subkategori.id_services')
        ->leftJoin('kategori','service_subkategori.id_kategori','=','kategori.id')
        ->groupBy('services.id')->orderBy('services.id', 'DESC');

        $services = $this->filterBy($services, $request)->paginate(10);

        $kategori_all = kategori::get();
        $old_value = $request->all();

        return view('main.service', compact('services','kategori_all','old_value'));
    }

    public function tambah_data_service()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori = kategori::get();
        return view('input_form.tambah_data_service', compact('kategori'));
    }

    public function postservice(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'jenis_paket' => 'required',
            'detail_paket' => 'required',
            'harga' => 'required',
            // 'foto' => 'required'
        ]);
 
        $data = services::create($request->all());
        if($data)
        {
            $check = service_subkategori::create([
                'id_services' => $data->id,
                'id_kategori' => $request->input('id_kategori')
            ]);
        }
      
        if($data != null)
        {
            return redirect()->route('servis')->with([ 'success' => 'Data Berhasil Ditambahkan!' ]);
        }
        else
        {
            return redirect()->route('servis')->with([ 'error' => 'Data Gagal Ditambahkan!' ]);
        }
        
    }

    public function edit($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $service = services::find($id);
        $kategori = service_subkategori::where('service_subkategori.id_services', $id)->first();
        if(empty($kategori))
        {
            $kategori = [];
            $kategori['id_kategori'] = null;
            $kategori = json_decode(json_encode($kategori), FALSE);
        }
        $kategori_all = kategori::get();
        return view('input_form.ubah_data_service', compact('service','kategori','kategori_all'));
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'jenis_paket' => 'required',
            'detail_paket' => 'required',
            'harga' => 'required'
        ]);

        $service = services::find($id);
        $service->jenis_paket = $request->input('jenis_paket');
        $service->detail_paket = $request->input('detail_paket');
        $service->harga = $request->input('harga');
        $service->syarat_layanan = $request->input('syarat_layanan');
        $service->estimasi_waktu = $request->input('estimasi_waktu');
        $service->tahapan_pengerjaan = $request->input('tahapan_pengerjaan');
        $service->update();
        if($request->input('id_kategori'))
        {
            $check = service_subkategori::where('id_services',$id)->update([
                'id_kategori' => $request->input('id_kategori')
            ]);
        }
        if ($service) {
            //redirect dengan pesan sukses
            return redirect()->route('servis')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('servis')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $service = services::find($id);
        $service->delete();
        $layanan_kategori = service_subkategori::where('id_services', $id)->delete();
        return redirect()->route('servis')->with('status', 'Data Berhasil Dihapus!');
    }
}
