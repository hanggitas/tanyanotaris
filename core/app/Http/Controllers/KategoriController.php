<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\kategori;
use App\Models\services;
use App\Models\service_subkategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    //
    public function filterBy($data, $request)
    {
        if($request->input('nama_kategori'))
        {
            $data->where('nama_kategori','LIKE','%'.$request->input('nama_kategori').'%');
        }
        return $data;
    }

    public function kategori(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori = kategori::orderBy('id','DESC');
        $kategori = $this->filterBy($kategori, $request)->paginate(10);
        $old_value = $request->all();
        return view('main.kategori', compact('kategori','old_value'));
    }

    public function tambah_data_kategori()
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        return view('input_form.tambah_data_kategori');
    }

    public function postkategori(Request $request)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'nama_kategori' => 'required',
            'foto' => 'required'
        ]);

        $validator = Validator::make(
            $request->all(),
            [
                'foto' => 'mimes:jpg,jpeg,png|max:10000',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = kategori::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        if($data != null)
        {
            return redirect()->route('kategori')->with(['success' => 'Data Berhasil Ditambahkan!']);
        }
        else
        {
            return redirect()->route('kategori')->with(['error' => 'Data Gagal Ditambahkan!']);
        }
        
    }

    public function edit($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori = kategori::find($id);
        return view('input_form.ubah_data_kategori', compact('kategori'));
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $this->validate($request, [
            'nama_kategori' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg|max:1024',
        ]);

        $kategori = kategori::find($id);
        // $kategori->nama_kategori = $request->input('nama_kategori');
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/pages/eCommerce/', $filename);
            $kategori->foto = $filename;
            $kategori->save();
        }
        $kategori->update([
            'nama_kategori' => $request->input('nama_kategori')
        ]);

        if ($kategori) {
            //redirect dengan pesan sukses
            return redirect()->route('kategori')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('kategori')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        if(Auth::user()->role != 'admin')
        {
            return redirect()->route('dashboard');
        }
        $kategori = kategori::find($id);
        $kategori->delete();
        $layanan_kategori = service_subkategori::where('id_kategori', $id)->get();
        if($layanan_kategori)
        {
            foreach ($layanan_kategori as $key) {
                $check = services::where('id',$key->id_services)->update(['id_kategori' => null]);
            }
        }
        return redirect()->route('kategori')->with('status', 'Data Berhasil Dihapus!');
    }
}
